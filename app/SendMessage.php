<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendMessage extends Model
{
    protected $table = "message_send";

    protected $fillable = ['send_id', 'message', 'phone_number', 'user_id', 'route_id', 'prefix_id', 'status', 'msg_count', 'msg_id', 'date'];

    public function user()
    {
        return $this->hasOne('App\Customer', 'id', 'user_id');
    }

    #every error message has error message
    public function route()
    {
        return $this->hasOne('App\Route', 'route', 'route_id');
    }

    public function callback()
    {
        return $this->hasOne('App\Callback', 'message_id', 'msg_id');
    }

}
