<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prefix extends Model
{
    protected $table = 'prefixes';
    protected $fillable = ['customer_id','prefix','rate','status'];

    public function prefix_info(){
        return $this->hasOne(PrefixList::class,'id','prefix');
    }
}
