<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelerivetApi extends Model
{
    protected $table = "telerivets";

    protected $fillable = ['label','api_key','project_id','route_id','random_key','status'];
}
