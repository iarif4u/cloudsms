<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrefixList extends Model
{
    protected $table = "prefix_lists";

    protected $fillable = ["label", "prefix", "color", "status"];
}
