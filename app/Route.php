<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'route_api';
    protected $primaryKey = 'route';

    protected $fillable = ['route_name', 'api', 'server', 'method', 'user', 'password', 'port', 'smpp_status', 'route_capacity', 'alert', 'tpm', 'bar', 'status'];

    //get customer prefix list
    public function prefix()
    {
        return $this->hasMany('App\RoutePrefix', 'route_id', 'route')->orderByRaw('CHAR_LENGTH(prefix) DESC')->with('prefix_info');
    }
}
