<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoute extends Model
{
    protected $table = "user_route_api";

    protected $fillable = ['user_id','route_id','capacity','count','total_send','tpm','status'];

    #get the customer
    public function customer(){
        return $this->hasOne('App\Customer','id','user_id');
    }

    #get the customer
    public function route(){
        return $this->hasOne('App\Route','route','route_id');
    }

    #get the get active roue
    public function active_route(){
        return $this->hasOne('App\Route','route','route_id')->where('status','=','1')->with('prefix');
    }
}
