<?php

namespace App\ShortLink;

use Illuminate\Database\Eloquent\Model;

class ShortLinks extends Model
{
    protected $table = 'short_links';

    protected $fillable = ['url','long_url','regex_id','account_id','customer_id'];

    public function account(){
        return $this->hasOne('App\ShortLink\ShortLinkAccount','id','account_id');
    }

    public function customer(){
        return $this->hasOne('App\Customer','id','customer_id');
    }
}
