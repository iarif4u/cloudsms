<?php

namespace App\ShortLink;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class ShortLinkAccount extends Model
{
    protected $table = 'short_link_accounts';

    protected $fillable = ['label','access_token','account_vendor','username','secret_key','tpm','total','status'];

    public function last_min_send(){
        return $this->hasMany('App\ShortLink\ShortLinks','account_id','id')->where('created_at', '>', Carbon::now()
            ->subMinutes(1));
    }
}
