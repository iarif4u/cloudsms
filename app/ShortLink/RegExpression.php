<?php

namespace App\ShortLink;

use Illuminate\Database\Eloquent\Model;

class RegExpression extends Model
{
    protected $table = 'reg_expressions';
    protected  $fillable =['label','regx','total','status'];
}
