<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCapacity extends Model
{
    protected $table = 'user_capacity';
    protected $fillable=['user_id','capacity','capacity_type'];


}


