<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    protected $table ="call_backs";

    protected $fillable = ['message_id','source','user_id','from','to','message','message_type','user_info','message_guid','gateway','status_code','connector'];

    public function message_data(){
        return $this->hasOne('App\ReplaceMessage','msg_id','message_id')->with('customer');
    }
}
