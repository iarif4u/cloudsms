<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessSMS;
use App\Prefix;
use App\QueueMessage;
use App\Route;
use App\UserRoute;
use Carbon\Carbon;
use DB;
use Ixudra\Curl\Facades\Curl;

class SMSSenderController extends Controller
{
    //route id which has send the message
    private $route_id = 0;
    private $route_prefix_rate = 0;
    private $msg_id = 0;

    #send queue messages
    public function __construct($id)
    {
        $file = fopen("send_logs.txt", "a");
        $message = QueueMessage::where(['id' => $id])->first();

        $message_id = $this->send_sms($message);
        if ($message_id) {
            $this->update_message_status($message, $message_id, $file);
        }

        fclose($file);
    }

    //update message status

    private function send_sms($message)
    {
        $this->msg_id = $message->id;
        $user_id = $message->user_id;
        $phone = $message->phone_number;
        $msg = $message->message;
        $route = $this->get_available_route($user_id, $phone);
        if ($route) {
            $this->route_id = $route->route;
            $api = $route->api;
            $request_method = $route->method;

            if (strpos(strtolower($request_method), strtolower("GET")) !== false) {

                /*dd($api);
                $api = str_ireplace('{number}', $phone, $api);
                $api = str_ireplace('{message}', $msg, $api);*/
                $request_api = explode('?', $api);
                $host = $request_api[0];
                $params = $request_api[1];
                $param_data = explode('&', $params);
                $request_params = array();

                foreach ($param_data as $param) {
                    $param_value = explode("=", $param);
                    $request_params[$param_value[0]] = $param_value[1];
                }
                $request_params[(array_search('[message]', $request_params))] = $msg;
                $request_params[(array_search('[number]', $request_params))] = $phone;
                $request_params['message_id'] = $this->msg_id;
//			dd($host);

                $response = Curl::to($host)
                    ->withData($request_params)
                    ->get();

                //  return "FFFFFF";
                if ($this->isJson($response)) {
                    $data = json_decode($response);

                    if (array_key_exists('sms_id', $data)) {
                        return $data->sms_id;
                    }
                    return "FFFFFF";
                } else {
                    return "FFFFFF";
                }

            }else{
                dispatch(new SMSSenderController($this->message->id))->delay(Carbon::now()->addMinute());
            }

        } else {
            dispatch(new ProcessSMS($message->id))->delay(Carbon::now()->addMinute());
            return false;
        }
    }

    //get the customer prefix rate

    private function get_available_route($user_id, $phone)
    {
        $routeList = UserRoute::with('active_route')->where(['user_id' => $user_id, 'status' => 1])->get();

        if ($routeList->count() > 0) {
            foreach ($routeList as $route) {
                if ($route->active_route) {
                    foreach ($route->active_route->prefix as $prefix) {
                        if ($this->match_prefix($prefix->prefix_info->prefix, $phone)) {
                            $this->route_prefix_rate = $prefix->rate;
                            $route_capacity = $route->active_route->route_capacity;
                            $route_bar = $route->active_route->bar;

                            if ($route_capacity > $route_bar) {
                                $user_route_tpm = $route->tpm;
                                $route_tpm = $route->active_route->tpm;
                                $route_msgs = (int)QueueMessage::where('created_at', '>', Carbon::now()->subMinutes(1))
                                    ->where(['route_id' => $route->active_route->route])->sum('msg_count');
                                $user_route_msgs = (int)QueueMessage::where('created_at', '>', Carbon::now()->subMinutes(1))
                                    ->where(['route_id' => $route->active_route->route, 'user_id' => $user_id])
                                    ->sum('msg_count');
                                if ($route_tpm > $route_msgs) {
                                    if ($user_route_tpm > $user_route_msgs) {
                                        return $route->active_route;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    //sms send by route

    private function match_prefix($prefix, $phone)
    {
        return substr($phone, 0, strlen($prefix)) == $prefix;
    }

    //get the active and available tmp route by user id

    private function isJson($string)
    {
        json_decode($string);
        if ((json_last_error() == JSON_ERROR_NONE)) {
            return json_decode($string);
        } else {
            return false;
        }
    }

    //is prefix match the number

    private function update_message_status($message, $message_id, $file)
    {
        $queue_id = $message->id;
        $user_id = $message->user_id;
        $router_id = $this->route_id;
        $prefix_list = Prefix::with('prefix_info')->where(['customer_id' => $user_id])->orderByRaw('CHAR_LENGTH(prefix) DESC')->get();

        $customer_prefix_rate = $this->customer_prefix_rate($prefix_list, $message->phone_number);

        $status = ($message_id == "FFFFFF") ? 3 : 1;
        $msg_count = $message->msg_count;

        $deduct_balance = $message->msg_count * $customer_prefix_rate;
        $route_deduct_balance = $message->msg_count * $this->route_prefix_rate;
        //start transaction
        DB::beginTransaction();
        try {
            QueueMessage::where(['id' => $queue_id])->update(['send_id' => $message_id,
                'route_id' => $this->route_id, 'status' => $status, 'created_at' => Carbon::now()]);
            UserRoute::where(['user_id' => $user_id, 'route_id' => $router_id])->update([
                'total_send' => DB::raw('total_send+' . $msg_count)
            ]);
            $txt = "\n[ " . now() . " ] User: " . $user_id . " Route: " . $router_id . " Page :" . $msg_count;
            fwrite($file, "\n" . $txt);
            Customer::where(['id' => $user_id])->decrement('user_capacity', $deduct_balance);
            Route::where(['route' => $this->route_id])->decrement('route_capacity', $route_deduct_balance);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            echo json_encode(array('msg' => $exception->getMessage()));
        }
    }

    //get the host name by url

    private function customer_prefix_rate($prefixList, $phone)
    {

        foreach ($prefixList as $prefix_info) {
            $prefix = $prefix_info->prefix_info;
            if (substr($phone, 0, strlen($prefix->prefix)) == $prefix->prefix) {
                return $prefix_info->rate;
            }
        }
        return false;
    }

    //check is it a valid json or not

    private function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }


}
