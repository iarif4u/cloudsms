<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\ReplaceMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;
class FeedbackController extends Controller
{
    public function index(Request $request){
        $validator=  Validator::make($request->all(), [
            'user_name' => 'required|exists:user,name',
            'password' => 'required|exists:user,password',

        ],[
            'user_name.required' => 'Username is required',
            'user_name.exists' => "Username doesn't match",
            'password.required' => 'Password is required',
            'password.exists' => "Password doesn't match",
        ]);
        if ($validator->fails())
        {
            //return error true, with validation error if has
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $is_valid_user = $this->is_valid_user($request);
            if ($is_valid_user){
                $messageList =  DB::table('message_replace_report')
                            ->leftJoin('queue_message', 'message_replace_report.msg_id', '=', 'queue_message.id')
                            ->leftJoin('message_send', 'message_replace_report.msg_id', '=', 'message_send.msg_id')
                            ->leftJoin('route_api', 'message_send.route_id', '=', 'route_api.route')
                            ->select('message_replace_report.*', 'queue_message.*', 'message_send.*','route_api.route_name')
                            ->limit(50)
                            ->where('message_send.status','>=',2)
                            ->where('message_replace_report.feedback','=',0)
                            ->orderBy('message_replace_report.id', 'asc')
                            ->get();
                $message = array();
                $msg_id = array();
                foreach ($messageList as $msg){
                    $msg_id[] = $msg->id;
                    $message[]=   [
                                    'message_id' =>$msg->send_id,
                                    'number' =>$msg->phone_number,
                                    'message' =>trim(preg_replace('/\s\s+/', ' ', $msg->incoming_msg)),
                                    'sender' =>$msg->route_name,
                                    'unit' =>$msg->msg_count,
                                    'status' =>($msg->status==2)?'Fail':'Success',
                                    'date' =>date('h:i A d-m-Y',strtotime($msg->created_at)),
                        'delivery' =>($this->isValidTimeStamp($msg->delivery_at))? date('h:i A 
                                    d-m-Y',strtotime ($msg->delivery_at)):null
                                ];

                }
                $i =0;
                echo json_encode($message,JSON_UNESCAPED_UNICODE);
                foreach ($msg_id as $dmsg){
                    ReplaceMessage::where(['id'=>$dmsg])->update(['feedback'=>1]);
                }
            }else{
                echo json_encode(['error'=>'true','message'=>"Username or password doesn't match"]);
            }
        }
    }

    //Check given credentials data is valid or not
    private function is_valid_user($request){
        $user = $request->input('user_name');
        $password = $request->input('password');
        $customer = Customer::where(['name'=>$user,'password'=>$password])->first();
        if($customer){
            return $customer;
        }
        return false;
    }

    //check valid time stamp or not
    private function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

}
