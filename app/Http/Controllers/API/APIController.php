<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessSMS;
use App\OtpOutputString;
use App\OtpString;
use App\OverCapacity;
use App\QueueMessage;
use App\ReplaceMessage;
use App\ShortLink\RegExpression;
use App\ShortLink\ShortLinkAccount;
use App\ShortLink\ShortLinks;
use Bitly;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Instasent\SMSCounter\SMSCounter;

class APIController extends Controller
{
    //get the customer/use id
    private $user = 0;
    //get the phone number
    private $phone = null;
    private $message = null;
    private $orginal_msg = null;
    private $user_tpm = 0;
    private $replace_string_id = 0;
    private $prefix_id = 0;
    private $uuid = '';
    //send message to queue processing start
    /*
     * param: user_name  {{Customer user name}}
     * param: password  {{Customer password}}
     * param: phone_no  {{Number which receive the message }}
     * param: msg        {{ Message body }}
     */
    public function send_api_sms(Request $request)
    {
        $myFile = "callback_log.txt";
        $fhs = fopen($myFile, 'a') or die("can't open file");

        $data = json_encode($request->all());
        $stringData = date('d-m-Y h:i:s A') . " " . $data . "\n";
        fwrite($fhs, $stringData);
        fclose($fhs);
        $validator = Validator::make($request->all(), [
            'api_key' => 'required|exists:user,api_key',
            'phone_no' => 'required',
            'msg' => 'required',
        ], [
            'api_key.required' => 'Api key is required',
            'api_key.exists' => 'Api key is invalid',
            'phone_no.required' => 'Phone number is required',
            'msg.required' => 'Message is required',
        ]);
        if ($validator->fails()) {
            //return error true, with validation error if has
            return ['error' => 'true', 'message' => $validator->errors()->all()];
        } else {
            //working continue if has not any validation error
            $is_valid = $this->is_valid_user($request);
            /*get the requested phone no and message*/
            $r_phoneList = $request->input('phone_no');
            $r_message = $request->input('msg');
            if ($is_valid) {
                //process when user is valid
                //check is multiple phone or single and if single make it array for make multiple pattern
                $r_phoneList = (is_array($r_phoneList)) ? $r_phoneList : array($r_phoneList);
                foreach ($r_phoneList as $r_phone):
                    $check_phone = $this->is_valid_phone($r_phone);
                    if ($check_phone) {
                        //start sms sending after pass primary validation
                        return $this->tiger_to_msg_send($r_message);
                    } else {
                        //return error true, with giver invalid phone no msg

                        return ['error' => 'true', 'message' => $r_phone . " is invalid phone no or prefix not allowed"];
                    }
                endforeach;
            } else {
                //return error true, with giver invalid user msg
                return ['error' => 'true', 'message' => "Username or password doesn't match", "uuid" => $this->uuid];
            }

        }
    }

    //1st step to message send

    private function is_valid_user($request)
    {
        $api_key = $request->input('api_key');
        $customer = Customer::with('prefix_list')->where(['api_key' => $api_key])->first();
        if ($customer) {
            $this->user = $customer;
            $this->user_tpm = $customer->user_tpm;
            return true;
        }
        return false;
    }

    //send message to queue

    private function is_valid_phone($phone)
    {

        //prefix list which are accepted for the user
        foreach ($this->user->prefix_list as $prefix) {

            //get the prefix of the phone no
            $primary_digit = substr($phone, 0, strlen($prefix->prefix_info->prefix));
            if ($primary_digit == $prefix->prefix_info->prefix) {
                $this->phone = $phone;
                $this->prefix_id = $prefix->prefix;
                return true;
            }
        }
        return false;
    }

    //get the number of message page which is user given

    private function tiger_to_msg_send($request_msg)
    {

        //set the original message
        $this->orginal_msg = $request_msg;
        //get the messages of last minute send

        $last_minute_send = QueueMessage::where('created_at', '>', Carbon::now()->subMinutes(1))->where(['user_id' => $this->user->id])->count();

        if ($last_minute_send < $this->user_tpm) {

            //get the otp matching string message
            $string_msg = $this->get_replace_message($request_msg);
            $this->message = $string_msg;

            //get the message unit number
            $msg_count = $this->get_message_unit_number($request_msg);

            //user running queue message
            $user_queue_msg = QueueMessage::where(['user_id' => $this->user->id, 'status' => 0])->sum('msg_count');

            if ($this->user->user_capacity > $user_queue_msg) {
                return $this->send_msg_to_queue($msg_count);
            }
        } else {
            OverCapacity::create([
                'user_id' => $this->user->id,
                'message' => $request_msg,
                'phone' => $this->phone
            ]);
            return ['error' => 'false', 'message' => "Message goes to overcapacity"];
        }
    }

    //get the replace message string before replace string

    private function get_replace_message($request_msg)
    {
        $stringList = OtpString::where(['status' => 1])->get();
        $request_msg = $this->make_n_replace_short_link($request_msg);
        foreach ($stringList as $string) {
            if (strpos(strtolower($request_msg), strtolower($string->incoming_string)) !== false) {
                try {
                    $replace_str = OtpOutputString::where(['incoming_string_id' => $string->id, 'status' => 1])->orderBy('counter', 'ASC')->first();
                    if ($replace_str) {
                        $replace_str->counter += 1;
                        $replace_str->save();
                        $request_msg = str_ireplace($string->incoming_string, $replace_str->replace_string, $request_msg);
                    }
                } catch (\Exception $exception) {
                    continue;
                }
            }
        }

        return preg_replace("/[\n\r]/", "", $request_msg);
    }

    //get short link active account which tpm is available

    private function make_n_replace_short_link($message)
    {
        $active_account = $this->get_short_link_acc();

        if ($active_account) {
            $short_link = false;
            $reg_express = RegExpression::where(['status' => 1])->get();
            foreach ($reg_express as $regx) {
                if ($this->isRegularExpression($regx->regx)) {
                    $result = preg_match($regx->regx, $message, $out_string);

                    if ($result > 0) {
                        foreach ($out_string as $url) {

                            $parse_url = $this->parse_valid_url($url);
                            if (filter_var($parse_url, FILTER_VALIDATE_URL)) {
                                try {
                                    if ($active_account->account_vendor == Config::get('info.short_link.account.bit_lay')) {
                                        $short_link = $this->get_bitly_url($parse_url, $active_account->access_token);
                                    }
                                    if ($short_link) {
                                        $active_account->increment('total', 1);
                                        $regx->increment('total', 1);
                                        ShortLinks::create([
                                            'url' => $short_link,
                                            'long_url' => $url,
                                            'regex_id' => $regx->id,
                                            'account_id' => $active_account->id,
                                            'customer_id' => $this->user->id,
                                        ]);
                                        $message = str_ireplace($url, $short_link, $message);
                                    }
                                } catch (\Exception $exception) {
                                    dd($exception->getMessage());
                                    return $message;
                                }

                            }
                        }
                    }
                }
            }
        }
        return $message;
    }

    #replace and make shortlink mesage

    private function get_short_link_acc()
    {
        //dd(Carbon::now()->subMinutes(1));
        $account_list = ShortLinkAccount::with(['last_min_send'])->where(['status' => 1])->orderBy('total', 'ASC')->get();
        foreach ($account_list as $account) {
            if ($account->last_min_send->count() < $account->tpm) {
                return $account;
            }
        }
        return false;
    }

    //make bit.ly short url

    private function isRegularExpression($string)
    {
        return @preg_match($string, '') !== FALSE;
    }

    //check the url is valid or not

    private function parse_valid_url($urlStr)
    {
        if (preg_match('/http[s]?:\/\//', $urlStr, $out_string)) {
            return $urlStr;
        } else {
            return 'http://' . $urlStr;
        }
    }

    //check the regular expression string is valid or not

    private function get_bitly_url($url, $access_token)
    {
        try {
            $req_result = Bitly::getUrl($url, $access_token);
            if ($req_result['status_code'] == 200 && $req_result['status_txt'] == 'OK') {
                return $req_result['data']['url'];
            }
        } catch (\Exception $exception) {
            return false;
        }
        return false;
    }

    //Check given credentials data is valid or not

    private function get_message_unit_number($message)
    {
        $smsCounter = new SMSCounter();
        $details = $smsCounter->count($message);
        return $details->messages;
    }

    //check phone no is valid or not

    private function send_msg_to_queue($msg_count)
    {
        $uuid = uniqid();
        $this->uuid = $uuid;
        DB::beginTransaction();
        try {
            $queue = QueueMessage::create([
                'message' => $this->message,
                'phone_number' => $this->phone,
                'uuid' => $this->uuid,
                'user_id' => $this->user->id,
                'route_id' => 0,
                'prefix_id' => $this->prefix_id,
                'status' => 0,
                'send_id' => 0,
                'msg_count' => $msg_count
            ]);
            //new SMSSenderController($queue->id);
            dispatch(new ProcessSMS($queue->id))->delay(Carbon::now()->addSeconds(1));
        } catch (\Exception $exception) {
            DB::rollback();
            return ['error' => 'true', 'message' => $exception->getMessage()];
        }
        DB::commit();
        return ['error' => false, 'message' => "Message send success", "UUID" => $this->uuid];
    }
}
