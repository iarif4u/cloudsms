<?php

namespace App\Http\Controllers\API;

use App\ErrorMessage;
use App\Http\Controllers\Controller;
use App\QueueMessage;
use App\ReplaceMessage;
use App\SendMessage;
use App\TelerivetApi;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

require_once dirname(__FILE__) . '/../../../../vendor/telerivet/telerivet-php-client/telerivet.php';

class StatusUpdateController extends Controller
{
    //route id
    private $route_id = 0;

    //update telerivet status which are on status =1.
    public function update_telerivet_sms_status()
    {
        $message_list = QueueMessage::with('route')->where('status', '=', 1)
            ->get();
        foreach ($message_list as $message) {
            $route_id = $message->route->route;
            $api = $message->route->api;
            $random_key = $this->get_random_number($api);
            $telerivet = $this->get_telerivet_credential($random_key);
            $telerivet_api_key = $telerivet['api_key'];
            $telerivet_project_id = $telerivet['project_id'];

            $telerivet = new \Telerivet_API($telerivet_api_key);
            $project = $telerivet->initProjectById($telerivet_project_id);

            try {
                $telerivet_msg = $project->getMessageById($message->send_id);
                /*     echo date('h:i:s A d-m-Y',$telerivet_msg->time_sent);
                     dd($telerivet_msg);*/
                $status = 1;
                $error_message = false;
                if ($telerivet_msg->status == "delivered") {
                    $status = 3;
                } elseif ($telerivet_msg->status == "sent") {
                    $status = 3;
                } elseif ($telerivet_msg->status == "failed") {
                    $status = 2;
                    $error_message = $telerivet_msg->error_message;
                } elseif ($telerivet_msg->status == "not_delivered") {
                    $status = 2;
                    $error_message = $telerivet_msg->error_message;
                } else {
                    continue;
                }
                QueueMessage::where(['id' => $message->id])->update(['status' => $status]);
                if ($status == 3) {
                    ReplaceMessage::where(['msg_id' => $message->id])->update(['delivery_at' => $telerivet_msg->time_sent]);
                }
                if ($error_message != false) {
                    ErrorMessage::create([
                        'msg_id' => $message->id,
                        'send_id' => $message->send_id,
                        'message' => $error_message
                    ]);
                }
            } catch (\Exception $e) {
                QueueMessage::where(['id' => $message->id])->update(['status' => 3]);
                continue;
            }

        }

    }

    //get the telerivet apikey and project id

    private function get_random_number($api)
    {
        $spilts = explode('?', $api);
        $spilts = explode('&', $spilts[1]);
        $parts = array();
        foreach ($spilts as $spilt) {
            $part = explode('=', $spilt);
            if (($part[0] == "random_key")) {
                return $part[1];
            }

        }
    }

    //return the random number from api

    private function get_telerivet_credential($random_key)
    {
        $telerivet = TelerivetApi::where(['random_key' => $random_key])->first();
        return ['api_key' => $telerivet->api_key, 'project_id' => $telerivet->project_id];
    }

    //change messages to message send table

    public function change_message_to_send()
    {
        $message_list = QueueMessage::with('route')->where('status', '>', 1)->get();
        $messages = array();
        $deletes = array();
        foreach ($message_list as $message) {
            $deletes[] = $message->id;
            //   SendMessage::create();
            $messages[] = [
                'send_id' => $message->send_id,
                'message' => $message->message,
                'phone_number' => $message->phone_number,
                'user_id' => $message->user_id,
                'route_id' => $message->route_id,
                'prefix_id' => $message->prefix_id,
                'status' => $message->status,
                'msg_count' => $message->msg_count,
                'msg_id' => $message->id,
                'date' => $message->created_at,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
            // QueueMessage::where(['id' => $message->id])->delete();
        }
        try {
            DB::beginTransaction();
            // ['send_id','message','phone_number','user_id','route_id','status','msg_count','msg_id','date']
            foreach (array_chunk($messages, 100) as $messagList) {
                SendMessage::insert($messagList);
            }
            foreach (array_chunk($deletes, 100) as $delete) {
                QueueMessage::whereIn('id', $delete)->delete();
            }
            DB::commit();
            return response()->json('Success');
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json($exception->getMessage(), 403);
        }
    }
}
