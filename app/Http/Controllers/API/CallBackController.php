<?php

namespace App\Http\Controllers\API;


use App\CallBack;
use App\QueueMessage;
use App\ReplaceMessage;
use App\SendMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Customer;
use App\OtpOutputString;
use App\OtpString;
use App\OverCapacity;
use DB;
use Instasent\SMSCounter\SMSCounter;

class CallBackController extends Controller
{

    //get the customer/use id
    private $user = 0;
    //get the phone number
    private $phone = null;
    private $message = null;
    private $orginal_msg = null;
    private $user_tpm = 0;
    private $replace_string_id = 0;


    //get call back request
    public function get_call_back(Request $request)
    {
        //write log data
        $this->write_log_txt($request->all());

        $validator = Validator::make($request->all(), [
            'source' => 'required',
            'user_id' => 'required',
            'from' => 'required',
            'to' => 'required',
            'message' => 'required',
            'message_type' => 'required',
            'user_info' => 'required',
            'message_guid' => 'required',
            'message_parts' => 'required',
        ]);

        if (!$validator->fails()) {
            $user = $request->input('source');
            $customer = $this->is_valid_user($user);
            if ($customer) {
                $source = $request->input('source');
                $user_id = $request->input('user_id');
                $from = $request->input('from');
                $to = $request->input('to');
                $message = $request->input('message');
                $message_type = $request->input('message_type');
                $user_info = $request->input('user_info');
                $message_guid = $request->input('message_guid');
                $gateway = $request->input('gateway',"wintel");
                $status_code = $request->input('status_code',200);
                $connector = $request->input('connector','connector');
                $phone = $this->get_valid_phone($to);

                /*get the requested phone no and message*/
                $r_phoneList = $to;
                $r_message = $message;


                $check_phone = $this->is_valid_phone($r_phoneList);
                if ($check_phone) {
                    //start sms sending after pass primary validation
                    $queue_id = $this->tigger_to_msg_send($r_message);

                    CallBack::create([
                        'message_id' => $queue_id,
                        'source' => $source,
                        'user_id' => $user_id,
                        'from' => $from,
                        'to' => $phone,
                        'message' => $message,
                        'message_type' => $message_type,
                        'user_info' => $user_info,
                        'message_guid' => $message_guid,
                        'gateway' => $gateway,
                        'status_code' => $status_code,
                        'connector' => $connector
                    ]);
                } else {
                    //return error true, with giver invalid phone no msg
                    echo json_encode(['error' => 'true', 'message' => $r_phoneList . " is invalid phone no or length"]);
                }
            } else {
                //return error true, with giver invalid user msg
                echo json_encode(['error' => 'true', 'message' => "Username or password doesn't match"]);
            }
        }


        echo "result=1";
    }

    //1st step to message send
    private function tigger_to_msg_send($request_msg)
    {
        //set the original message
        $this->orginal_msg = $request_msg;
        //get the messages of last minute send

        $messages = QueueMessage::where('created_at', '>', Carbon::now()->subMinutes(1))->where
        (['user_id' => $this->user->id])
            ->get();

        //get the number of messages of last minute send
        $last_minute_send = $messages->sum('msg_count');

        if ($last_minute_send < $this->user_tpm) {
            //get the otp matching string message
            $string_msg = $this->get_replace_message($request_msg);

            if ($string_msg) {
                //replace the message string data
                $replaced_msg = $this->replace_message_string($string_msg, $request_msg);
                $this->message = $replaced_msg;
            } else {
                $this->message = $request_msg;
                //doesn't match the string
            }
            //get the message unit number
            $msg_count = $this->get_message_unit_number($request_msg);

            //user running queue message
            $user_queue_msg = QueueMessage::where(['user_id' => $this->user->id, 'status' => 0])->get()->sum('msg_count');

            if ($this->user->user_capacity > $user_queue_msg) {
                return $this->send_msg_to_queue($msg_count);
            }
        } else {
            OverCapacity::create([
                'user_id' => $this->user->id,
                'message' => $request_msg,
                'phone' => $this->phone
            ]);
            echo json_encode(['error' => 'false', 'message' => "Message goes to overcapacity"]);
        }
    }

    //get the string replaced message which has to send
    private function replace_message_string($string_msg,$request_msg){
        $replace_str =OtpOutputString::where(['incoming_string_id'=>$string_msg->id,'status'=>1])->orderBy('counter',
            'ASC')->first();
        if ($replace_str){
            $this->replace_string_id = $replace_str->id;
            return str_ireplace($string_msg->incoming_string, $replace_str->replace_string, $request_msg);
        }else{
            return $request_msg;
        }
    }
    //get the replace message string before replace string
    private function get_replace_message($request_msg){
        $stringList = OtpString::where(['status'=>1])->get();
        foreach ($stringList as $string){
            if (strpos(strtolower($request_msg), strtolower($string->incoming_string)) !== false) {
                return $string;
            }
        }
        return false;
    }
    //send message to queue
    private function send_msg_to_queue($msg_count)
    {
        DB::beginTransaction();
        try {
            $queue = QueueMessage::create([
                'message' => $this->message,
                'phone_number' => $this->phone,
                'user_id' => $this->user->id,
                'route_id' => 0,
                'status' => 0,
                'send_id' => 0,
                'msg_count' => $msg_count
            ]);
            try {
                ReplaceMessage::create([
                    "msg_id" => $queue->id,
                    'incoming_msg' => $this->orginal_msg,
                    'phone' => $this->phone,
                    'user_id' => $this->user->id,
                    'route_id' => 0,
                ]);
                if ($this->replace_string_id > 0) {
                    OtpOutputString::where(['id' => $this->replace_string_id])->increment('counter');
                }
            } catch (\Exception $exception) {
                DB::rollback();
                echo json_encode(['error' => 'true', 'message' => $exception->getMessage()]);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            echo json_encode(['error' => 'true', 'message' => $exception->getMessage()]);
        }

        DB::commit();
       
        return $queue->id;
    }

    //get the send message id
    private function get_message_id($to, $message)
    {
        $phone = $this->get_valid_phone($to);
        $this->write_log_txt($phone);
        $messages = ReplaceMessage::where('created_at', '>', Carbon::now()->subSeconds(3))
            ->where(['phone' => $phone, 'incoming_msg' => $message])
            ->first();
        return ($messages) ? $messages->msg_id : 0;
    }

    //get the number of message page which is user given
    private function get_message_unit_number($message)
    {
        $smsCounter = new SMSCounter();
        $details = $smsCounter->count($message);
        return $details->messages;
    }

    //check phone no is valid or not
    private function get_valid_phone($phone)
    {
        //prefix list which are accepted by bd
        $allow_prefix = ['015', '016', '017', '018', '019'];
        //get the last 11 digit of phone no with out +88
        $local_no = substr($phone, -11);
        //get the prefix of the phone no
        $primary_digit = substr($local_no, 0, 3);
        if (in_array($primary_digit, $allow_prefix)) {
            return "+88" . $local_no;
        }
        return false;
    }

    //check phone no is valid or not
    private function is_valid_phone($phone)
    {
        //prefix list which are accepted by bd
        $allow_prefix = ['015', '016', '017', '018', '019'];
        //get the last 11 digit of phone no with out +88
        $local_no = substr($phone, -11);
        //get the prefix of the phone no
        $primary_digit = substr($local_no, 0, 3);
        if (in_array($primary_digit, $allow_prefix)) {
            $temp_no = "+88" . $local_no;
            if (strlen($temp_no) == 14) {
                $this->phone = $temp_no;
                return true;
            }
        }
        return false;
    }

    //get smpp request by diafaan
    public function vai_smpp_req(Request $request)
    {
        $this->write_smpp_log_txt($request->all());
		 echo "result=1";
        //working continue if has not any validation error
        //   $is_valid = $this->is_valid_user($request);
    }

    //Check given credentials data is valid or not
    private function is_valid_user($user)
    {
        $customer = Customer::where(['name' => $user])->first();

        if ($customer) {
            $this->user = $customer;
            $this->user_tpm = $customer->user_tpm;
            return true;
        }
        return false;
    }

    //write request log into file
    private function write_log_txt($request)
    {
        $myFile = "callback_log.txt";
        $fh = fopen($myFile, 'a') or die("can't open file");
        $data = json_encode($request);
        $stringData = date('d-m-Y h:i:s A') . " " . $data . "\n";
        fwrite($fh, $stringData);
        fclose($fh);
    }

    //write smpp request log into file
    private function write_smpp_log_txt($request)
    {
        $myFile = "smpp_log.txt";
        $fh = fopen($myFile, 'a') or die("can't open file");
        $data = json_encode($request);
        $stringData = date('d-m-Y h:i:s A') . " " . $data . "\n";
        fwrite($fh, $stringData);
        fclose($fh);
    }
}
