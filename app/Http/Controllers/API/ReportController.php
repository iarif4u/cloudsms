<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use App\SendMessage;

class ReportController extends Controller
{
    public function make_template_report()
    {
        $customers = Customer::orderByDesc('id')->get();
        $messages = array();
        foreach ($customers as $customer) {
            foreach ($customer->prefix_list as $prefix) {
                $msg = [
                    'prefix' => $prefix->prefix,
                    'user' => $customer->id,
                    'message' => SendMessage::with('route')
                        ->where(['user_id' => $customer->id, 'prefix_id' => $prefix->prefix])
                        ->groupBy('route_id')
                        ->count()
                ];
                $messages[] = $msg;
            }
        }
        dd($messages);
    }
}
