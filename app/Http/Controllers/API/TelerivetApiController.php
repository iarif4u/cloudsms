<?php

namespace App\Http\Controllers\API;
use App\Delete;
use DB;
use App\TelerivetApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
require_once dirname(__FILE__).'/../../../../vendor/telerivet/telerivet-php-client/telerivet.php';
class TelerivetApiController extends Controller
{
    #get telerivet info
    public function get_telerivet_info(){
        $telerivetList = TelerivetApi::all();
        $deletedTelerivet = Delete::where(['name'=>'telerivets'])->get();
        return view('admin.api.telerivet',['telerivetList'=>$telerivetList,'deletedTelerivet'=>$deletedTelerivet]);
    }

    #make new telerivet api
    public function make_telerivet_api(Request $request){
        $validator=  Validator::make($request->all(), [
            'telerivet_label' => 'required|unique:telerivets,label',
            'telerivt_api_key' => 'required|size:36',
            'telerivt_project_id' => 'required|size:18',
            'status' => 'required|in:0,1'
        ],[
            'telerivet_label.required' => 'Telerivet API Label is required',
            'telerivet_label.unique' => 'Telerivet API Label has already been taken',
            'telerivt_api_key.required' => 'Telerivet API key is required',
            'telerivt_api_key.size' => 'Telerivet API key is invalid',
            'telerivt_project_id.required' => 'Telerivet project id is required',
            'telerivt_project_id.size' => 'Telerivet project id is invalid',
            'status.size' => 'Telerivet API status is required',
            'status.in' => 'Telerivet API status is invalid',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $ran = '';
            for( $i=0; $i<32; $i++ ) {
                $ran .= chr( rand( 65, 90 ) );
            }
            TelerivetApi::create([
                'label'=>$request->input('telerivet_label'),
                'random_key'=>$ran.time(),
                'api_key'=>$request->input('telerivt_api_key'),
                'project_id'=>$request->input('telerivt_project_id'),
                'route_id'=>$request->input('telerivt_route_id'),
                'status'=>$request->input('status'),
            ]);
            return redirect()->back()->with('message',"New Telerivet API Create Success");
        }
    }

    //update telerivet api details info
    public function update_telerivet_api(Request $request){
        $telerivet_id = $request->input('telerivet_id');
        $validator=  Validator::make($request->all(), [
            'telerivet_label' => 'required|unique:telerivets,label,'.$telerivet_id,
            'telerivt_api_key' => 'required|size:36',
            'telerivt_project_id' => 'required|size:18',
            'status' => 'required|in:0,1'
        ],[
            'telerivet_label.required' => 'Telerivet API Label is required',
            'telerivet_label.unique' => 'Telerivet API Label has already been taken',
            'telerivt_api_key.required' => 'Telerivet API key is required',
            'telerivt_api_key.size' => 'Telerivet API key is invalid',
            'telerivt_project_id.required' => 'Telerivet project id is required',
            'telerivt_project_id.size' => 'Telerivet project id is invalid',
            'status.size' => 'Telerivet API status is required',
            'status.in' => 'Telerivet API status is invalid',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            TelerivetApi::where(['id'=>$telerivet_id])->update([
                'label'=>$request->input('telerivet_label'),
                'api_key'=>$request->input('telerivt_api_key'),
                'project_id'=>$request->input('telerivt_project_id'),
                'route_id'=>$request->input('telerivt_route_id'),
                'status'=>$request->input('status'),
            ]);
            return redirect()->back()->with('message',"Telerivet API Update Success");
        }
    }

    public function update_telerivet_status(Request $request){
        $telerivet_id = $request->input('telerivet_id');
        $status = $request->input('status');

        $validator=  Validator::make($request->all(), [
            'telerivet_id' => 'required|exists:telerivets,id',

        ],[
            'telerivet_id.required' => 'Telerivet API id is required',
            'telerivet_id.exists' => 'Telerivet API id doesn\'t match',

        ]);

        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            DB::beginTransaction();
            try{
                TelerivetApi::where(['id'=>$telerivet_id])->update(['status'=>$status]);
            }catch (\Exception $exception){
                DB::rollback();
                echo json_encode(['error'=>'true','message'=>$exception->getMessage()]);
            }
            DB::commit();
            echo json_encode(['error'=>'false','message'=>"Status Update Success"]);

        }
    }

    //delete telerivet api data
    public function delete_telerivet_api(Request $request){

        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:telerivets,id',

        ],[
            'string_id.required' => 'Telerivet API id is required',
            'string_id.exists' => 'Telerivet API id doesn\'t match',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all());
        }else{
            DB::beginTransaction();
            try{
                $telerivet_id = $request->input('string_id');
                $string =  TelerivetApi::find($telerivet_id);
                try{
                    Delete::create([
                        'primary_value'=>$string->id,
                        'name'=>$string->getTable(),
                        'others_value'=>json_encode($string)
                    ]);
                    $string->delete();
                }catch (\Exception $exception){
                    DB::rollback();
                    return redirect()->back()->withErrors($exception->getMessage());
                }
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors($exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message',"Telerivet Api Delete Success");

        }
    }

    //sms send vai telerivet api

    /**
     * @param Request $request
     */
    public function send_telerivet_sms(Request $request){

        $validator=  Validator::make($request->all(), [
            'random_key' => 'required|exists:telerivets,random_key',
            'number' => 'required',
            'msg' => 'required',

        ],[
            'random_key.required' => 'API Random key is required',
            'random_key.exists' => 'API Random key is invalid',

        ]);

        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $number = $this->is_valid_number($request->input('number'));
            if ($number){
                $telerivetCredential  = TelerivetApi::where(['random_key'=>$request->input('random_key'),'status'=>1])
                    ->first();
                if ($telerivetCredential){
                    $telerivetAPI = $telerivetCredential->api_key;
                    $projectId = $telerivetCredential->project_id;
                    $route_id = ($telerivetCredential->route_id)?$telerivetCredential->route_id:null;
                    $message = $request->input('msg');

                    $this->send_sms_by_api($telerivetAPI,$projectId,$number,$message,$route_id);

                }else{
                    echo json_encode(['error'=>'true','message'=>"Credential doesn't match or no active"]);
                }
            }else{
                echo json_encode(['error'=>'true','message'=>"Invalid number"]);
            }

        }
    }

    #send message by telerivet api

    /**
     * @param $api_key
     * @param $project_id
     * @param $number
     * @param $msg
     * @param null $route_id
     */
    private function send_sms_by_api($api_key, $project_id, $number, $msg, $route_id=null){
        $telerivet = new \Telerivet_API($api_key);
        $project = $telerivet->initProjectById($project_id);
        
        // Send a SMS message
        try{
            if ($route_id==null){
                $send_data= $project->sendMessage(array(
                    'to_number' => $number,
                    'content' => $msg,
                ));
                $txt = "\nDate : [".now()."] Route: Default || Number: ".$number;
            }else{
                $send_data = $project->sendMessage(array(
                    'to_number' => $number,
                    'content' => $msg,
                    'route_id' => $route_id
                ));
                $txt = "\nDate : [".now()."] Route: ".$route_id." || Number: ".$number;
            }
            $file = fopen("telerivet_send_logs.txt","a");
            fwrite($file, $txt);
            fclose($file);
            echo json_encode(['error'=>false,'message_id'=>$send_data->id]);
            //message send end
        }catch (\Exception $exception){
            echo json_encode(['error'=>'true','message'=>$exception->getMessage()]);

        }


    }

    //check valid number or not

    /**
     * @param $number
     * @return bool|string
     */
    private function is_valid_number($number){
        $allow_prefix = ['015','016','017','018','019'];
        $local_no = substr($number, -11);
        $primary_digit = substr($local_no, 0,3);
        return (in_array($primary_digit,$allow_prefix)) ? "+88".$local_no :false;
    }
}
