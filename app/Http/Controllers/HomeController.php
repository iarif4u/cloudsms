<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    //customer login from admin
    public function customer_login($customer_id, Request $request)
    {
        $customer = Customer::where(['id'=>$customer_id])->first();

        $login_data = [
            'email' => $customer->email,
            'password' => $customer->password,
        ];
        Auth::guard('customer')->attempt($login_data);

        return redirect(route('customer.home'));
    }
}
