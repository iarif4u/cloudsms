<?php

namespace App\Http\Controllers\ShortLink;

use App\Delete;
use App\ShortLink\ShortLinkAccount;
use App\ShortLink\RegExpression;
use App\ShortLink\ShortLinks;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Bitly;
use App\Http\Controllers\Admin\BaseController;
class ShortLinkController extends BaseController
{
    //get bit.ly settings view
    public function get_bitly_view(){

        $bitLays =ShortLinkAccount::all();
        return view('shortlink.bitlay',['bitLays'=>$bitLays]);
    }

    //get short url list which are created
    public function get_short_urls(){
        $short_links = ShortLinks::with(['account','customer'])->orderBy('id','desc')->get();
        return view('shortlink.short_links',['short_links'=>$short_links]);
    }

    //make new bit lay account by access token
    public function make_new_bitly(Request $request){
        $validator=  Validator::make($request->all(), [
            'bitly_name' => 'required',
            'bitly_token' => 'required|unique:short_link_accounts,access_token|alpha_num',
            'bitly_tpm' => 'required|numeric',
            'status' => 'required|in:0,1',
        ],[
            'bitly_name.required' => 'Bit.ly label is required',
            'bitly_token.required' => 'Bit.ly access token is required',
            'bitly_token.unique' => 'The Bit.ly access token has already been taken',
            'bitly_token.alpha_num' => 'The Bit.ly access token has invalid characters',
            'tpm.required' => 'Bit.ly TPM is required',
            'tpm.numeric' => 'Bit.ly TPM is invalid or non-numeric value',
            'status.required' => 'Bit.ly status is required',
            'status.in' => 'Bit.ly status is invalid'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{

            $label = $request->input('bitly_name');
            $bitly_tpm = $request->input('bitly_tpm');
            $bitly_token = $request->input('bitly_token');
            $status = $request->input('status');
            if (strlen($bitly_token)!=40){
                return redirect()->back()->withErrors(['The Bit.ly access token is invalid or invalid length'])
                    ->withInput();
            }
            try{
                ShortLinkAccount::create([
                    'label'=>$label,
                    'access_token'=>$bitly_token,
                    'account_vendor'=>Config::get('info.short_link.account.bit_lay'),
                    'tpm'=>$bitly_tpm,
                    'total'=>0,
                    'status'=>$status
                ]);
            }catch (\Exception $exception){
                return redirect()->back()->withErrors(['The Bit.ly access token registered fail due to '.$exception->getMessage()])
                    ->withInput();
            }
            return redirect()->back()->with('message','The Bit.ly access token registered successfully done');
        }

    }
    //update bit lay account/access token info
    public function update_bitLay_info(Request $request){
        $bit_lay_id = $request->input('bit_lay_id',0);
        $validator=  Validator::make($request->all(), [
            'bit_lay_id' => 'required|exists:short_link_accounts,id',
            'edit_bitly_name' => 'required',
            'edit_bitly_token' => 'required|alpha_num|unique:short_link_accounts,access_token,'.$bit_lay_id,
            'edit_bitly_tpm' => 'required|numeric',
            'edit_status' => 'required|in:0,1',
        ],[
            'bit_lay_id.required' => 'Bit.ly label id is required',
            'bit_lay_id.exits' => 'Bit.ly label id is invalid',
            'edit_bitly_name.required' => 'Bit.ly label is required',
            'edit_bitly_token.required' => 'Bit.ly access token is required',
            'edit_bitly_token.unique' => 'The Bit.ly access token has already been taken',
            'edit_bitly_token.alpha_num' => 'The Bit.ly access token has invalid characters',
            'edit_tpm.required' => 'Bit.ly TPM is required',
            'edit_tpm.numeric' => 'Bit.ly TPM is invalid or non-numeric value',
            'edit_status.required' => 'Bit.ly status is required',
            'edit_status.in' => 'Bit.ly status is invalid'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $label = $request->input('edit_bitly_name');
            $bitly_tpm = $request->input('edit_bitly_tpm');
            $bitly_token = $request->input('edit_bitly_token');
            $status = $request->input('edit_status',1);
            if (strlen($bitly_token)!=40){
                return redirect()->back()->withErrors(['The Bit.ly access token is invalid or invalid length'])
                    ->withInput();
            }
            try{
                ShortLinkAccount::where(['id'=>$bit_lay_id])->update([
                    'label'=>$label,
                    'access_token'=>$bitly_token,
                    'tpm'=>$bitly_tpm,
                    'status'=>$status
                ]);
            }catch (\Exception $exception){
                return redirect()->back()->withErrors(['The Bit.ly access token update fail due to '
                    .$exception->getMessage()])
                    ->withInput();
            }
            return redirect()->back()->with('message','The Bit.ly access token update successfully done');
        }
    }

    //update bitlay access status is activate or deactivate
    public function update_bitlay_status(Request $request){
        $validator=  Validator::make($request->all(), [
            'bit_lay_id' => 'required|exists:short_link_accounts,id',
        ],[
            'bit_lay_id.required' => 'Bit.ly label id is required',
            'bit_lay_id.exits' => 'Bit.ly label id is invalid',
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $bit_lay_id = $request->input('bit_lay_id', 0);
            $status = $request->input('status');
            $bit_lay = ShortLinkAccount::find($bit_lay_id);
            $bit_lay->status = $status;
            $bit_lay->save();
            echo json_encode(['error'=>'false','message'=>"Status Update Success"]);
        }
    }
    //delete bitlay info by post req
    public function delete_bitLay_info(Request $request){
        $validator=  Validator::make($request->all(), [
            'bit_lay_id' => 'required|exists:short_link_accounts,id',
        ],[
            'bit_lay_id.required' => 'Bit.ly label id is required',
            'bit_lay_id.exits' => 'Bit.ly label id is invalid',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            DB::beginTransaction();
            try {
                $bit_lay_id = $request->input('bit_lay_id', 0);
                $bit_lay = ShortLinkAccount::find($bit_lay_id);
                Delete::create([
                    'primary_value' => $bit_lay->id,
                    'name' => $bit_lay->getTable(),
                    'others_value' => json_encode($bit_lay)
                ]);
                $bit_lay->delete();
                DB::commit();
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors(['The Bit.ly access token delete fail due to '
                    .$exception->getMessage()])
                    ->withInput();
            }
            return redirect()->back()->with('message','The Bit.ly access token delete successfully done');
        }
    }

    //setting up regulation expression setting
    public function get_regx_setting(){
        $reg_expr =RegExpression::all();
        $deleted_regx = Delete::where(['name'=>'reg_expressions'])->get();

        return view('shortlink.setting',['reg_expr'=>$reg_expr,'deleted_regx'=>$deleted_regx]);
    }

    //update regular expression
    public function update_regular_expression(Request $request){
        $regx_id = $request->input('regx_id',0);
        $validator=  Validator::make($request->all(), [
            'regx_id' => 'required|exists:reg_expressions,id',
            'edit_regx_label' => 'required',
            'edit_regx' => 'required|unique:reg_expressions,regx,'.$regx_id,
            'edit_status' => 'required|in:0,1',
        ],[
            'regx_id.required' => 'Regular Expression id is required',
            'regx_id.exists' => 'Regular Expression id is invalid',
            'edit_regx_label.required' => 'Regular Expression label is required',
            'edit_regx.required' => 'Regular Expression string is required',
            'edit_regx.unique' => 'The Regular Expression string has already been taken',
            'edit_status.required' => 'Regular Expression status is required',
            'edit_status.in' => 'Regular Expression status is invalid'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $label = $request->input('edit_regx_label');
            $regx = $request->input('edit_regx');
            $status = $request->input('edit_status');
            if (!$this->isRegularExpression($regx)){
                return redirect()->back()->withErrors([$regx.' contain invalid regular expression'])->withInput();
            }
            try{
                RegExpression::where(['id'=>$regx_id])->update([
                    'label'=>$label,
                    'regx'=>$regx,
                    'status'=>$status
                ]);
            }catch (\Exception $exception){
                return redirect()->back()->withErrors(['Regular Expression update fail due to '
                    .$exception->getMessage()])
                    ->withInput();
            }
            return redirect()->back()->with('message','Regular Expression update successfully done');
        }
    }

    //delete regular expression
    public function delete_regular_expression(Request $request){
        $validator=  Validator::make($request->all(), [
            'regx_id' => 'required|exists:reg_expressions,id',
        ],[
            'regx_id.required' => 'Regular Expression id is required',
            'regx_id.exists' => 'Regular Expression id is invalid'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            DB::beginTransaction();
            try {
                $regx_id= $request->input('regx_id', 0);
                $regx = RegExpression::find($regx_id);
                Delete::create([
                    'primary_value' => $regx->id,
                    'name' => $regx->getTable(),
                    'others_value' => json_encode($regx)
                ]);
                $regx->delete();
                DB::commit();
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors(['The Regular Expression delete fail due to '
                    .$exception->getMessage()])
                    ->withInput();
            }
            return redirect()->back()->with('message','The Regular Expression delete successfully done');
        }
    }
    //change regular expression status
    public function change_regx_status(Request $request){
        $validator=  Validator::make($request->all(), [
            'regx_id' => 'required|exists:reg_expressions,id',
        ],[
            'regx_id.required' => 'Regular expression id is required',
            'regx_id.exits' => 'Regular expression id is invalid',
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $regx_id = $request->input('regx_id', 0);
            $status = $request->input('status');
            $regx = RegExpression::find($regx_id);
            $regx->status = $status;
            $regx->save();
            echo json_encode(['error'=>'false','message'=>"Status Update Success"]);
        }
    }

    //make new regular expression
    public function make_new_regx(Request $request){
        $validator=  Validator::make($request->all(), [
            'regx_label' => 'required',
            'regx' => 'required|unique:reg_expressions,regx',
            'status' => 'required|in:0,1',
        ],[
            'regx_label.required' => 'Regular Expression label is required',
            'regx.required' => 'Regular Expression string is required',
            'regx.unique' => 'The Regular Expression string has already been taken',
            'status.required' => 'Regular Expression status is required',
            'status.in' => 'Regular Expression status is invalid'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }
        $regx = $request->input('regx');
        $label = $request->input('regx_label');
        $status = $request->input('status');
        if (!$this->isRegularExpression($regx)){
            return redirect()->back()->withErrors([$regx.' contain invalid regular expression'])->withInput();
        }
        try{
            RegExpression::create([
                'label'=>$label,
                'regx'=>$regx,
                'total'=>0,
                'status'=>$status
            ]);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors(['Regular Expression insert fail due to '
                .$exception->getMessage()])
                ->withInput();
        }
        return redirect()->back()->with('message','Regular Expression insert successfully done');
    }
    //test the api
    public function test_short_link(){
        $token = 'bd19201d04d3c6f7a68a92af28ea1c0f6f3c6f4c';
        $long_url = "http://v.whatsapp.com/787425";
        try{
            $url = Bitly::getUrl($long_url,$token);
        }catch (\Exception $exception){
            $url = $exception->getMessage();
        }
        return $url;
    }

    //check the regular expression string is valid or not
    private function isRegularExpression($string) {
        return @preg_match($string, '') !== FALSE;
    }
}
