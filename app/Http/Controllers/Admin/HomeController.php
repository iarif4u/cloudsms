<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Prefix;
use App\PrefixList;
use App\QueueMessage;
use App\ReplaceMessage;
use App\SendMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;

class HomeController extends BaseController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $month_number = date('n', strtotime(Carbon::now()));
        $month = $this->get_month_name($month_number);
        $days = $this->get_days();
        $prefix_list = PrefixList::where(['status' => 1])->get();
        $monthData = [];
        $dailyData = [];
        foreach ($prefix_list as $prefix) {
            $monthData[$prefix->label] = ['color' => $prefix->color, 'data' => $this->get_month_data($prefix->id, $month_number)];
            $dailyData[$prefix->label] = ['color' => $prefix->color, 'data' => $this->get_daily_data($prefix->id)];
        }


        $lineChart = $this->get_line_chart($days, $dailyData);

        $barChart = $this->get_bar_chart($month, $monthData);
        $messageStatus = [
            'Success (' . $this->get_success_message_number() . ')',
            'Fail (' . $this->get_fail_message_number() . ')',
            'Pending (' . $this->get_pending_message_number() . ')',
            'Queue (' . $this->get_queue_message_number() . ')',

        ];
        $pieData = [
            $this->get_success_message_number(),
            $this->get_fail_message_number(),
            $this->get_pending_message_number(),
            $this->get_queue_message_number(),
        ];

        $pieChart = $this->get_pie_chart($messageStatus, $pieData);
        return view('admin.pages.home', ['pieChart' => $pieChart, 'lineChart' => $lineChart, 'barChart' => $barChart,
            'month_number' => $month_number]);
    }


    //get pending message
    public function get_pending_message_number()
    {
        return intval(QueueMessage::where(['status' => 1])->count());
    }


    //get queue messages
    public function get_queue_message_number()
    {
        return intval(QueueMessage::where(['status' => 0])->count());
    }


    //get the number of success message
    public function get_success_message_number()
    {
        $successCount = QueueMessage::where(['status' => 3])->count();
        return intval($successCount);
    }

    //get the number of failed message
    public function get_fail_message_number()
    {
        $failCount = QueueMessage::where(['status' => 2])->count();
        return intval($failCount);
    }

    //get pie chart data for kendo chart
    public function get_pie_chart_data()
    {
        $success = $this->get_success_message_number();
        $fail = $this->get_fail_message_number();
        $queue = $this->get_queue_message_number();
        $pending = $this->get_pending_message_number();
        $total = $success + $fail + $queue + $pending;
        $success_percent = ($total > 0) ? $success * 100 / $total : 0;
        $queue_percent = ($total > 0) ? $queue * 100 / $total : 0;
        $fail_percent = ($total > 0) ? $fail * 100 / $total : 0;
        $pending_percent = ($total > 0) ? $pending * 100 / $total : 0;
        return [
            [
                "source" => "Success ($success)",
                "percentage" => round($success_percent, 2),
                'color' => 'green'
            ],
            [
                "source" => "Fail ($fail)",
                "percentage" => round($fail_percent, 2),
                'color' => 'red'
            ],
            [
                "source" => "Queue ($queue)",
                "percentage" => round($queue_percent, 2),
                'color' => 'black'
            ],
            [
                "source" => "Pending ($pending)",
                "percentage" => round($pending_percent, 2),
                'color' => 'orange'
            ]
        ];
    }

    //get line chart
    public function get_line_chart($days, $datalist)
    {
        $dataset = [];
        foreach ($datalist as $operator => $data) {
            $dataset[] = [
                "label" => $operator . " (" . array_sum($data['data']) . ")",
                'borderColor' => $data['color'],
                "pointBorderColor" => $data['color'],
                "pointBackgroundColor" => $data['color'],
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => $data['color'],
                'data' => $data['data'],
            ];
        }

        return app()->chartjs
            ->name('lineChartTest')
            ->size(['width' => 350, 'height' => 200])
            ->labels($days)
            ->datasets($dataset)
            ->options([]);
    }

    //get bar chart
    public function get_bar_chart($label, $data_list)
    {
        $dataset = [];
        foreach ($data_list as $operator => $data) {
            $dataset[] = [
                "label" => $operator . " (" . array_sum($data['data']) . ")",
                "backgroundColor" => $data['color'],
                'borderColor' => $data['color'],
                "pointBorderColor" => $data['color'],
                "pointBackgroundColor" => $data['color'],
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => $data['color'],
                'data' => $data['data'],
            ];
        }

        return app()->chartjs
            ->name('barChartTest')
            ->type('bar')
            ->size(['width' => 350, 'height' => 130])
            ->labels($label)
            ->datasets($dataset)
            ->options([]);
    }


    //get pie chart
    public function get_pie_chart($label, $data)
    {

        return app()->chartjs
            ->name('pieChartTest')
            ->type('doughnut')
            ->size(['width' => 300, 'height' => 250])
            ->labels($label)
            ->datasets([
                [
                    'backgroundColor' => ['green', 'red', 'orange', 'black'],
                    'hoverBackgroundColor' => ['green', 'red', 'orange', 'black'],
                    'data' => [intval($data[0]), intval($data[1]), intval($data[2]), intval($data[3])]
                ]
            ])->options([]);

    }

    //get last month data for each month total send by prefix
    public function get_month_data($prefix, $month)
    {
        $data = QueueMessage::orderBy('id', 'DESC')
            ->where(['prefix_id' => $prefix])
            ->select('id', 'phone_number', 'created_at')
            ->get()
            ->where("created_at", ">", Carbon::now()->subMonths($month))
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

        $return = array();
        foreach ($data as $value) {
            $return[date('n', strtotime($value[0]->created_at))] = $value->count();
        }


        ksort($return);

        $values = array();
        for ($i = 1; $i <= date('n'); $i++) {
            if (array_key_exists($i, $return)) {
                $values[$i] = $return[$i];
            } else {
                $values[$i] = 0;
            }
        }
        $retun_data = array();
        foreach ($values as $data) {
            $retun_data[] = $data;
        }
        return $retun_data;
    }

    //get last month data for each month total send by prefix
    public function get_daily_data($prefix)
    {

        $data = QueueMessage::orderBy('id', 'DESC')
            ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
            ->where(['prefix_id' => $prefix])
            ->select('id', 'phone_number', 'created_at')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d'); // grouping by months
            });
        $return = array();
        foreach ($data as $value) {
            $return[date('j', strtotime($value[0]->created_at))] = $value->count();
        }

        ksort($return);
        $values = array();
        for ($i = 1; $i <= date('j'); $i++) {
            if (array_key_exists($i, $return)) {
                $values[] = $return[$i];
            } else {
                $values[] = 0;
            }

        }
        return $values;
    }

    //get name of running month till current year
    public function get_month_name($size)
    {
        $month[] = date('F');
        for ($i = 1; $i < $size; $i++) {
            $month[] = date('F', strtotime("-$i month"));
        }
        return array_reverse($month);
    }

    //get the days of current month
    public function get_days()
    {
        $days = [];
        for ($i = 1; $i <= date('j'); $i++) {
            $days[] = Date('D', strtotime(Date('Y-m-') . $i)) . " " . $i;
        }
        return $days;
    }

    //update admin time zone
    public function update_time_zone(Request $request)
    {
        //validate timezone
        $this->validate($request, [
            'time_zone' => 'required|timezone',
        ], [
            'time_zone.required' => "Time Zone is required",
            'time_zone.timezone' => "Time Zone is invalid",
        ]);
        $user = Admin::findOrFail(auth()->user()->getAuthIdentifier());
        $user->time_zone = $request->input('time_zone');

        $user->save();
        if ($user) {
            return redirect()->back()->with('message', "Time zone successfully update to " . $user->time_zone);
        } else {
            return redirect()->back()->withErrors("Time zone failed to update");
        }
    }

    //get admin profile setting view
    public function get_profile_setting()
    {
        $time_zone = array();
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $time_zone[$zone] = $zone . ' ( ' . date('P', $timestamp) . " )";
        }
        $this->timeZoneList = $time_zone;
        return view('admin.pages.profile', ['time_zone' => $time_zone]);
    }

    //update profile setting for admin
    public function update_profile_setting(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'nullable|min:6',
            'phone' => 'required|numeric',
            'time_zone' => 'required|timezone',
        ], [
            'name.required' => 'Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email address is invalid',
            'password.min' => 'Password must be at least 6 character',
            'time_zone.required' => 'Time zone is required',
            'time_zone.timezone' => 'Time zone is is invalid',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        } else {
            $name = $request->input('name');
            $email = $request->input('email');
            $password = $request->input('password');

            $phone = $request->input('phone');
            $time_zone = $request->input('time_zone');
            if (strlen($password) > 0) {
                $update = [
                    'name' => $name,
                    'email' => $email,
                    'password' => Hash::make($password),
                    'phone' => $phone,
                    'time_zone' => $time_zone,
                ];
            } else {
                $update = [
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'time_zone' => $time_zone,
                ];
            }
            try {
                Admin::where(['id' => auth()->user()->getAuthIdentifier()])->update($update);
                return redirect()->back()->with('message', 'Admin profile update successfully done');
            } catch (\Exception $exception) {
                return redirect()->back()->withErrors($exception->getMessage())->withInput();
            }

        }
    }
}
