<?php

namespace App\Http\Controllers\Admin;

use App\Delete;
use App\OtpString;
use App\OtpVendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
class OtpStringController extends BaseController {
    #get otp vendor string
    public function otp_input_string(){
        $otp_string_list =OtpString::with('vendor')->get();
        $vendor_list = OtpVendor::all();
        $vendor_list = $this->make_collection_select($vendor_list,'id','vendor_name');
        $string_list = $this->make_collection_select($otp_string_list,'id','status');
        $deleted_string = Delete::where(['name'=>'otp_string'])->get();
        return view('admin.pages.input_string',['deleted_string'=>$deleted_string,'string_status'=>
            array_count_values($string_list),
            'otp_string_list'=>$otp_string_list,
        'vendor_list'=>$vendor_list]);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list,$id,$name){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->$id] = $data->$name;
        }
        return $select;
    }
    #insert new input string
    public function make_input_string(Request $request,$string_id=null){
        $vendor_id = $request->input('vendor');
        $string = $request->input('input_string');
        $status = $request->input('status');
        $validator=  Validator::make($request->all(), [
            'vendor' => 'required|exists:otp_vendors,id',
            'input_string' => 'required|unique:otp_string,incoming_string,'.$string_id,
            'status' => 'required|in:1,0',
        ],[
            'input_string.required' => 'Input string is required',
            'input_string.unique' => 'Input string has all ready been taken',
            'vendor.required' => 'Vendor is required',
            'vendor.exists' => 'Vendor id doesn\'t match',
            'status.required' => 'String status is required',
            'status.in' => 'String status is invalid',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $input_string =OtpString::create([
                'incoming_string'=>$string,
                'vendor_id'=>$vendor_id,
                'status'=>$status
            ]);
            if ($input_string){
                return redirect()->back()->with('message',"Input String insert successfully done");
            }else{
                return redirect()->back()->withErrors("Input String insert fail")->withInput();
            }
        }
    }

    #change input string status
    public function change_input_string_status(Request $request){
        $string_id = $request->input('string_id');
        $status = $request->input('status');


        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:otp_string,id',
            'status' => 'required|in:0,1',
        ],[
            'string_id.required' => 'Input string not found',
            'string_id.exists' => 'Input string id doesn\'t match',
            'status.in' => 'Invalid status',
            'status.required' => 'Status is required'
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            OtpString::where(['id'=>$string_id])->update(['status'=>$status]);
            echo json_encode(['error'=>'false','message'=>"Status Update Success"]);
        }
    }

    //update input string data
    public function update_input_string(Request $request){
        $string_id = $request->input('string_id');
        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:otp_string,id',
            'vendor' => 'required|exists:otp_vendors,id',
            'input_string' => 'required|unique:otp_string,incoming_string,'.$string_id,
            'status' => 'required|in:1,0',
        ],[
            'string_id.required' => 'Input string id is required',
            'string_id.exists' => 'Input string id doesn\'t match',
            'input_string.required' => 'Input string is required',
            'input_string.unique' => 'Input string has all ready been taken',
            'vendor.required' => 'Vendor is required',
            'vendor.exists' => 'Vendor id doesn\'t match',
            'status.required' => 'String status is required',
            'status.in' => 'String status is invalid',

        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $string = $request->input('input_string');
            $vendor_id = $request->input('vendor');
            $status = $request->input('status');
            $input_string =OtpString::where(['id'=>$string_id])->update([
                'incoming_string'=>$string,
                'vendor_id'=>$vendor_id,
                'status'=>$status
            ]);
            if ($input_string){
                return redirect()->back()->with('message',"Input String update successfully done");
            }else{
                return redirect()->back()->withErrors("Input String insert fail")->withInput();
            }
        }

    }

    #delete string by id
    public function delete_input_string(Request $request){
        $string_id = $request->input('string_id');
        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:otp_string,id',

        ],[
            'string_id.required' => 'Input string id is required',
            'string_id.exists' => 'Input string id doesn\'t match',

        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            DB::beginTransaction();
            try{
                $string =  OtpString::find($string_id);
                Delete::create([
                    'primary_value'=>$string->id,
                    'name'=>$string->getTable(),
                    'others_value'=>json_encode($string)
                ]);
                $string->delete();
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors($exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message','Input String delete successfully done');

        }

    }



}
