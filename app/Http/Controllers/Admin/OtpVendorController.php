<?php

namespace App\Http\Controllers\Admin;

use App\Delete;
use App\OtpString;
use App\OtpVendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class OtpVendorController extends BaseController {
    #get otp vendor list
    public function get_otp_vendor(){
        $otp_vendors = OtpVendor::all();
        $deleted_vendors = Delete::where(['name'=>'otp_vendors'])->get();
        return view('admin.pages.otp_vendor',['deleted_vendors'=>$deleted_vendors,'otp_vendors'=>$otp_vendors]);
    }

    #make an otp vendor
    public function make_otp_vendor(Request $request){
        $validator=  Validator::make($request->all(), [
            'vendorName' => 'required|unique:otp_vendors,vendor_name',
            'status' => 'required|in:1,0',
        ],[
            'vendorName.required' => 'Vendor name is required',
            'vendorName.unique' => 'Vendor name has already been taken',
            'status.required' => 'Vendor status is required',
            'status.in' => 'Vendor status is invalid',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $vendor =OtpVendor::create(['vendor_name'=>$request->input('vendorName'),'status'=>$request->input
            ('status')]);
            if ($vendor){
                return redirect()->back()->with('message',"Vendor insert successfully done");
            }else{
                return redirect()->back()->withErrors("Vendor insert fail")->withInput();
            }
        }
    }

    #change the otp vendor status
    public function change_otp_vendor_status(Request $request){
        $vendor_id = $request->input('vendor_id');
        $status = $request->input('status');

        $validator=  Validator::make($request->all(), [
            'vendor_id' => 'required|exists:otp_vendors,id',
        ],[
            'vendor_id.required' => 'Vendor not found',
            'vendor_id.exists' => 'Vendor id doesn\'t match'
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            OtpVendor::where(['id'=>$vendor_id])->update(['status'=>$status]);
            echo json_encode(['error'=>'false','message'=>"Status Update Success"]);
        }
    }

    #update otp vendor name and status
    public function update_vendor(Request $request){
        $vendor_id = $request->input('vendor_id');
        $validator=  Validator::make($request->all(), [
            'vendor_id' => 'required|exists:otp_vendors,id',
            'vendorName' => 'required|unique:otp_vendors,vendor_name,'.$vendor_id,
            'status' => 'required|in:1,0',
        ],[
            'vendor_id.required' => 'Vendor not found',
            'vendor_id.exists' => 'Vendor id doesn\'t match',
            'vendorName.required' => 'Vendor name is required',
            'vendorName.unique' => 'Vendor name has already been taken',
            'status.required' => 'Vendor status is required',
            'status.in' => 'Vendor status is invalid',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $vendor =OtpVendor::where(['id'=>$vendor_id])->update(['vendor_name'=>$request->input('vendorName'),
                'status'=>$request->input
            ('status')]);
            if ($vendor){
                return redirect()->back()->with('message',"Vendor update successfully done");
            }else{
                return redirect()->back()->withErrors("Vendor update fail")->withInput();
            }
        }
    }

    #delete vendor
    public function delete_otp_vendor(Request $request){

        $vendor_id = $request->input('vendor_id');

        $validator=  Validator::make($request->all(), [
            'vendor_id' => 'required|exists:otp_vendors,id',
        ],[
            'vendor_id.required' => 'Vendor not found',
            'vendor_id.exists' => 'Vendor id doesn\'t match'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all());
        }else{
            try{
                $vendor =  OtpVendor::find($vendor_id);
                Delete::create([
                    'primary_value'=>$vendor->id,
                    'name'=>$vendor->getTable(),
                    'others_value'=>json_encode($vendor)
                ]);
                $vendor->delete();
                return redirect()->back()->with('message','Vendor delete successfully done');
            }catch (\Exception $exception){
                return redirect()->back()->withErrors($exception->getMessage());
            }
        }
    }

}
