<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Delete;
use App\LoginUser;
use App\PrefixList;
use App\UserCapacity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Ixudra\Curl\Facades\Curl;

class CustomersController extends BaseController
{
    //change customer status by ajax request
    public function change_customer_status(Request $request)
    {
        $customer_id = $request->input('customer_id');
        $status = $request->input('status');
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:user,id',
            'status' => 'required|in:0,1',
        ], [
            'customer_id.required' => 'Route not found',
            'customer_id.status' => 'Status not found',
            'customer_id.in' => 'Status is invalid',
            'customer_id.exists' => 'Route id doesn\'t match'
        ]);
        if ($validator->fails()) {
            echo json_encode(['error' => 'true', 'message' => $validator->errors()->all()]);
        } else {
            $route = Customer::find($customer_id);
            $route->status = $status;
            $route->save();
            echo json_encode(['error' => 'false', 'message' => "Status Update Success"]);
        }
    }

    //view customer details
    public function get_customer_view()
    {
        $customer_list = Customer::with('login_data')->get();
        $customer_select = $this->make_collection_select($customer_list, 'id', 'name');
        $previous_customers = Delete::where(['name' => 'user'])->get();
        $prefixs = PrefixList::all();
        $prefixList = array();
        $prefixList[0] = "Select Prefix";
        foreach ($prefixs as $prefix) {
            $prefixList[$prefix->id] = $prefix->prefix;
        }

        return view('admin.pages.customer',
            [
                'previous_customers' => $previous_customers,
                'customer_list' => $customer_list,
                'customer_select' => $customer_select,
                'prefixList' => $prefixList
            ]);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list, $id, $name)
    {
        $select[0] = 'Select One';
        foreach ($data_list as $data) {
            $select[$data->$id] = $data->$name;
        }
        return $select;
    }

    //update customer capacity and capacity status
    public function update_customer_capacity(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'customer_capacity' => 'numeric',
            'capacity_type' => 'required|in:1,2,3'
        ], [
            'customer_capacity.numeric' => 'Capacity amount is invalid',
            'customer_capacity.required' => 'Capacity amount is required',
            'capacity_type.required' => 'Capacity type is required',
            'capacity_type.in' => 'Capacity type is invalid',
            'customer_id.required' => 'Customer name is required',
            'customer_id.exists' => 'Customer not found',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all());
        } else {
            $customer_id = $request->input('customer_id');
            $customer_capacity = $request->input('customer_capacity');
            $capacity_type = $request->input('capacity_type');
            if ($capacity_type == 3) {
                Customer::where(['id' => $customer_id])->decrement('user_capacity', $customer_capacity);
            } else {
                Customer::where(['id' => $customer_id])->increment('user_capacity', $customer_capacity);
            }

            UserCapacity::create([
                'user_id' => $customer_id,
                'capacity' => $customer_capacity,
                'capacity_type' => $capacity_type,  //1 for paid 2 for due and 3 for return
            ]);
            return redirect()->back()->with('message', "User capacity successfully updated");

        }
    }

    //get customer capacity history for datatable by ajax request
    public function customer_capacity_history(Request $request)
    {
        $customer_id = $request->input('customer_id');
        $capacity_history = UserCapacity::where(['user_id' => $customer_id])->get();
        $data = array();
        foreach ($capacity_history as $capacity) {
            if ($capacity->capacity_type == 1) {
                $type = "Paid";
            }
            if ($capacity->capacity_type == 2) {
                $type = "Due";
            }
            if ($capacity->capacity_type == 3) {
                $type = "Return";
            }
            if ($capacity->capacity_type == 0) {
                $type = "Invalid";
            }
            $data[] = [
                date('d-m-Y', strtotime($capacity->updated_at)),
                date('h:m A', strtotime($capacity->updated_at)),
                $capacity->capacity,
                $type
            ];
        }
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    //make new customer
    public function make_new_customer(Request $request)
    {

        $has_error = $this->validate_new_cus_req($request->all());
        if ($has_error) {
            return redirect()->back()->withErrors($has_error)->withInput();
        } else {
            //start transaction
            DB::beginTransaction();
            try {
                // 'smpp_sys_id' => 'string|required_if:is_smpp,1|unique:user,system_id,' . $customer_id,
                //            'smpp_password'
                $customer = DB::table('user')->insertGetId([
                    'name' => $request->input('customerName'),
                    'email' => $request->input('customerEmail'),
                    'password' => $request->input('customerPassword'),
                    'api_key' => Str::random(env('KEY_LENGTH', 48)),
                    'user_capacity' => $request->input('customerBalance'),
                    'alert' => $request->input('customerAlert'),
                    'user_tpm' => $request->input('customerTPM'),
                    'is_smpp' => $request->input('is_smpp'),
                    'smpp_port' => $request->input('smpp_port'),
                    'system_id' => $request->input('smpp_sys_id'),
                    'smpp_password' => $request->input('smpp_password'),
                ]);
                UserCapacity::create(['user_id' => $customer, 'capacity' => $request->input('customerBalance'), 'capacity_type' => 1]);
                LoginUser::create([
                    'id' => $customer,
                    'name' => $request->input('customerName'),
                    'email' => $request->input('customerEmail'),
                    'time_zone' => $request->input('timeZone'),
                    'password' => Hash::make($request->input('customerPassword'))
                ]);
                if ($request->input('is_smpp')) {
                    $response = Curl::to(env('NODE_URL'))
                        ->withData([
                            'system_id' => $request->input('email'),
                            'port' => $request->input('smpp_port'),
                            'password' => $request->input('password')
                        ])
                        ->asJson()
                        ->get();
                    if ($response->status == false) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Something went wrong with smpp")->withInput();
                    }
                }
                //commit transaction
                DB::commit();
                return redirect()->back()->with('message', 'New customer added successfully done');
            } catch (\Exception $exception) {
                //rollback
                DB::rollBack();
                return redirect()->back()->withErrors($exception->getMessage())->withInput();
            }
        }
    }

    //update customer api key

    private function validate_new_cus_req($request, $customer_id = null)
    {
        $validator = Validator::make($request, [
            'customer_id' => 'exists:customers,id',
            'customerName' => ['required','regex:/^([a-zA-Z\']+)$/'],
            'customerEmail' => 'required|email|unique:user,email,' . $customer_id,
            'customerAlert' => 'required|numeric',
            'customerPassword' => 'required',
            'customerBalance' => 'required|numeric',
            'customerTPM' => 'required|numeric',
            'timeZone' => 'required|timezone',
            'is_smpp' => 'required|in:0,1',
            'smpp_port' => 'numeric|required_if:is_smpp,1|unique:user,smpp_port,' . $customer_id,
            'smpp_sys_id' => 'string|required_if:is_smpp,1|unique:user,system_id,' . $customer_id,
            'smpp_password' => 'string|required_if:is_smpp,1,' . $customer_id,
        ], [
            'customer_id.exists' => 'Customer not found',
            'customerName.required' => 'Customer name is required',
            'customerName.regex' => 'Customer name format is invalid',
            'customerEmail.required' => 'Customer email is required',
            'customerEmail.unique' => 'Customer email has already been taken',
            'customerEmail.email' => 'Customer email is invalid',
            'customerAlert.required' => 'Customer SMS alert is required',
            'customerAlert.numeric' => 'Customer SMS alert is must be number',
            'customerBalance.required' => 'Customer SMS balance is required',
            'customerBalance.numeric' => 'Customer SMS balance is must be number',
            'customerTPM.required' => 'Customer SMS TPM is required',
            'customerTPM.numeric' => 'Customer SMS TPM is must be number',
            'customerPassword.required' => 'Customer password is required',
            'timeZone.required' => 'Customer Time Zone is required',
            'timeZone.timezone' => 'Customer Time Zone is invalid',
            'is_smpp.required' => 'Customer SMPP status is required',
            'is_smpp.in' => 'Customer SMPP status is invalid',
            'smpp_port.numeric' => 'SMPP port is invalid',
            'smpp_port.required_if' => 'SMPP port is required',
            'smpp_port.unique' => 'The SMPP port has already been taken another user',
            'smpp_password.required_if' => 'SMPP password is required',
            'smpp_sys_id.required_if' => 'SMPP System Id is required',
            'smpp_password.string' => 'SMPP password contain invalid string',
            'smpp_sys_id.string' => 'SMPP System Id contain invalid string',
            'smpp_sys_id.unique' => 'The SMPP System Id has already been taken another user',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        } else {
            return false;
        }
    }

    //validate new customer request

    public function update_customer_key(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_key' => 'required|exists:user,api_key',
        ], [
            'customer_key.required' => 'Customer not found',
            'customer_key.exists' => 'Customer id doesn\'t match'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 422);
        } else {
            $new_key = Str::random(env('KEY_LENGTH', 48));
            $customer = Customer::where(['api_key' => $request->input('customer_key')])->first();
            $customer->api_key = $new_key;
            $customer->save();
            return response()->json(['status' => true, 'message' => "New api key generate successfully done", 'key' => $new_key]);
        }
    }

    public function update_customer(Request $request)
    {
        $customer_id = $request->input('customer_id');

        $has_error = $this->validate_new_cus_req($request->all(), $customer_id);

        if ($has_error) {
            return redirect()->back()->withErrors($has_error);
        } else {
            DB::beginTransaction();
            try {
                Customer::where(['id' => $customer_id])->update([
                    'email' => $request->input('customerEmail'),
                    'password' => $request->input('customerPassword'),
                    'alert' => $request->input('customerAlert'),
                    'user_tpm' => $request->input('customerTPM'),
                    'is_smpp' => $request->input('is_smpp')
                ]);

                if ($request->input('is_smpp')) {
                    Customer::where(['id' => $customer_id])->update([
                        'system_id' => $request->input('smpp_sys_id'),
                        'smpp_password' => $request->input('smpp_password'),
                        'smpp_port' => $request->input('smpp_port'),
                    ]);
                }

                LoginUser::where(['id' => $customer_id])->update([
                    'name' => $request->input('customerName'),
                    'email' => $request->input('customerEmail'),
                    'time_zone' => $request->input('timeZone'),
                    'password' => Hash::make($request->input('customerPassword'))
                ]);
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();
                return redirect()->back()->withErrors("Something went wrong, Customer update fail");
            }
            return redirect()->back()->with('message', "Customer update successfully done");

        }
    }

    #delete customer by id
    public function delete_customer(Request $request)
    {
        $customer_id = $request->input('customer_id');
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:user,id',
        ], [
            'customer_id.required' => 'Customer not found',
            'customer_id.exists' => 'Customer id doesn\'t match'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all());
        } else {
            $customer = Customer::find($customer_id);

            Delete::create([
                'primary_value' => $customer->id,
                'name' => $customer->getTable(),
                'others_value' => json_encode($customer)
            ]);
            $customer->delete();
            LoginUser::destroy($customer_id);
            return redirect()->back()->with("message", "Customer delete successfully done");
        }
    }


}
