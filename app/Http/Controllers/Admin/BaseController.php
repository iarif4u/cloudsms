<?php

namespace App\Http\Controllers\Admin;

use DateTimeZone;
use App\Http\Controllers\Controller;
use View;

class BaseController extends Controller
{
    public $timeZoneList = array();
    public function __construct()
    {
        $time_zone = array();
        $timestamp = time();
        foreach(timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $time_zone[$zone] = $zone.' ( ' . date('P', $timestamp)." )";
        }
        $this->timeZoneList =$time_zone;

        // Sharing is caring
        View::share('tzlist', $time_zone);
    }
}
