<?php

namespace App\Http\Controllers\Admin;

use App\Delete;
use App\OtpOutputString;
use App\OtpString;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
class OtpOutputStringController extends BaseController {
    //view details of otp output string
    public function otp_output_string(){
        $outgoing_string = OtpOutputString::with(['input_string'])->orderBy('id','desc')->get();
        $otp_string_list =OtpString::all();
        $otp_string_select = $this->make_collection_select($otp_string_list,'id','incoming_string');
        $string_list = $this->make_collection_select($outgoing_string,'id','status');

        $deleted_string = Delete::where(['name'=>'otp_outgoing_string'])->orderBy('id','desc')->get();

        return view('admin.pages.output_string',['deleted_string'=>$deleted_string,
            'otp_string_select'=>$otp_string_select,'string_status'=>
            array_count_values($string_list),
            'outgoing_string'=>$outgoing_string]);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list,$id,$name){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->$id] = $data->$name;
        }
        return $select;
    }

    //insert new output staring against input string
    public function make_otp_output_string(Request $request){
        $validator=  Validator::make($request->all(), [
            'output_string' => 'required',
            'input_string' => 'required|exists:otp_string,id',
            'status' => 'required|in:1,0',
        ],[
            'output_string.required' => 'Output string is required',
            'input_string.required' => 'Input string is required',
            'input_string.exists' => 'Input string doesn\'t match',
            'status.required' => 'String status is required',
            'status.in' => 'String status is invalid',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            try{
                OtpOutputString::create([
                    'incoming_string_id'=>$request->input('input_string'),
                    'replace_string'=>$request->input('output_string'),
                    'counter'=>0,
                    'status'=>$request->input('status')
                ]);
            }catch (\Exception $exception){
                return redirect()->back()->withErrors($exception->getMessage())->withInput();
            }

            return redirect()->back()->with("message","Output string insert successfully done");
        }
    }

    //update output string by id
    public function update_output_string(Request $request){
        $string_id = $request->input('string_id');
        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:otp_outgoing_string,id',
            'output_string' => 'required',
            'input_string' => 'required|exists:otp_string,id',
            'status' => 'required|in:1,0',
        ],[
            'string_id.required' => 'Output string id not found',
            'string_id.exists' => 'Output string id doesn\'t match',
            'output_string.required' => 'Output string is required',
            'input_string.required' => 'Input string is required',
            'input_string.exists' => 'Input string doesn\'t match',
            'status.required' => 'String status is required',
            'status.in' => 'String status is invalid',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            try{
                OtpOutputString::where(['id'=>$string_id])->update([
                    'incoming_string_id'=>$request->input('input_string'),
                    'replace_string'=>$request->input('output_string'),
                    'status'=>$request->input('status')
                ]);
            }catch (\Exception $exception){
                return redirect()->back()->withErrors($exception->getMessage())->withInput();
            }

            return redirect()->back()->with("message","Output string update successfully done");
        }
    }

    //delete the output string by it's id
    public function delete_output_string(Request $request){
        $string_id = $request->input('string_id');
        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:otp_outgoing_string,id',

        ],[
            'string_id.required' => 'Output string id is required',
            'string_id.exists' => 'Output string id doesn\'t match',

        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            DB::beginTransaction();
            try{
                $string =  OtpOutputString::find($string_id);
                Delete::create([
                    'primary_value'=>$string->id,
                    'name'=>$string->getTable(),
                    'others_value'=>json_encode($string)
                ]);
                $string->delete();
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors($exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message','Output String delete successfully done');

        }

    }
    public function change_output_string_status(Request $request){
        $string_id = $request->input('string_id');
        $status = $request->input('status');
        $validator=  Validator::make($request->all(), [
            'string_id' => 'required|exists:otp_outgoing_string,id',

        ],[
            'string_id.required' => 'Output string id is required',
            'string_id.exists' => 'Output string id doesn\'t match',

        ]);

        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            DB::beginTransaction();
            try{
                OtpOutputString::where(['id'=>$string_id])->update(['status'=>$status]);
            }catch (\Exception $exception){
                DB::rollback();
                echo json_encode(['error'=>'true','message'=>$exception->getMessage()]);
            }
            DB::commit();
            echo json_encode(['error'=>'false','message'=>"Status Update Success"]);

        }
    }
}
