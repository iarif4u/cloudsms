<?php

namespace App\Http\Controllers\Admin;

use App\CallBack;
use Illuminate\Http\Request;

class SMPPController extends BaseController
{
    //get the whole smpp message report
    public function get_smpp_report(){
        $messages = CallBack::with(['message_data'])->orderBy('id','desc')->get();
        return view('admin.reports.smpp',['messages'=>$messages]);
    }
}
