<?php

namespace App\Http\Controllers\Admin;

use App\Prefix;
use App\PrefixHistory;
use App\PrefixList;
use App\RoutePrefix;
use App\RoutePrefixHistory;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PrefixController extends BaseController
{
    //get view of prefix setting
    public function get_prefix_settings_view()
    {
        $prefixList = PrefixList::all();
        return view('admin.prefix.prefix_settings', ['prefixList' => $prefixList]);
    }

    public function change_prefix_status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean',
            'prefix_id' => 'required|exists:prefix_lists,id',
        ], [
            'prefix_id.required' => 'Prefix is required',
            'prefix_id.exists' => 'Prefix is invalid',
            'status.required' => 'Prefix status is required',
            'status.boolean' => 'Prefix status is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 403);
        } else {
            PrefixList::where(['id' => $request->input('prefix_id')])->update(['status' => $request->input('status')]);
            return response()->json(['Prefix status update success']);
        }
    }

    #update prefix settings
    public function update_prefix_settings(Request $request)
    {
        $prfix_id = $request->input('prefix_id', 0);
        $validator = Validator::make($request->all(), [
            'prefix_id' => 'required|exists:prefix_lists,id',
            'label' => 'required|unique:prefix_lists,label,' . $prfix_id,
            'prefix' => 'required|unique:prefix_lists,prefix,' . $prfix_id,
            'color' => 'required|unique:prefix_lists,color,' . $prfix_id,
        ], [
            'label.required' => 'Label is required',
            'label.unique' => 'Label has already been taken',
            'prefix.array' => 'Prefix is invalid',
            'prefix.required' => 'Prefix is required',
            'prefix.unique' => 'Prefix has already been taken',
            'color.required' => 'Prefix color is required',
            'color.unique' => 'Prefix color has already been taken',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        } else {
            $color_validator = Validator::make(['color' => $request->input('color')], ['color' => 'color_hex']);
            if ($color_validator->fails()) {
                return redirect()->back()->withErrors($validator->errors()->all())->withInput();
            } else {
                PrefixList::where(['id' => $prfix_id])->update([
                    "label" => $request->input('label'),
                    "prefix" => $request->input('prefix'),
                    "color" => $request->input('color'),
                    "status" => true,
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]);
                return redirect()->back()->with('message', "Prefix update successfully done");
            }
        }
    }

    public function set_prefix_settings(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'label' => 'required|array',
            'prefix' => 'required|array',
            'prefix.*' => 'required|unique:prefix_lists,prefix|distinct',
            'color' => 'required|unique:prefix_lists,color',
        ], [
            'label.required' => 'Label is required',
            'label.array' => 'Label is invalid',
            'prefix.array' => 'Prefix is invalid',
            'prefix.required' => 'Prefix is required',
            'prefix.*.required' => 'Prefix is required',
            'prefix.*.unique' => 'Prefix has already been taken',
            'prefix.*.distinct' => 'Prefix has dublicate value',
            'color.required' => 'Prefix color is required',
            'color.unique' => 'Prefix color has already been taken',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        } else {
            $color_validator = Validator::make(['color' => $request->input('color')], ['color' => 'color_hex']);
            if ($color_validator->fails()) {
                return redirect()->back()->withErrors($validator->errors()->all())->withInput();
            } else {
                $lable = $request->input('label');
                $prefix = $request->input('prefix');
                $prifixs = array();
                foreach ($prefix as $sl => $index) {
                    $prifixs[] = [
                        "label" => $lable[$sl],
                        "prefix" => $index,
                        "color" => $request->input('color'),
                        "status" => true,
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now(),
                    ];
                }
                PrefixList::insert($prifixs);
                return redirect()->back()->with('message', "Prefix insert successfully done");
            }
        }
    }

    #set customer prefix with rate
    public function set_customer_prefix(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:user,id',
            'prefix' => 'required',
            'rate' => 'required',
            'prefix.*' => 'required',
            'rate.*' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ], [
            'customer_id.required' => 'Customer is required',
            'customer_id.exists' => 'Customer id is invalid',
            'prefix.required' => 'Prefix is required',
            'rate.required' => 'Prefix rate is required',
            'rate.*.required' => 'Prefix rate is invalid',
            'rate.*.regex' => 'Prefix rate is non numeric or invalid',
            'prefix.*.required' => 'Prefix is invalid',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        } else {
            $customer_id = $request->input('customer_id');
            $prefix = $request->input('prefix');
            $rate = $request->input('rate');

            DB::beginTransaction();
            try {
                $prefix_list = array();
                for ($i = 0; $i < sizeof($prefix); $i++) {
                    $prefix_data = Prefix::where([
                        'customer_id' => $customer_id,
                        'prefix' => $prefix[$i],
                        'status' => 1
                    ])->first();
                    if ($prefix_data) {
                        $prefix_list[] = $prefix_data->id;
                        if ($prefix_data->rate != $rate[$i]) {
                            PrefixHistory::create([
                                'customer_id' => $customer_id,
                                'prefix' => $prefix[$i],
                                'rate' => $rate[$i],
                            ]);
                            $prefix_data->rate = $rate[$i];
                            $prefix_data->save();
                        }
                    } else {
                        PrefixHistory::create([
                            'customer_id' => $customer_id,
                            'prefix' => $prefix[$i],
                            'rate' => $rate[$i],
                        ]);
                        $new_prefix = Prefix::create([
                            'customer_id' => $customer_id,
                            'prefix' => $prefix[$i],
                            'rate' => $rate[$i],
                            'status' => 1,
                        ]);
                        $prefix_list[] = $new_prefix->id;
                    }
                }
                $drop_prefix = Prefix::whereNotIn('id', $prefix_list)->where(['customer_id' => $customer_id])->get();

                foreach ($drop_prefix as $drop) {
                    $history_prefix = PrefixHistory::where(['customer_id' => $customer_id, 'prefix' => $drop->prefix,
                        'rate' => $drop->rate])->first();
                    if ($history_prefix) {
                        $history_prefix->is_delete = 1;
                        $history_prefix->save();

                    }
                    $drop->delete();
                }

            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->withErrors('Customer prefix update fail ' . $e->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message', 'Customer prefix update success');
        }
    }

    //get prefix rate list
    public function get_customer_prefix_rate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:user,id',
        ], [
            'customer_id.required' => 'Customer is required',
            'customer_id.exists' => 'Customer id is invalid',
        ]);
        if ($validator->fails()) {
            echo json_encode(['error' => 'true', 'message' => $validator->errors()->all()]);
        } else {
            $customer_id = $request->input('customer_id');
            $prefix = Prefix::with('prefix_info')->where(['customer_id' => $customer_id])->get();
            echo json_encode(['error' => 'false', 'message' => $prefix]);
        }
    }

    //set route prefix rate
    public function set_route_prefix(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'route_id' => 'required|exists:route_api,route',
            'prefix' => 'required',
            'rate' => 'required',
            'prefix.*' => 'required',
            'rate.*' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ], [
            'route_id.required' => 'Route is required',
            'route_id.exists' => 'Route id is invalid',
            'prefix.required' => 'Prefix is required',
            'rate.required' => 'Prefix rate is required',
            'rate.*.required' => 'Prefix rate is invalid',
            'rate.*.regex' => 'Prefix rate is non numeric or invalid',
            'prefix.*.required' => 'Prefix is invalid',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        } else {
            $route_id = $request->input('route_id');
            $prefix = $request->input('prefix');
            $rate = $request->input('rate');
            DB::beginTransaction();
            try {
                $prefix_list = array();
                for ($i = 0; $i < sizeof($prefix); $i++) {
                    $prefix_data = RoutePrefix::where([
                        'route_id' => $route_id,
                        'prefix' => $prefix[$i],
                        'status' => 1
                    ])->first();
                    if ($prefix_data) {
                        $prefix_list[] = $prefix_data->id;
                        if ($prefix_data->rate != $rate[$i]) {
                            RoutePrefixHistory::create([
                                'route_id' => $route_id,
                                'prefix' => $prefix[$i],
                                'rate' => $rate[$i],
                                'is_delete' => 0,
                            ]);
                            $prefix_data->rate = $rate[$i];
                            $prefix_data->save();
                        }
                    } else {
                        RoutePrefixHistory::create([
                            'route_id' => $route_id,
                            'prefix' => $prefix[$i],
                            'rate' => $rate[$i],
                            'is_delete' => 0,
                        ]);
                        $new_prefix = RoutePrefix::create([
                            'route_id' => $route_id,
                            'prefix' => $prefix[$i],
                            'rate' => $rate[$i],
                            'status' => 1,
                        ]);
                        $prefix_list[] = $new_prefix->id;
                    }
                }
                $drop_prefix = RoutePrefix::whereNotIn('id', $prefix_list)->where(['route_id' => $route_id])->get();
                foreach ($drop_prefix as $drop) {
                    $history_prefix = RoutePrefixHistory::where(['route_id' => $route_id, 'prefix' => $drop->prefix,
                        'rate' => $drop->rate])->first();
                    $history_prefix->is_delete = 1;
                    $history_prefix->save();
                    $drop->delete();
                }
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->withErrors('Route prefix update fail ' . $e->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message', 'Route prefix update success');
        }
    }

    //get route prefix rate by ajax request
    public function get_route_prefix_rate(Request $request)
    {
        if ($request->ajax()) {
            $validator = Validator::make($request->all(), [
                'route_id' => 'required|exists:route_api,route',
            ], [
                'route_id.required' => 'Route is required',
                'route_id.exists' => 'Route id is invalid',
            ]);
            if ($validator->fails()) {
                echo json_encode(['error' => 'true', 'message' => $validator->errors()->all()]);
            } else {
                $route_id = $request->input('route_id');
                $prefix = RoutePrefix::with(['prefix_info'])->where(['route_id' => $route_id])->get();
                echo json_encode(['error' => 'false', 'message' => $prefix]);
            }
        }
    }
}
