<?php

namespace App\Http\Controllers\Admin;

use App\Delete;
use App\PrefixList;
use App\RouteAuth;
use DB;
use App\Customer;
use App\Route;
use App\RouteCapacity;
use App\UserRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ixudra\Curl\Facades\Curl;

class RouteController extends BaseController
{
    #get route view
    public function get_route_view()
    {
        $route_list = Route::all();
        $route_list_select = $this->make_collection_select($route_list, 'route', 'route_name');
        $deleted_routes = Delete::where(['name' => 'route_api'])->get();
        $prefixs = PrefixList::all();
        $prefixList = array();
        $prefixList[0] = "Select Prefix";
        foreach ($prefixs as $prefix) {
            $prefixList[$prefix->id] = $prefix->prefix;
        }
        return view('admin.pages.route', ['deleted_routes' => $deleted_routes, 'route_list' => $route_list,'prefixList'=>$prefixList,
            'route_list_select' => $route_list_select]);
    }

    public function get_smsbox_route_view(){
        $route_list = Route::where('is_sms_box','=',1)->get();
        $route_list_select = $this->make_collection_select($route_list, 'route', 'route_name');
        $deleted_routes = Delete::where(['name' => 'route_api'])->get();
        $prefixs = PrefixList::all();
        $prefixList = array();
        $prefixList[0] = "Select Prefix";
        foreach ($prefixs as $prefix) {
            $prefixList[$prefix->id] = $prefix->prefix;
        }
        return view('admin.pages.route_box', ['deleted_routes' => $deleted_routes, 'route_list' => $route_list,'prefixList'=>$prefixList,
            'route_list_select' => $route_list_select]);
    }

    //add new capacity to route
    public function add_route_capacity(Request $request)
    {
        $route_id = $request->input('route_id');
        $routeCapacity = $request->input('routeCapacity');
        $validator = Validator::make($request->all(), [
            'route_id' => 'required|exists:route_api,route',
            'routeCapacity' => 'required|numeric',
        ], [
            'route_id.route_id' => 'Route id is required',
            'route_id.exists' => 'Route not found',
            'routeCapacity.required' => 'Route capacity is required',
            'routeCapacity.numeric' => 'Route capacity must be numeric',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all());
        } else {

            DB::beginTransaction();

            try {
                // Validate, then create if valid
                RouteCapacity::create(['route_id' => $route_id, 'route_capacity' => $routeCapacity]);
            } catch (ValidationException $e) {
                // Rollback and then redirect
                // back to form with errors
                DB::rollback();
                return redirect()->back()->withErrors($e->getMessage())->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->withErrors($e->getMessage())->withInput();
            }
            try {
                // Validate, then create if valid
                Route::where(['route' => $route_id])->increment('route_capacity', $routeCapacity);
            } catch (ValidationException $e) {
                // Rollback and then redirect
                // back to form with errors
                DB::rollback();
                return redirect()->back()->withErrors($e->getMessage())->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->withErrors($e->getMessage())->withInput();
            }
            DB::commit();
            return redirect()->back()->with('message', "Route capacity successfully update");

        }
    }

    //get route capacity history to datatable
    public function route_capacity_history(Request $request)
    {
        $route_id = $request->input('route_id');
        $capacityList = RouteCapacity::where(['route_id' => $route_id])->get();
        $data = array();
        foreach ($capacityList as $capacity) {
            $schedule_date = new \DateTime($capacity->created_at);
            $schedule_date->setTimeZone(new \DateTimeZone(auth()->user()->time_zone));
            $data[] = [
                $capacity->route_capacity,
                $schedule_date->format('d-m-Y'),
                $schedule_date->format('h:m A'),
            ];
        }
        $output = array(
            'draw' => intval($request->input('draw')),
            'recordsTotal' => $capacityList->count(),
            'data' => $data,
        );
        echo json_encode($output);
    }

    //check smpp status
    public function check_smpp_status(Request $request)
    {
        $smpp_server = $request->input('server');
        $smpp_port = $request->input('port');
        $smpp_user = $request->input('user');
        $smpp_pass = $request->input('password');
        $response = Curl::to(env('NODE_URL') . "/check")
            ->withData([
                'server' => $smpp_server,
                'port' => $smpp_port,
                'user' => $smpp_user,
                'password' => $smpp_pass,
            ])->get();
        $smpp_status = ($response) ? json_decode($response)->status : false;
        Route::where([
            'server' => $smpp_server,
            'method' => "SMPP",
            'user' => $smpp_user,
            'password' => $smpp_pass,
            'port' => $smpp_port
        ])->update(['smpp_status' => $smpp_status]);
        return response()->json($response);
    }

    #make new route
    public function make_route(Request $request)
    {

        $has_error = $this->validate_new_route_req($request);
        if ($has_error) {
            return redirect()->back()->withErrors($has_error)->withInput();
        } else {
            $smpp_status = false;
            if ($request->input('routeMethod') == "SMPP") {
                $smpp_server = $request->input('smpp_server');
                $smpp_port = $request->input('smpp_port');
                $smpp_user = $request->input('smppUser');
                $smpp_pass = $request->input('smppPassword');
                $response = Curl::to(env('NODE_URL') . "/check")
                    ->withData([
                        'server' => $smpp_server,
                        'port' => $smpp_port,
                        'user' => $smpp_user,
                        'password' => $smpp_pass,
                    ])->asJson()->get();
                $smpp_status = ($response) ? $response->status : false;

            }
            DB::beginTransaction();
            try {

                $route = Route::create([
                    'route_name' => $request->input('routeName'),
                    'api' => $request->input('routeAPI', null),
                    'server' => $request->input('smpp_server'),
                    'method' => $request->input('routeMethod'),
                    'user' => $request->input('smppUser', null),
                    'password' => $request->input('smppPassword', null),
                    'port' => $request->input('smpp_port', null),
                    'smpp_status' => $smpp_status,
                    'route_capacity' => $request->input('routeCapacity'),
                    'alert' => $request->input('routeAlert'),
                    'tpm' => $request->input('routeTPM'),
                    'bar' => $request->input('routeBar'),
                    'status' => $request->input('routeStatus'),
                ]);
                if ($request->input('basicAuth')) {
                    RouteAuth::create(["username" => $request->input('basicAuthUser'), "password" => $request->input('basicAuthPass'), "route_id" => $route->route]);
                }
                try {
                    RouteCapacity::create(['route_id' => $route->route, 'route_capacity' => $request->input('routeCapacity')]);
                } catch (\Exception $exception) {
                    DB::rollback();
                    return redirect()->back()->withErrors($exception->getMessage())->withInput();
                }
            } catch (\Exception $exception) {
                DB::rollback();
                return redirect()->back()->withErrors($exception->getMessage())->withInput();
            }
            if ($route) {
                DB::commit();
                return redirect()->back()->with('message', "New route insert success");
            } else {
                DB::rollback();
                return redirect()->back()->withErrors("New route insert failed")->withInput();
            }
        }
    }

    //validate new route request
    private function validate_new_route_req($request, $route_id = 0)
    {

        $validator = Validator::make($request->all(), [
            'route_id' => 'exists:route_api,route',
            'routeName' => 'required|unique:route_api,route_name,' . $route_id . ',route',
            'routeCapacity' => 'required|numeric',
            'routeTPM' => 'required|numeric',
            'routeAlert' => 'required|numeric',
            'routeBar' => 'required|numeric',
            'routeStatus' => 'required|numeric',
            'basicAuth' => 'nullable|required_if:routeMethod,POST|required_if:routeMethod,GET',
            'basicAuthUser' => 'nullable|required_if:basicAuth,1',
            'basicAuthPass' => 'nullable|required_if:basicAuth,1',
            'routeAPI' => 'nullable|required_if:routeMethod,POST|required_if:routeMethod,GET|url',
            'routeMethod' => 'required',
            'smpp_port' => 'nullable|numeric|required_if:routeMethod,SMPP',
            'smpp_server' => 'required_if:routeMethod,SMPP',
            'smppUser' => 'required_if:routeMethod,SMPP',
            'smppPassword' => 'required_if:routeMethod,SMPP',
        ], [
            'route_id.exists' => 'Route not found',
            'routeName.required' => 'Route name is required',
            'routeName.unique' => 'Route name has already been taken',
            'routeCapacity.required' => 'Route capacity is required',
            'routeCapacity.numeric' => 'Route capacity must be numeric',
            'routeTPM.required' => 'Route TPM is required',
            'routeAlert.required' => 'Route alert is required',
            'routeTPM.numeric' => 'Route TPM must be numeric',
            'routeAlert.numeric' => 'Route alert must be numeric',
            'routeBar.required' => 'Route bar is required',
            'routeBar.numeric' => 'Route bar must be numeric',
            'routeStatus.required' => 'Route status is required',
            'routeStatus.numeric' => 'Route status is invalid',
            'routeAPI.required_if' => 'Route API is required',
            'basicAuthUser.required_if' => 'Route basic auth user is required',
            'basicAuthPass.required_if' => 'Route basic auth password is required',
            'routeAPI.url' => 'Route API is invalid URL',
            'routeMethod.required' => 'Route API request method is required',
            'basicAuth.required' => 'Route basic auth is required',
            'smpp_port.required_if' => 'SMPP port no is required',
            'smpp_port.numeric' => 'SMPP port no is invalid',
            'smpp_server.required_if' => 'SMPP server is required',
            'smppPassword.required_if' => 'SMPP server is required',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        } else {
            return false;
        }
    }

    #update route details by post request
    public function update_route(Request $request)
    {
        $route_id = $request->input('update_route_id');
        $has_error = $this->validate_new_route_req($request, $route_id);
        if ($has_error) {
            return redirect()->back()->withErrors($has_error)->withInput();
        } else {
            $route = Route::where(['route' => $route_id])->update([
                'route_name' => $request->input('routeName'),
                'api' => $request->input('routeAPI'),
                'route_capacity' => $request->input('routeCapacity'),
                'alert' => $request->input('routeAlert'),
                'method' => $request->input('routeMethod'),
                'tpm' => $request->input('routeTPM'),
                'bar' => $request->input('routeBar'),
                'status' => $request->input('routeStatus')
            ]);
            if ($route) {
                return redirect()->back()->with('message', "Route update success");
            } else {
                return redirect()->back()->withErrors("Route update failed")->withInput();
            }
        }
    }

    #delete a route by route id
    public function delete_route(Request $request)
    {
        $route_id = $request->input('route_id');
        $validator = Validator::make($request->all(), [
            'route_id' => 'required|exists:route_api,route',
        ], [
            'route_id.required' => 'Route not found',
            'route_id.exists' => 'Route id doesn\'t match'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all());
        } else {

            $route = Route::find($route_id);
            Delete::create([
                'primary_value' => $route->route,
                'name' => $route->getTable(),
                'others_value' => json_encode($route)
            ]);
            $route->delete();
            return redirect()->back()->with("message", "Route delete successfully done");
        }
    }

    #change status of route
    public function change_status(Request $request)
    {
        $route_id = $request->input('route_id');
        $route_status = $request->input('status');
        $validator = Validator::make($request->all(), [
            'route_id' => 'required|exists:route_api,route',
        ], [
            'route_id.required' => 'Route not found',
            'route_id.exists' => 'Route id doesn\'t match'
        ]);
        if ($validator->fails()) {
            echo json_encode(['error' => 'true', 'message' => $validator->errors()->all()]);
        } else {
            $route = Route::find($route_id);
            $route->status = $route_status;
            $route->save();
            echo json_encode(['error' => 'false', 'message' => "Status Update Success"]);
        }
    }

    #get view of route assign to customer
    public function get_route_assign()
    {
        $customer_list = Customer::all();
        $route_list = Route::all();
        $customer_list = $this->make_collection_select($customer_list, 'id', 'name');
        $route_list = $this->make_collection_select($route_list, 'route', 'route_name');
        $route_assign_list = UserRoute::with(['customer', 'route'])->orderBy('user_id', 'asc')->get();

        return view('admin.pages.route_assign', ['route_assign_list' => $route_assign_list,
            'customer_list' => $customer_list,
            'route_list' => $route_list]);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list, $id, $name)
    {
        $select[0] = 'Select One';
        foreach ($data_list as $data) {
            $select[$data->$id] = $data->$name;
        }
        return $select;
    }

    #make new route assign for customer
    public function make_route_assign(Request $request)
    {
        $has_error = $this->validate_new_route_assign_req($request);
        if ($has_error) {
            return redirect()->back()->withErrors($has_error)->withInput();
        } else {
            $route_assign = UserRoute::create([
                'user_id' => $request->input('customer_id'),
                'route_id' => $request->input('route_id'),
                'capacity' => 0,
                'count' => 0,
                'total_send' => 0,
                'tpm' => $request->input('routeTPM'),
                'status' => $request->input('routeStatus')
            ]);
            if ($route_assign) {
                return redirect()->back()->with('message', "Route assign successfully done");
            } else {
                return redirect()->back()->withErrors("Route assign to user failed");
            }
        }
    }

    //validate new route request
    private function validate_new_route_assign_req($request, $assign_id = 0)
    {

        $user_route = UserRoute::where(['user_id' => $request->input('customer_id'), 'route_id' => $request->input('route_id')])
            ->whereNotIn('id', [$assign_id])
            ->get()
            ->count();
        if ($user_route > 0) {
            return ['User has already assigned this route'];
        }

        $validator = Validator::make($request->all(), [
            'update_route_assign_id' => 'exists:user_route_api,id',
            'route_id' => 'required|exists:route_api,route',
            'customer_id' => 'required|exists:user,id',
            'routeTPM' => 'required|numeric',
            'routeStatus' => 'required|in:0,1'
        ], [
            'update_route_assign_id.required' => 'Route assign record is required',
            'update_route_assign_id.exists' => 'Route assign record not found',
            'route_id.required' => 'Route is required',
            'route_id.exists' => 'Route not found',
            'customer_id.required' => 'Customer name is required',
            'customer_id.exists' => 'Customer name  not found',
            'routeTPM.required' => 'Route assign TPM is required',
            'routeTPM.numeric' => 'Route assign TPM is invalid',
            'routeStatus.required' => 'Route assign status is required',
            'routeStatus.in' => 'Route assign status is invalid',
        ]);
        if ($validator->fails()) {

            return $validator->errors()->all();
        } else {
            return false;
        }
    }

    //update customer route assign info
    public function update_route_assign(Request $request)
    {
        $assign_id = $request->input('update_route_assign_id');
        $has_error = $this->validate_new_route_assign_req($request, $assign_id);
        if ($has_error) {
            return redirect()->back()->withErrors($has_error)->withInput();
        } else {
            $route_assign = UserRoute::where(['id' => $assign_id])->update([
                'user_id' => $request->input('customer_id'),
                'route_id' => $request->input('route_id'),
                'capacity' => 0,
                'count' => 0,
                'total_send' => 0,
                'tpm' => $request->input('routeTPM'),
                'status' => $request->input('routeStatus')
            ]);
            if ($route_assign) {
                return redirect()->back()->with('message', "Route assign successfully update");
            } else {
                return redirect()->back()->withErrors("Route assign to update failed");
            }
        }
    }

    //delete route assign by post request
    public function delete_route_assign(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'route_assign_id' => 'required|exists:user_route_api,id',
        ], [
            'route_assign_id.required' => 'Route assign record is required',
            'route_assign_id.exists' => 'Route assign record not found',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all());
        } else {
            $assign_id = $request->input('route_assign_id');
            $assign = UserRoute::find($assign_id);
            Delete::create([
                'primary_value' => $assign->id,
                'name' => $assign->getTable(),
                'others_value' => json_encode($assign)
            ]);
            $assign->delete();

            return redirect()->back()->with("message", "Route delete successfully done");
        }
    }

    #update route with assign customer
    public function update_route_assign_status(Request $request)
    {
        $route_id = $request->input('route_id');
        $route_status = $request->input('status');

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|exists:user_route_api,id',
            'status' => 'required|in:0,1'
        ], [
            'route_id.required' => 'Route assign record is required',
            'route_id.exists' => 'Route assign record not found',
            'status.required' => 'Route assign status not found',
            'status.in' => 'Route assign status invalid',
        ]);
        if ($validator->fails()) {
            echo json_encode(['error' => 'true', 'message' => $validator->errors()->all()]);
        } else {
            $route = UserRoute::find($route_id);
            $route->status = $route_status;
            $route->save();
            echo json_encode(['error' => 'false', 'message' => "Status Update Success"]);
        }
    }

}
