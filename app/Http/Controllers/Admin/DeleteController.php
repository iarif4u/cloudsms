<?php

namespace App\Http\Controllers\Admin;

use App\Delete;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class DeleteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Delete  $delete
     * @return \Illuminate\Http\Response
     */
    public function show(Delete $delete)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Delete  $delete
     * @return \Illuminate\Http\Response
     */
    public function edit(Delete $delete)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Delete  $delete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Delete $delete)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Delete  $delete
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delete $delete)
    {
        //
    }
}
