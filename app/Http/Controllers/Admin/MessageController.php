<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\PrefixList;
use App\QueueMessage;
use App\ReplaceMessage;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class MessageController extends BaseController
{
    public function convertTimeToUSERzone($str, $format = 'Y-m-d H:i:s')
    {
        $userTimezone = auth()->user()->time_zone;
        if (empty($str)) {
            return '';
        }

        $new_str = new DateTime($str, new DateTimeZone('UTC'));
        $new_str->setTimeZone(new DateTimeZone($userTimezone));
        return $new_str->format($format);
    }

    #get daily report details view
    public function get_daily_report(Request $request)
    {
        $date = ($request->method() == "POST") ? date('Y-m-d', strtotime($request->input('selectDate'))) : date('Y-m-d');
        $to = ($request->method() == "POST") ? date('Y-m-d', strtotime($request->input('selectDate'))) : date('Y-m-d');
        $date = Carbon::parse($date, auth()->user()->time_zone);
        $date->setTimezone('UTC');
        $to = Carbon::parse($to, auth()->user()->time_zone);
        $to->setTimezone('UTC');
        $to->addDay();
        $reportData = QueueMessage::with(['route', 'user'])
            ->where('created_at', '>=', $date)
            ->where('created_at', '<', $to)
            ->orderBy('id', 'desc')
            ->get();
        $dataList = array();
        $color_list = array();
        $prefix_list = PrefixList::all();
        foreach ($prefix_list as $allowed_prefix) {
            $dataList[$allowed_prefix->label] = $allowed_prefix->id;
            $color_list[$allowed_prefix->label] = $allowed_prefix->color;
        }

        //dd($dataList);
        $hour_data = $this->get_msg_by_prefix($dataList, $date);
        //dd($hour_data);
        $chartjs = $this->get_line_chart(array_keys($dataList), $color_list, $hour_data);
        return view('admin.reports.daily', ['reportData' => $reportData, 'chartjs' => $chartjs, 'date' => ($request->method() == "POST") ? date('Y-m-d', strtotime($request->input('selectDate'))) : date('Y-m-d')]);
    }

    //get line chart

    public function get_msg_by_prefix($prefixList, $date)
    {
        $to = Carbon::parse($date);

        $returList = array();
        foreach ($prefixList as $key => $prefix) {

            $data = QueueMessage::orderBy('id', 'DESC')
                ->where(['prefix_id' => $prefix])
                ->select('id', 'phone_number', 'created_at')
                ->whereBetween('created_at', [$date, $to->addDay()])
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at, auth()->user()->time_zone)->format('G');
                    //grouping by houre
                });

            $return = array();
            foreach ($data as $value) {
                $time = $this->get_time_by_timezone($value[0]->created_at, auth()->user()->time_zone, 'G');
                $return[$time] = $value->count();
            }
            ksort($return);
            $values = array();
            $schedule_date = new \DateTime();
            $schedule_date->setTimeZone(new \DateTimeZone(auth()->user()->time_zone));

            $timestamp = (date('Y-m-d') == $date) ? $schedule_date->format('G') : 23;
            for ($i = 0; $i <= $timestamp; $i++) {
                if (array_key_exists($i, $return)) {
                    $values[] = $return[$i];
                } else {
                    $values[] = 0;
                }
            }

            $returList[$key] = $values;
        }

        return $returList;
    }

    //get message number by prefix

    public function get_time_by_timezone($time, $timezone, $fomat)
    {
        $triggerOn = $time;
        $schedule_date = new \DateTime($triggerOn);
        $schedule_date->setTimeZone(new \DateTimeZone($timezone));
        return $schedule_date->format($fomat);
    }

    //get time by time zone

    public function get_line_chart($labels, $color_list, $data)
    {
        $time = array();
        foreach ($data[$labels[0]] as $key => $send) {
            $time[] = date('h:i A', strtotime("$key:00"));
        }
        $chart_data = array();
        foreach ($labels as $label) {
            $chart_data[] = [
                "label" => $label,
                'borderColor' => $color_list[$label],
                'backgroundColor' => $color_list[$label],
                "pointBorderColor" => "#00c0ef",
                "pointBackgroundColor" => $color_list[$label],
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "#00c0ef",
                'data' => $data[$label],
            ];
        }
        //  dd($chart_data);
        return app()->chartjs
            ->name('lineChartTest')
            ->type('bar')
            ->size(['width' => 350, 'height' => 120])
            ->labels($time)
            ->datasets($chart_data)
            ->options([]);
    }

    //get date range report view

    public function get_range_report_view()
    {
        return view('admin.reports.range');
    }

    //get date range report details between two dates
    public function get_range_report(Request $request)
    {
        $date_range = $request->input('selectDate');
        $string = explode('-', $date_range);
        $from_date = str_replace("/", '-', trim($string[0]));
        $to_date = str_replace("/", '-', trim($string[1]));
        $to_date = Carbon::parse($to_date);
        $to_date->setTimezone(auth()->user()->time_zone);


        $from_date = Carbon::parse($from_date);
        $from_date->setTimezone(auth()->user()->time_zone);

        $dataList = array();
        $color_list = array();
        $prefix_ist = PrefixList::all();
        foreach ($prefix_ist as $allowed_prefix) {
            $dataList[$allowed_prefix->label] = $allowed_prefix->id;
            $color_list[$allowed_prefix->label] = $allowed_prefix->color;
        }

   /*     $messages = ReplaceMessage::with(['queue', 'send'])
            ->where('created_at', '>=', $from_date)
            ->where('created_at', '<=', $to_date)
            ->orderBy('id', 'DESC')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at, auth()->user()->time_zon)->format('d-m-Y');
                //grouping by date
            });*/

        $reportData = QueueMessage::with(['route', 'user'])
            ->where('created_at', '>=', $from_date)
            ->where('created_at', '<=', $to_date->addDay())
            ->orderBy('id', 'DESC')
            ->get();

        $prefixMessages = $this->get_msg_by_prefix_range($dataList, $from_date, $to_date);

        $totalData = $this->get_total_count($reportData);

        $total = $totalData['total'];
        //dd($to_date);
        $chartjs = $this->get_date_range_line_chart(array_keys($dataList), $prefixMessages, $from_date, Carbon::parse($to_date)->subDay(), $color_list);
        return view('admin.reports.range', ['date_range' => $date_range, 'reportData' => $reportData, 'total' => $total, 'chartjs' => $chartjs]);
    }

    #get total cout of group by collection

    private function get_msg_by_prefix_range($prefixList, $from_date, $to_date)
    {

        $returnList = array();
        foreach ($prefixList as $key => $prefix) {
            $data = QueueMessage::orderBy('id', 'DESC')
                ->where(['prefix_id' => $prefix])
                ->select('id', 'phone_number', 'created_at')
                ->whereBetween('created_at', [$from_date, $to_date])
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at, auth()->user()->time_zone)->format('d-m-Y');
                    //grouping by date
                });

            $return = array();
            foreach ($data as $value) {
                $time = $this->get_time_by_timezone($value[0]->created_at, auth()->user()->time_zone, 'd-m-Y');
                $return[$time] = $value->count();
            }

            ksort($return);
            $values = array();
            $schedule_date = new \DateTime();
            $schedule_date->setTimeZone(new \DateTimeZone(auth()->user()->time_zone));


            $begin = new \DateTime($from_date);
            $end = new \DateTime($to_date);

            for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
                if (array_key_exists($i->format("d-m-Y"), $return)) {
                    $values[$i->format("d-m-Y")] = $return[$i->format("d-m-Y")];
                } else {
                    $values[$i->format("d-m-Y")] = 0;
                }

            }
            $returnList[$key] = $values;
        }

        return $returnList;
    }

    #get date range prefix date

    private function get_total_count($collection)
    {
        $total = 0;
        $daysData = $collection->map(function ($item, $key) use (&$total) {
            $total += collect($item)->count();
            return collect($item)->count();
        });
        return ['total' => $total, 'intValData' => $daysData];
    }

    #get date range chart
    //get line chart

    public function get_date_range_line_chart($labels, $data, $from, $to, $color_list)
    {

        $begin = new \DateTime($from);
        $end = new \DateTime($to);
        $time = array();
        for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
            $time[] = $i->format("d-m-Y");
        }
        $chart_data = array();

        foreach ($labels as $label) {
            $chart_data[] = [
                "label" => $label,
                'borderColor' => $color_list[$label],
                'backgroundColor' => $color_list[$label],
                "pointBorderColor" => "#00c0ef",
                "pointBackgroundColor" => $color_list[$label],
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "#00c0ef",
                'data' => $this->get_values($data[$label]),
            ];
        }
        return app()->chartjs
            ->name('lineChartTest')
            ->type('bar')
            ->size(['width' => 350, 'height' => 120])
            ->labels($time)
            ->datasets($chart_data)
            ->options([]);
    }

    //remove array key from array. return only value
    private function get_values($list)
    {
        $return = array();
        foreach ($list as $data) {
            $return[] = $data;
        }
        return $return;
    }

    //get the view to customer wides report
    public function get_customer_report_view()
    {
        $customer_list = Customer::with('login_data')->get();
        $customer_select = $this->make_collection_select($customer_list, 'id', 'name');
        return view('admin.reports.customer', ['customer_list' => $customer_select]);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list, $id, $name)
    {
        $select[] = 'Select One';
        foreach ($data_list as $data) {
            $select[$data->$id] = $data->$name;
        }
        return $select;
    }

    //get customer report view
    public function get_customer_report(Request $request)
    {
        $date_range = $request->input('selectDate');
        $customer_id = $request->input('customer_id');
        $string = explode('-', $date_range);
        $from_date = str_replace("/", '-', trim($string[0]));
        $to_date = str_replace("/", '-', trim($string[1]));

        $from_date = Carbon::parse($from_date);
        $from_date->setTimezone(auth()->user()->time_zone);

        $to_date = Carbon::parse($to_date);
        $to_date->setTimezone(auth()->user()->time_zone);

        $color_list = array();
        $prefix_ist = PrefixList::all();
        foreach ($prefix_ist as $allowed_prefix) {
            $dataList[$allowed_prefix->label] = $allowed_prefix->id;
            $color_list[$allowed_prefix->label] = $allowed_prefix->color;
        }

        $reportData = QueueMessage::with(['route', 'user'])
            ->where('created_at', '>=', $from_date)
            ->where('created_at', '<=', $to_date->addDay())
            ->where('user_id', '=', $customer_id)
            ->orderBy('id', 'DESC')
            ->get();
        $prefixMessages = $this->get_msg_by_prefix_range_customer($dataList, $customer_id, $from_date, $to_date);

        $totalData = $this->get_total_count($reportData);
        $total = $totalData['total'];

        $chartjs = $this->get_date_range_line_chart(array_keys($dataList), $prefixMessages, $from_date, $to_date->subDay(), $color_list);
        $customer_list = Customer::with('login_data')->get();
        $customer_select = $this->make_collection_select($customer_list, 'id', 'name');
        return view('admin.reports.customer', ['customer_list' => $customer_select, 'date_range' => $date_range, 'reportData' => $reportData, 'total' => $total, 'chartjs' => $chartjs, 'customer_id' => $customer_id]);
    }

    #get date range prefix date
    private function get_msg_by_prefix_range_customer($prefixList, $customer_id, $from_date, $to_date)
    {

        $returnList = array();
        foreach ($prefixList as $key => $prefix) {

            $data = QueueMessage::orderBy('id', 'DESC')
                ->where(['prefix_id' => $prefix])
                ->select('id', 'phone_number', 'created_at')
                ->whereBetween('created_at', [$from_date, $to_date])
                ->where('user_id', '=', $customer_id)
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at, auth()->user()->time_zone)->format('d-m-Y');
                    //grouping by date
                });
            $return = array();
            foreach ($data as $value) {
                $time = $this->get_time_by_timezone($value[0]->created_at, auth()->user()->time_zone, 'd-m-Y');
                $return[$time] = $value->count();
            }

            ksort($return);
            $values = array();
            $schedule_date = new \DateTime();
            $schedule_date->setTimeZone(new \DateTimeZone(auth()->user()->time_zone));


            $begin = new \DateTime($from_date);
            $end = new \DateTime($to_date);

            for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
                if (array_key_exists($i->format("d-m-Y"), $return)) {
                    $values[$i->format("d-m-Y")] = $return[$i->format("d-m-Y")];
                } else {
                    $values[$i->format("d-m-Y")] = 0;
                }

            }
            $returnList[$key] = $values;
        }

        return $returnList;
    }
}
