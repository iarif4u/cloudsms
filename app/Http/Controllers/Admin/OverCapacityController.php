<?php

namespace App\Http\Controllers\Admin;

use App\OverCapacity;
use Illuminate\Http\Request;

class OverCapacityController extends BaseController {
    #get over capacity view
    public function get_over_capacity(){
        $over_capacity = OverCapacity::all();
        return view('admin.reports.over_capacity',['over_capacity'=>$over_capacity]);
    }
}
