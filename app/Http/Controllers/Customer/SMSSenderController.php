<?php

namespace App\Http\Controllers\Customer;

use App\Contact;
use App\Customer;
use App\Jobs\ProcessSMS;
use App\PhoneBook;
use App\Prefix;
use App\QueueMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Instasent\SMSCounter\SMSCounter;

class SMSSenderController extends BaseController
{
    //get the view of sms send view
    public function get_sms_send_view()
    {
        $phonebooks = PhoneBook::with(['contact'])->where(['user_id' => auth()->user()->getAuthIdentifier()])->get();
        return view('customer.pages.sms_compose', ['phonebooks' => $phonebooks]);
    }

    //send dynamic sms
    public function send_dynamic_sms(Request $request)
    {
        $msg = $request->input('message');
        $sms_count = $this->get_sms_count($msg);
        $allowed = Prefix::with('prefix_info')->where(['customer_id' => auth()->user()->getAuthIdentifier()])->get();
        $processing = QueueMessage::where('user_id', "=", auth()->user()->getAuthIdentifier())->where('status', "=", 0)
            ->select(DB::raw('sum(msg_count) as processing'))
            ->first();
        $deduct_balance = ($processing) ? $processing->processing : 0;
        $queue = array();

        $this->validate(request(), [
            'book_id' => 'required|array',
            'book_id.*' => 'required|exists:phone_books,id',
            'recipients' => ['required', function ($attribute, $value, $fail) use (
                $allowed, $msg, $sms_count, &$queue,
                &$deduct_balance
            ) {
                $numbers = explode(',', $value);
                $allow_number = array();
                foreach ($numbers as $number) {
                    $isValid = false;
                    foreach ($allowed as $prefix) {
                        if (is_numeric($number)) {
                            $primary_digit = substr($number, 0, strlen($prefix->prefix_info->prefix));
                            if ($primary_digit == $prefix->prefix_info->prefix) {
                                if (!in_array($number, $allow_number)) {
                                    $deduct_balance += $sms_count * $prefix->rate;
                                }
                                $allow_number[] = $number;
                                $isValid = true;
                            }
                        }
                    }

                    if ($isValid == false) {
                        $fail($number . ' has invalid prefix');
                    }
                }
            }],
            'message' => 'required'

        ], [
            'recipients.required' => "Recipients no is required",
            'message.required' => "Message is required",
            'book_id.required' => 'Phonebook is required',
            'book_id.array' => 'Phonebook is invalid',
            'book_id.*.required' => 'Phonebook id is required',
            'book_id.*.exists' => 'exists id is invalid',
        ]);

        $receipt_numbers = [];
        $msg = $request->input('message');
        $books = $request->input('book_id');
        $contacts = Contact::whereIn('phonebook_id', $books)->select(['name', 'number'])->get();
        $customer = Customer::where(['id' => auth()->user()->getAuthIdentifier()])->first();
        if ($customer->user_capacity < $deduct_balance) {
            return redirect()->back()->withErrors('You hove not enough balance')->withInput();
        } else {
            foreach ($contacts as $contact) {
                $body = str_ireplace('[name]', $contact->name, $msg);
                if (!in_array($contact->number, $receipt_numbers)) {
                    $receipt_numbers[] = $contact->number;
                    foreach ($allowed as $prefix) {
                        if (is_numeric($contact->number)) {
                            $primary_digit = substr($contact->number, 0, strlen($prefix->prefix_info->prefix));
                            if ($primary_digit == $prefix->prefix_info->prefix) {
                                $queue = QueueMessage::firstOrNew([
                                    'message' => $body,
                                    'phone_number' => $contact->number,
                                    'user_id' => auth()->user()->getAuthIdentifier(),
                                    'route_id' => 0,
                                    'prefix_id' => $prefix->prefix_info->id,
                                    'status' => 0,
                                    'send_id' => null,
                                    'msg_count' => $this->get_sms_count($body)
                                ]);
                                $queue->uuid = uniqid();
                                $queue->save();
                                dispatch(new ProcessSMS($queue->id))->delay(Carbon::now()->addSeconds(1));
                            }
                        }
                    }
                }
            }
            return redirect()->back()->with('message', sizeof($receipt_numbers) . ' Message send successfully!!');
        }

    }

    //get the contact list of phone book
    public function get_phonebook_contacts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'book_id' => 'required|array',
            'book_id.*' => 'required|exists:phone_books,id',

        ], [
            'book_id.required' => 'Phonebook is required',
            'book_id.array' => 'Phonebook is invalid',
            'book_id.*.required' => 'Phonebook id is required',
            'book_id.*.exists' => 'exists id is invalid',
        ]);
        if ($validator->fails()) {
            //return error true, with validation error if has
            return ['error' => 'true', 'message' => $validator->errors()->all()];
        } else {
            $book_id = $request->input('book_id');
            $contacts = PhoneBook::with('contact')
                ->where('user_id', '=', auth()->user()->getAuthIdentifier())
                ->whereIn('id', $book_id)->get();
            $taken = array();
            if ($contacts) {
                foreach ($contacts as $contactlist) {
                    foreach ($contactlist->contact as $contact) {
                        if (!in_array($contact->number, $taken)) {
                            $taken[] = $contact->number;
                        }
                    }
                }
            }
            return implode(',', array_unique($taken));
        }
    }

    //get the sms unit and character length
    public function get_sms_counter(Request $request)
    {
        $message = $request->input('sms');
        $smsCounter = new SMSCounter();
        $details = $smsCounter->count($message);
        return ['characters' => $details->length, 'sms_count' => $details->messages];
    }

    //get message count

    public function send_customer_sms(Request $request)
    {
        $msg = $request->input('message');
        $sms_count = $this->get_sms_count($msg);
        $allowed = Prefix::with('prefix_info')->where(['customer_id' => auth()->user()->getAuthIdentifier()])->get();
        $processing = QueueMessage::where('user_id', "=", auth()->user()->getAuthIdentifier())->where('status', "=", 0)
            ->select(DB::raw('sum(msg_count) as processing'))
            ->first();
        $deduct_balance = ($processing) ? $processing->processing : 0;
        $queue = array();
        $this->validate(request(), [
            'recipients' => ['required', function ($attribute, $value, $fail) use (
                $allowed, $msg, $sms_count, &$queue,
                &$deduct_balance
            ) {
                $numbers = explode(',', $value);
                $allow_number = array();
                foreach ($numbers as $number) {

                    $isValid = false;
                    foreach ($allowed as $prefix) {
                        if (is_numeric($number)) {
                            $primary_digit = substr($number, 0, strlen($prefix->prefix_info->prefix));

                            if ($primary_digit == $prefix->prefix_info->prefix) {
                                if (!in_array($number, $allow_number)) {
                                    $deduct_balance += $sms_count * $prefix->rate;

                                    $queue[] = [
                                        'message' => $msg,
                                        'phone_number' => $number,
                                        'uuid' => uniqid(),
                                        'user_id' => auth()->user()->getAuthIdentifier(),
                                        'route_id' => 0,
                                        'prefix_id' => $prefix->prefix_info->id,
                                        'status' => 0,
                                        'send_id' => null,
                                        'msg_count' => $sms_count,
                                        'updated_at' => now(),
                                        'created_at' => now(),
                                    ];

                                }
                                $allow_number[] = $number;
                                $isValid = true;
                            }
                        }
                    }

                    if ($isValid == false) {
                        $fail($number . ' has invalid prefix');
                    }
                }
            }],
            'message' => 'required'

        ], [
            'recipients.required' => "Recipients no is required",
            'message.required' => "Message is required",
        ]);
        $customer = Customer::where(['id' => auth()->user()->getAuthIdentifier()])->first();
        //dd($customer->user_capacity,$deduct_balance);
        if ($customer->user_capacity < $deduct_balance) {
            return redirect()->back()->withErrors('You hove not enough balance')->withInput();
        } else {
            foreach ($queue as $message) {
                $msg = QueueMessage::create($message);
                dispatch(new ProcessSMS($msg->id))->delay(Carbon::now()->addSeconds(1));
            }
            return redirect()->back()->with('message', sizeof($queue) . ' Message send successfully!!');
        }
    }

    //send the sms

    private function get_sms_count($msg)
    {
        $smsCounter = new SMSCounter();
        $details = $smsCounter->count($msg);
        return $details->messages;
    }

    public function get_dynamic_send_view()
    {
        $phonebooks = PhoneBook::with(['contact'])->where(['user_id' => auth()->user()->getAuthIdentifier()])->get();
        return view('customer.pages.dynamic_sms_compose', ['phonebooks' => $phonebooks]);
    }

    //get contact list by file

    public function get_file_contacts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // check validtion for image or file
            'file' => 'required',
        ], [
            'phone_book_id.required' => 'Phone book id is required',
            'phone_book_id.exists' => 'Phone book id is invalid',
            'file.required' => 'Excel/CSV file is required',
            'file.mimes' => 'Excel/CSV file is invalid format',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();

        } else {
            $accepted_extention = ['txt', 'csv', 'xls', 'xlsx', 'xlt'];
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $extention = $file->getClientOriginalExtension();
                $numbers = $request->input('numbers');
                $previous_no = (strlen($numbers) > 0) ? explode(',', $numbers) : array();
                // return ['error'=>false,'message'=> explode(',',array_unique($previous_no))];
                if (in_array($extention, $accepted_extention)) {
                    $path = $file->getRealPath();
                    $data = \Excel::load($path, function ($reader) {
                    })->get();
                    if (!empty($data) && $data->count()) {

                        foreach ($data as $key => $value) {
                            if (!in_array($value->number, $previous_no)) {
                                $previous_no[] = $value->number;
                            }
                        }
                        return ['error' => false, 'message' => implode(',', array_unique($previous_no))];
                    } else {
                        return ['error' => true, 'message' => "Contact numbers not found"];
                    }
                } else {
                    return ['error' => true, 'message' => "File format not supported"];
                }
            } else {
                return ['error' => true, 'message' => "File not found"];
            }
        }
    }

    //send excel direct sms to
    public function send_dynamic_excel_sms(Request $request)
    {

        $validator = Validator::make($request->all(), [
            // check validtion for image or file
            'file' => 'required|file|mimes:xlsx,txt,csv,xls,xlt',
        ], [
            'file.required' => 'Excel/CSV file is required',
            'file.mimes' => 'Excel/CSV file is invalid format',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();

        } else {
            $accepted_extension = ['txt', 'csv', 'xls', 'xlsx', 'xlt'];
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $queue = [];
                if (in_array($extension, $accepted_extension)) {
                    $path = $file->getRealPath();
                    $data = \Excel::load($path, function ($reader) {
                    })->get();
                    if (!empty($data) && $data->count()) {
                        $allowed = Prefix::with('prefix_info')->where(['customer_id' => auth()->user()->getAuthIdentifier()])->get();
                        $deduct_balance = 0;
                        $allow_number = [];
                        foreach ($data as $key => $value) {
                            $sms_count = $this->get_sms_count($value->message);
                            if (!in_array($value->number, $allow_number)) {
                                foreach ($allowed as $prefix) {
                                    $primary_digit = substr($value->number, 0, strlen($prefix->prefix_info->prefix));
                                    if ($primary_digit == $prefix->prefix_info->prefix) {
                                        $deduct_balance += $sms_count * $prefix->rate;
                                        $queue[] = [
                                            'message' => $value->message,
                                            'phone_number' => $value->number,
                                            'uuid' => uniqid(),
                                            'user_id' => auth()->user()->getAuthIdentifier(),
                                            'route_id' => 0,
                                            'prefix_id' => $prefix->prefix_info->id,
                                            'status' => 0,
                                            'send_id' => null,
                                            'msg_count' => $sms_count,
                                            'updated_at' => now(),
                                            'created_at' => now(),
                                        ];
                                        $allow_number[] = $value->number;
                                    }
                                }
                            }
                        }
                        $customer = Customer::where(['id' => auth()->user()->getAuthIdentifier()])->first();
                        if ($customer->user_capacity < $deduct_balance) {
                            return redirect()->back()->withErrors('You hove not enough balance')->withInput();
                        } else {
                            foreach ($queue as $message) {
                                $msg = QueueMessage::create($message);
                                dispatch(new ProcessSMS($msg->id))->delay(Carbon::now()->addSeconds(1));
                            }
                            return redirect()->back()->with('message', sizeof($queue) . ' Message send successfully!!');
                        }
                    } else {
                        return redirect()->back()->withErrors(["Something went wrong"]);
                    }
                } else {
                    return redirect()->back()->withErrors(["File format not supported"]);
                }
            } else {
                return redirect()->back()->withErrors(["File not found"]);
            }
        }
    }
}
