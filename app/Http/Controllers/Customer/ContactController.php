<?php

namespace App\Http\Controllers\Customer;

use App\Contact;
use App\Delete;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use DB;
class ContactController extends BaseController
{
    //create new contact list
    public function create_contact_list(Request $request){
        $validator=  Validator::make($request->all(), [
            'contact_name' => 'nullable|max:191',
            'contact_name.*' => 'nullable|max:191',
            'number' =>'required|max:191',
            'number.*' =>'required|numeric',
            'phone_book_id' => 'required|exists:phone_books,id',
        ],[
            'contact_name.max' => 'Name is invalid',
            'contact_name.*.max' => 'Name is invalid',
            'number.required' => 'Phone number is required',
            'number.*.required' => 'Phone number is required',
            'number.*.numeric' => 'Phone number is invalid',
            'phone_book_id.required' => 'Phone book is required',
            'phone_book_id.exists' => 'Phone book is invalid',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $name = $request->input('contact_name');
            $number_list = $request->input('number');
            $phone_book_id = $request->input('phone_book_id');
            DB::beginTransaction();
            try{
                for ($i=0;$i<sizeof($name);$i++){
                    Contact::create([
                        'name' => $name[$i],
                        'number' => $number_list[$i],
                        'phonebook_id' => $phone_book_id
                    ]);
                }
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors('Route prefix update fail '.$exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message','Contact lis insert into phone book successfully done');

        }
    }

    //get full contact list of phone boook
    public function get_contact_list(Request $request){
        $validator=  Validator::make($request->all(), [
            'phone_book_id' => 'required|exists:phone_books,id',
        ],[
            'phone_book_id.required' => 'Phone book is required',
            'phone_book_id.exists' => 'Phone book is invalid',
        ]);
        if (!$validator->fails())
        {
           $phone_book_id = $request->input('phone_book_id');
           $contact_list = Contact::where(['phonebook_id'=>$phone_book_id])->get();
            $data = array();
            foreach ($contact_list as $contact):
                $triggerOn = $contact->created_at;
                $schedule_date = new \DateTime($triggerOn);
                $schedule_date->setTimeZone(new \DateTimeZone(auth()->user()->time_zone));
                $timestamp =  $schedule_date->format('Y-m-d H:i:s');

                $sub_array = array();
                $sub_array[] = '<div class="cnt-name"><span></span><p>'.$contact->name.'</p></div>';
                $sub_array[] = '<div class="cnt-number"><span></span><p>'.$contact->number.'</p><div>';
                $sub_array[] = '<span class="span-cnt-id invisible">'.$contact->id.'</span><div class="col-md-1"><a href="javascript:void(0);" 
value="'
                    .$contact->id.'" 
class="contact-drop-btn btn btn-xs btn-danger">
                                    <i class="fa fa-remove"></i>
                                </a></div>
                                <div class="edit col-md-1"><span></span><a href="javascript:void(0);" value="'
                    .$contact->id.'" 
                                class="contact-update-btn btn btn-xs btn-primary">
                                    <i class="fa fa-pencil"></i>
                                </a></div>
                                ';
                $data[] = $sub_array;
            endforeach;
            $output = array(
                'draw' => intval($request->input('draw')),
                'data' => $data,
            );
            return $output;
        }
    }

    //delete contact by post request
    public function delete_contact_data(Request $request){
        $validator=  Validator::make($request->all(), [
            'contact_id' => 'required|exists:contacts,id',
        ],[
            'contact_id.required' => 'Contact id is required',
            'contact_id.exists' => 'Contact id is invalid',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all());
        }else{
            $contact_id = $request->input('contact_id');
            DB::beginTransaction();
            try{
                $contact = Contact::find($contact_id);

                Delete::create([
                    'primary_value'=>$contact->id,
                    'name'=>$contact->getTable(),
                    'others_value'=>json_encode($contact)
                ]);
                $contact->delete();
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors('Contact delete fail '.$exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message','Contact delete successfully done');
        }
    }
    //update contact info

    /**
     * @param Request $request
     */
    public function update_contact_data(Request $request){
        $validator=  Validator::make($request->all(), [
            'contact_id' => 'required|exists:contacts,id',
            'contact_number' => 'required|numeric',
        ],[
            'contact_id.required' => 'Contact id is required',
            'contact_id.exists' => 'Contact id is invalid',
            'contact_name.required' => 'Contact name is required',
            'contact_number.required' => 'Contact number is required',
            'contact_number.numeric' => 'Contact number is invalid',
        ]);
        $name = $request->input('contact_name','Unknown');
        $name = (strlen($name)>0) ? $name : 'Unknown';
        $contact_number = $request->input('contact_number');
        Contact::where(['id'=>$request->input('contact_id')])->update(['name'=>$name,'number'=>$contact_number]);
        return ['status'=>'success'];
    }

    //contact list import by excel/csv file
    public function import_contact_list(Request $request){
        $this->validate($request, [
            // check validtion for image or file
            'phone_book_id' => 'required|exists:phone_books,id',
            'file' => 'required',
        ],[
            'phone_book_id.required'=>'Phone book id is required',
            'phone_book_id.exists'=>'Phone book id is invalid',
            'file.required'=>'Excel/CSV file is required',
            'file.mimes'=>'Excel/CSV file is invalid format',
        ]);
        $accepted_extention = ['txt','csv','xls','xlsx','xlt'];
        if ($request->hasFile('file')) {
            $book_id = $request->input('phone_book_id');
            $file = $request->file('file');

            $extention = $file->getClientOriginalExtension();
            if (in_array($extention, $accepted_extention)){

                $path = $file->getRealPath();
                $previous_no = $this->get_contact_list_number($book_id);
                $duplicate = 0;
                $data = \Excel::load($path, function ($reader) {

                })->get();
                if (!empty($data) && $data->count()) {
                    $insert = array();
                    foreach ($data as $key => $value) {
                        if (!in_array($value->number, $previous_no)) {
                            if (!is_null($value->number)){
                                $previous_no[] = $value->number;
                                $insert[] = [
                                    'name' => (is_null($value->name)) ? "Unknown" : $value->name,
                                    'number' => $value->number,
                                    'phonebook_id' => $book_id,
                                    'created_at' => now(),
                                    'updated_at' => now(),
                                ];
                            }
                        } else {
                            $duplicate++;
                        }
                    }
                    if (sizeof($insert) > 0) {

                        DB::table('contacts')->insert($insert);
                        return redirect()->back()->with('message', sizeof($insert) . " contact insert successfully done and $duplicate duplicate found");
                    } else {
                        return redirect()->back()->withErrors('Contact list import fail and ' . $duplicate . ' duplicate found');
                    }
                } else {
                    return redirect()->back()->withErrors('Contact list is empty');
                }
            }else{
                return redirect()->back()->withErrors(['You have inserted invalid file']);
            }
        }else{
            return redirect()->back()->withErrors(['Excel/CSV file is required']);
        }

    }

    //get phone book contact list
    private function get_contact_list_number($book_id){
        $contact_list = Contact::where(['phonebook_id'=>$book_id])->get();
        $list_array = array();
        foreach ($contact_list as $contact){
            $list_array[] = $contact->number;
        }
        return $list_array;
    }


}
