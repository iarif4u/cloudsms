<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public function __construct()
    {
        $time_zone = array();
        $timestamp = time();
        foreach(timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $time_zone[$zone] = $zone.' ( ' . date('P', $timestamp)." )";
        }

        // Sharing is caring
        View::share('tzlist', $time_zone);
    }
}
