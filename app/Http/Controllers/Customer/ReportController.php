<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\OverCapacity;
use App\Prefix;
use App\QueueMessage;
use App\ReplaceMessage;
use App\SendMessage;
use App\UserCapacity;
use Illuminate\Http\Request;

class ReportController extends BaseController
{

    #get the view of daily report
    public function get_daily_report_view(){

        $queue_collection = QueueMessage::whereDate('created_at','=', date('Y-m-d'))
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC');

        $send_collection = SendMessage::whereDate('date','=', date('Y-m-d'))
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC');
        $prefix_list = Prefix::with('prefix_info')->where(['customer_id' => auth()->id()])->get();
        $total_prefix = [];
        foreach ($prefix_list as $prefix){

            $total_prefix[$prefix->prefix_info->label] = $this->get_prefix_total($prefix->prefix_info->prefix);
        }

       /* $total_prefix = [
            '+88017' => $this->get_prefix_total("017"),
            '+88019' => $this->get_prefix_total("019"),
            '+88016' => $this->get_prefix_total("+88016"),
            '+88018' => $this->get_prefix_total("+88018"),
            '+88015' => $this->get_prefix_total("+88015"),
        ];*/

        $capacity_list = UserCapacity::whereDate('created_at','=', date('Y-m-d'))
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC')->get();

        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();

        // $total = $message_send->count();
        return view('customer.pages.daily_report',['user_capacity'=>$user_capacity,'capacity_list'=>$capacity_list,'queue_success'=>$queue_collection->get(),'message_send'=>$send_collection->get(),'total_prefix'=>$total_prefix]);
    }

    #get data array by mobile number prefix
    public function get_prefix_total($prefix){
        $queue_collection = QueueMessage::whereDate('created_at','=', date('Y-m-d'))
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', $prefix.'%')->get()->sum('msg_count');

        $send_collection = SendMessage::whereDate('date','=', date('Y-m-d'))
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', $prefix.'%')->get()->sum('msg_count');
        return $queue_collection+$send_collection;

    }

    #get data array by mobile number prefix from over capacity
    public function get_overcapacity_prefix_total($prefix){
        $queue_collection = OverCapacity::where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone', 'like', $prefix.'%')->get()->sum('msg_count');

        return $queue_collection;

    }


    #get data array by mobile number prefix by date range
    public function get_prefix_daterange($prefix,$from,$to){
        $queue_collection = QueueMessage::whereBetween('created_at', [date('Y-m-d',strtotime($from)), date('Y-m-d',strtotime($to))])
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', $prefix.'%')->get()->sum('msg_count');

        $send_collection = SendMessage::whereBetween('date', [date('Y-m-d',strtotime($from)), date('Y-m-d',strtotime($to))])
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', $prefix.'%')->get()->sum('msg_count');

        return $queue_collection+$send_collection;

    }

    #get monthly report view
    public function get_monthly_report_view(){

        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        return view('customer.pages.monthly_report',['user_capacity'=>$user_capacity]);
    }

    #set date range report
    public function set_daterange_report(Request $request){

        $daterange = $request->input('daterange');
        $date = explode('-',$daterange);
        $from_date = $date[0];
        $to_date = date("Y-m-d H:i:s", strtotime($date[1]. ' +1 day'));

        $queue_collection = QueueMessage::whereBetween('created_at', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC');

        $send_collection = SendMessage::whereBetween('date', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime
        ($to_date))])
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC');
        $total_prefix = [
            '+88017' => $this->get_prefix_daterange("+88017",$from_date,$to_date),
            '+88019' => $this->get_prefix_daterange("+88019",$from_date,$to_date),
            '+88016' => $this->get_prefix_daterange('+88016',$from_date,$to_date),
            '+88018' => $this->get_prefix_daterange('+88018',$from_date,$to_date),
            '+88015' => $this->get_prefix_daterange('+88015',$from_date,$to_date),
        ];
        $capacity_list = UserCapacity::whereBetween('created_at', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC')->get();
        $total_send = $send_collection->sum('msg_count')+$queue_collection->sum('msg_count');
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        return view('customer.pages.monthly_report',['total_send'=>$total_send,'user_capacity'=>$user_capacity,'capacity_list'=>$capacity_list,'queue_success'=>$queue_collection->get(),'message_send'=>$send_collection->get(),'total_prefix'=>$total_prefix]);

    }

    #get view of prefix view
    public function get_prefix_report_view(){
        $user_capacity = Customer::with('prefix_list')->where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        return view('customer.pages.prefix_report',['user_capacity'=>$user_capacity]);
    }
    public function get_prefix_report(Request $request){

        $this->validate_prefix_req($request);
        $daterange = $request->input('daterange');
        $date = explode('-',$daterange);

        $to_date = str_replace('/', '-', $date[1]);
        $from_date = str_replace('/', '-', $date[0]);

        $to_date = date("Y-m-d H:i:s", strtotime($to_date. ' +1 day'));
        $from_date = date("Y-m-d H:i:s", strtotime($from_date));


        $prefix = $request->input('prefix');
        dd($prefix);
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        $queue_collection = QueueMessage::whereBetween('created_at', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('id', '=', $prefix)
            ->orderBy('id', 'DESC');

        $total_prefix = [
            $prefix=>   $this->get_prefix_daterange($prefix,$from_date,$to_date)
        ];
        return view('customer.pages.prefix_report',['daterange'=>$daterange,'total_prefix'=>$total_prefix,
            'user_capacity'=>$user_capacity,'queue_success'=>$queue_collection->get()]);
    }

    #validate the prefix
    private function validate_prefix_req($request){
        $this->validate($request,[
            'daterange' => 'required',
            'prefix' => 'required',
        ],[
            'daterange.required' => "Date is required",
            'prefix.required' => "Prefix is required",
        ]);
    }

    #get the balance history
    public function get_balance_history(){
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        $queue_collection = QueueMessage::where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC');

        $send_collection = SendMessage::where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC');
        $total_prefix = [
            '+88017' => $this->get_prefix_history('+88017'),
            '+88019' => $this->get_prefix_history('+88019'),
            '+88016' => $this->get_prefix_history('+88016'),
            '+88018' => $this->get_prefix_history('+88018'),
            '+88015' => $this->get_prefix_history('+88015'),
        ];
        $capacity_list = UserCapacity::where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->orderBy('id', 'DESC')->get();

        return view('customer.pages.balance_history',['capacity_list'=>$capacity_list,'total_prefix'=>$total_prefix,'user_capacity'=>$user_capacity,'queue_msg'=>$queue_collection->get(),'message_send'=>$send_collection->get()]);
    }

    #get data array by mobile number prefix history
    public function get_prefix_history($prefix){
        $queue_collection = QueueMessage::where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', $prefix.'%')->orderBy('id', 'DESC')->get()->sum('msg_count');

        $send_collection = SendMessage::where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', $prefix.'%')->orderBy('id', 'DESC')->get()->sum('msg_count');

        return $queue_collection+$send_collection;

    }

    #get pending report
    public function get_pending_list(){
        $queue_msg = QueueMessage::where(['user_id'=>auth()->user()->getAuthIdentifier(),'status'=>"0"])->orderBy('id', 'DESC')->get();

        return view('customer.pages.pending_report',['msg_list'=>$queue_msg]);


    }

    //get over capacity messages
    public function get_over_capacity(){
        $over_capacity = OverCapacity::where(['user_id'=>auth()->user()->getAuthIdentifier()])->orderBy('id', 'DESC')->get();
        $total_prefix = [
            '+88017' => $this->get_overcapacity_prefix_total('%88017'),
            '+88019' => $this->get_overcapacity_prefix_total('%88019'),
            '+88016' => $this->get_overcapacity_prefix_total('%88016'),
            '+88018' => $this->get_overcapacity_prefix_total('%88018'),
            '+88015' => $this->get_overcapacity_prefix_total('%88015'),
        ];
        return view('customer.pages.over_capacity',['over_capacity'=>$over_capacity,'total_prefix'=>$total_prefix]);
    }
    /*Message failed list*/
    public function get_failed_list(){
        $queue_msg = QueueMessage::with('error_message')->where(['user_id'=>auth()->user()->getAuthIdentifier(),'status'=>2])->orderBy('id', 'DESC')->get();
        $failed_msg = SendMessage::with('error_message')->where(['status'=>2,'user_id'=>auth()->user()->getAuthIdentifier()])->get();
        return view('customer.pages.failed',['msg_list'=>$queue_msg,'failed_msg'=>$failed_msg]);
    }

    //get success msg list
    public function get_success_list(){
        $messageList = ReplaceMessage::with(['queue','send'])->where(['user_id'=>auth()->user()->getAuthIdentifier()
        ])->orderBy('id','desc')->limit(10)->get();

     /*   $total = $messageList->sum('');*/
        return view('customer.pages.success',['messageList'=>$messageList]);
    }
    //get msg list for specific user
    function get_filter_msg($cursor,$msgList){
        $msg_list = array();
        $i =0;
        while ($cursor->hasNext()) {
            $message = $cursor->next();
            foreach ($msgList as $msg){
                if ($message->id == $msg->send_id){
                    $msg_list[$i++] = [
                        'send_id' =>   $message->id,
                        'msg' =>   $msg->message,
                        'phone' =>   $msg->phone_number,
                        'date_send' =>   $message->time_sent,
                        'date' =>   $msg->phone_number,
                    ];
                }
            }
        }
        return $msg_list;
    }

    #get view for number and date report
    public function get_date_n_number(){
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        return view('customer.pages.date_n_number',['user_capacity'=>$user_capacity]);
    }

    #get the report for number and date
    public function report_date_n_number(Request $request){
        $this->validate($request,[
            'daterange' => 'required|date_format:"d/m/Y',
            'number' => 'required|numeric',
        ],[
            'daterange.required' => "Date is required",
            'daterange.date_format' => "Invalid date format",
            'number.required' => "Number is required",
        ]);

        $date = $request->input('daterange');
        $number = $request->input('number');
        $date = str_replace('/', '-', $date);
        $new_date = date('Y-m-d',strtotime($date));


        $queue_collection = QueueMessage::whereDate('created_at','=', $new_date)
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', '%'.$number.'%')->orderBy('id', 'DESC')->get();

        $send_collection = SendMessage::whereDate('date','=', $new_date)
            ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
            ->Where('phone_number', 'like', '%'.$number.'%')->orderBy('id', 'DESC')->get();
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        return view('customer.pages.date_n_number',['date'=>$date,'number'=>$number,'user_capacity'=>$user_capacity,
            'queue_success'=>$queue_collection, 'message_send'=>$send_collection]);
    }

    #get view of combined report
    public function get_combined_report(Request $request){
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        $prefix_list = Prefix::with('prefix_info')->where(['customer_id' => auth()->id()])->get();
        $total_prefix = [''=>'Select All'];
        foreach ($prefix_list as $prefix){

            $total_prefix[$prefix->prefix_info->prefix] = $prefix->prefix_info->prefix;
        }
        return view('customer.pages.combined',['user_capacity'=>$user_capacity,'select_prefix'=>'','total_prefix'=>$total_prefix]);
    }
    #get details of combined_report
    public function combined_report(Request $request){
        $this->validate($request,[
            'daterange' => 'required'
        ],[
            'daterange.required' => "Date is required",
        ]);
        $daterange = $request->input('daterange');
        $date = explode('-',$daterange);

        $to_date = str_replace('/', '-', $date[1]);
        $from_date = str_replace('/', '-', $date[0]);

        $to_date = date("Y-m-d H:i:s", strtotime($to_date. ' +1 day'));
        $from_date = date("Y-m-d H:i:s", strtotime($from_date));

        $prefix = $request->input('prefix');
        $status = $request->input('status');
        if ($status ==""){
            $queue_collection = QueueMessage::whereBetween('created_at', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
                ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
                ->Where('phone_number', 'like', $prefix.'%')
                ->orderBy('id', 'DESC')
                ->get();

            $send_collection = SendMessage::whereBetween('date', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
                ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
                ->Where('phone_number', 'like', $prefix.'%')
                ->orderBy('id', 'DESC')
                ->get();
        }
        else{
            $queue_collection = QueueMessage::whereBetween('created_at', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
                ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
                ->where(['status'=>$status])
                ->Where('phone_number', 'like', $prefix.'%')
                ->orderBy('id', 'DESC')
                ->get();

            $send_collection = SendMessage::whereBetween('date', [date('Y-m-d',strtotime($from_date)), date('Y-m-d',strtotime($to_date))])
                ->where(['user_id'=>auth()->user()->getAuthIdentifier()])
                ->where(['status'=>$status])
                ->Where('phone_number', 'like', $prefix.'%')
                ->orderBy('id', 'DESC')
                ->get();
        }
        $user_capacity = Customer::where(['id'=>auth()->user()->getAuthIdentifier()])->first();
        $prefix_list = Prefix::with('prefix_info')->where(['customer_id' => auth()->id()])->get();
        $total_prefix = [''=>'Select All'];
        foreach ($prefix_list as $prefix){

            $total_prefix[$prefix->prefix_info->prefix] = $prefix->prefix_info->prefix;
        }

        $select_prefix = $request->input('prefix');
        //dd($select_prefix);
        return view('customer.pages.combined',['user_capacity'=>$user_capacity,'total_prefix'=>$total_prefix,
            'queue_collection'=>$queue_collection,'send_collection'=>$send_collection,'select_prefix'=>$select_prefix,
            'daterange'=>$daterange,'status'=>$status,'prefix'=>$prefix]);


    }
}
