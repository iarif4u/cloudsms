<?php

namespace App\Http\Controllers\Customer;

use App\Customer\Customer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends BaseController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.home');
    }

    //update user time zone
    public function update_time_zone(Request $request)
    {
        //validate timezone
        $this->validate($request, [
            'time_zone' => 'required|timezone',
        ], [
            'time_zone.required' => "Time Zone is required",
            'time_zone.timezone' => "Time Zone is invalid",
        ]);
        $user = Customer::findOrFail(auth()->user()->getAuthIdentifier());
        $user->time_zone = $request->input('time_zone');

        $user->save();
        if ($user) {
            return redirect()->back()->with('message', "Time zone successfully update to " . $user->time_zone);
        } else {
            return redirect()->back()->withErrors("Time zone failed to update");
        }
    }

    public function get_customer_profile_view()
    {
        $time_zone = array();
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $time_zone[$zone] = $zone . ' ( ' . date('P', $timestamp) . " )";
        }
        $this->timeZoneList = $time_zone;
        return view('customer.profile', ['time_zone' => $time_zone]);
    }

    public function update_customer_profile(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $customer = Customer::where(['id' => auth()->id()])->first();
        $customer->name = $request->input('name');
        $customer->password = Hash::make($request->input('password'));;
        $customer->save();

        return redirect()->back()->with('message', "Profile update successfully done");
    }

}
