<?php

namespace App\Http\Controllers\Customer;

use App\Contact;
use App\Delete;
use App\PhoneBook;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use DB;
class PhoneBookController extends BaseController
{
    //get phone book list for customer
    public function get_phone_book(){
        $phonebooks = PhoneBook::with(['contact'])->where(['user_id'=>auth()->user()->getAuthIdentifier()])->get();
        return view('customer.pages.phonebook',['phonebooks'=>$phonebooks]);
    }

    //create new phone book for contact list
    public function create_phone_book(Request $request){
        $validator=  Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('phone_books')->where(function ($query) {
                    $query->where('user_id', auth()->user()->getAuthIdentifier());
                })
            ],
            'description' =>'required|max:191'
        ],[
            'name.required' => 'Address book name is required',
            'description.required' => 'Address book description is required',
            'name.unique' => 'The name of address book has already been taken',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            PhoneBook::create([
                'name'=>$request->input('name'),
                'user_id'=> auth()->user()->getAuthIdentifier(),
                'description'=>$request->input('description'),
            ]);
            return redirect()->back()->with('message','Address book create successfully done');
        }
    }

    //update phone book
    public function update_phone_book(Request $request){
        $book_id = $request->input('book_id',0);
        $validator=  Validator::make($request->all(), [
            'book_id' => 'required|exists:phone_books,id',
            'name' => [
                'required',
                Rule::unique('phone_books')->ignore($book_id)->where(function ($query) {
                    $query->where('user_id', auth()->user()->getAuthIdentifier());
                })
            ],
            'description' =>'required|max:191'
        ],[
            'book_id.required' => 'Address book id is required',
            'book_id.exists' => 'Address book id is invalid',
            'name.required' => 'Address book name is required',
            'description.required' => 'Address book description is required',
            'name.unique' => 'The name of address book has already been taken',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            PhoneBook::where(['id'=>$book_id,'user_id'=> auth()->user()->getAuthIdentifier()])->update([
                'name'=>$request->input('name'),
                'description'=>$request->input('description'),
            ]);
            return redirect()->back()->with('message','Address book update successfully done');
        }
    }

    //delete phone book
    public function delete_phone_book(Request $request){
        $book_id = $request->input('book_id',0);
        $validator=  Validator::make($request->all(), [
            'book_id' => 'required|exists:phone_books,id',
        ],[
            'book_id.required' => 'Address book id is required',
            'book_id.exists' => 'Address book id is invalid',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            DB::beginTransaction();
            try{
                $book = PhoneBook::where(['id'=>$book_id,'user_id'=>auth()->user()->getAuthIdentifier()])->first();
                Delete::create([
                    'primary_value'=>$book->id,
                    'name'=>$book->getTable(),
                    'others_value'=>json_encode($book)
                ]);
                $book->delete();
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors('Address book delete fail '.$exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message','Address book delete successfully done');
        }
    }

    //download contact lis as excel file
    public function download_phone_book(Request $request){
        $validator=  Validator::make($request->all(), [
            'book_id' => 'required|exists:phone_books,id',
            'format_type' => 'required|in:csv,xls,xlsx,xlt,txt',
        ],[
            'book_id.required' => 'Address book id is required',
            'book_id.exists' => 'Address book id is invalid',
            'book_id.format_type' => 'Format type not found',
            'book_id.in' => 'Format type is invalid',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            $book_id = $request->input('book_id');
            $format_type = $request->input('format_type');
            $dataList = Contact::where(['phonebook_id'=>$book_id])->select(['name as Name','number as Number'])->get()
                ->toArray();
            $book_info = PhoneBook::findorfail($book_id);
            \Excel::create($book_info->name." ".date('d-m-Y'), function($excel) use ($dataList,$book_info) {
                $excel->setTitle($book_info->description);
                $excel->sheet('Address Book', function($sheet) use ($dataList){
                    $sheet->setAutoSize(true);
                    $sheet->fromArray($dataList);
                });
            })->download($format_type);
        }
    }
}
