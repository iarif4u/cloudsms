<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginUser extends Model
{
    protected $table ="customers";

    protected $fillable = [
        'name', 'email','time_zone','password'
    ];
}
