<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneBook extends Model
{
    protected $table = 'phone_books';

    protected $fillable = ['name','user_id','description'];

    public function contact(){
        return $this->hasMany('App\Contact','phonebook_id','id');
    }
}
