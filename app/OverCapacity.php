<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OverCapacity extends Model
{
    protected $table = 'over_capacity';

    protected $fillable = ['user_id','message','phone'];

    public function user(){
        return $this->hasOne('App\Customer','id','user_id');
    }
}
