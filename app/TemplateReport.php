<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateReport extends Model
{
    protected $table = "template_reports";

    protected $fillable = ["customer_id", "prefix_id", "route_id", "qty"];
}
