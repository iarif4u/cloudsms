<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpOutputString extends Model
{
    protected $table = "otp_outgoing_string";

    protected $fillable = ['incoming_string_id','replace_string','counter','status'];

    public function input_string(){

        return $this->hasOne('App\OtpString','id','incoming_string_id');
    }
}
