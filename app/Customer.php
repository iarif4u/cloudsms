<?php

namespace App;

use App\Notifications\CustomerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "user";

    protected $fillable = ['name', 'email', 'password', 'api_key', 'user_capacity', 'alert', 'user_tpm', "is_smpp", "system_id", "smpp_port", "smpp_password"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token));
    }

    public function login_data()
    {
        return $this->hasOne('App\LoginUser', 'email', 'email');
    }

    public function getTableName()
    {
        return $this->table;
    }

    //get customer prefix list
    public function prefix_list()
    {
        return $this->hasMany('App\Prefix', 'customer_id', 'id')->with('prefix_info');
    }
}
