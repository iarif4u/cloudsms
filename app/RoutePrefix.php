<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoutePrefix extends Model
{
    protected $table = 'route_prefixes';

    protected $fillable = ['route_id', 'prefix', 'rate', 'status'];

    public function prefix_info()
    {
        return $this->hasOne(PrefixList::class, 'id', 'prefix');
    }
}
