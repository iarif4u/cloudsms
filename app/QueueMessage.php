<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueueMessage extends Model
{
    protected $table = 'queue_message';

    protected $fillable = ['message', 'phone_number', 'uuid', 'user_id', 'route_id', 'prefix_id', 'status', 'send_id', 'msg_count', 'rate'];

    public function user()
    {
        return $this->hasOne('App\Customer', 'id', 'user_id');
    }

    //get route info
    public function route()
    {
        return $this->hasOne('App\Route', 'route', 'route_id');
    }

    public function callback()
    {
        return $this->hasOne('App\Callback', 'message_id', 'id');
    }
}
