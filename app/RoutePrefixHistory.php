<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoutePrefixHistory extends Model
{
    protected $table = 'route_prefix_histories';
    protected $fillable = ['route_id','prefix','rate','is_delete'];
}
