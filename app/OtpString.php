<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpString extends Model
{
    protected $table = "otp_string";

    protected $fillable = ['incoming_string','vendor_id','status'];

    public function vendor(){
        return $this->hasOne('App\OtpVendor','id','vendor_id');
    }
}
