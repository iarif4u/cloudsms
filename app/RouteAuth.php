<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteAuth extends Model
{
    protected $table = "route_auth";

    protected $fillable = ["username","password","route_id"];
}
