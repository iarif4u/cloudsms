<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrefixHistory extends Model
{
    protected $table = "prefix_histories";

    protected $fillable = ['customer_id','prefix','rate','is_delete'];
}
