<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpVendor extends Model
{
    protected $table ="otp_vendors";
    protected $fillable = ['vendor_name','status'];

}
