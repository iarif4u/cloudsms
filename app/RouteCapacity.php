<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteCapacity extends Model
{
    protected $table = 'route_capacity_history';
    protected $fillable=['route_id','route_capacity'];
}
