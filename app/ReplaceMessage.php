<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplaceMessage extends Model
{
    protected $table = "message_replace_report";

    protected $fillable = ["msg_id", 'incoming_msg', 'phone', 'user_id', 'route_id',  'delivery_at', 'feedback'];

    public function queue()
    {
        return $this->hasOne('App\QueueMessage', 'id', 'msg_id')->with(['user', 'route']);
    }

    public function send()
    {
        return $this->hasOne('App\SendMessage', 'msg_id', 'msg_id')->with(['user', 'route']);
    }

    public function successMsg()
    {

        return $this->hasOne('App\SendMessage', 'msg_id', 'msg_id')->where(['status' => 3]);

    }

    public function customer()
    {

        return $this->hasOne('App\Customer', 'id', 'user_id');

    }

    public function callback()
    {
        return $this->hasOne('App\Customer\Callback', 'message_id', 'msg_id');
    }


}
