<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delete extends Model
{
    protected $table = "deletes";

    protected $fillable = ['primary_value','name','others_value'];


}
