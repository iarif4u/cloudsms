<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorMessage extends Model
{
    protected $table = "error_message";
    protected $fillable = ['msg_id','send_id','message'];
}
