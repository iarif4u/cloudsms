<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/sms', 'API\APIController@send_api_sms')->name('send_api_sms');
Route::post('/sms', 'API\APIController@send_api_sms');

Route::get('/sms/user', 'API\FeedbackController@index')->name('feedback_api_sms');
Route::get('/smpp/status', 'Admin\RouteController@check_smpp_status')->name('smpp_status');

//this is for sending queue message run by looping.php
Route::get('/sms/send', 'API\SMSSenderController@send_queue_message')->name('send_queue_message');
//this is for telerivet sms send
Route::get('telerivet/sms/send', 'API\TelerivetApiController@send_telerivet_sms')->name('telerivet_sms_send');
//update telerivet message status
Route::get('telerivet/sms/status', 'API\StatusUpdateController@update_telerivet_sms_status')->name('telerivet_update_status');
//transfer message queue to message send
Route::get('telerivet/sms/transfer', 'API\StatusUpdateController@change_message_to_send')->name('change_message_to_send');


//those route for diafaan
Route::any('/callback','API\CallBackController@get_call_back')->name('callback');
Route::any('/smpp','API\CallBackController@vai_smpp_req')->name('smpp');


Route::get('/report', 'API\ReportController@make_template_report');
