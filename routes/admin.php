<?php

Route::group(['middleware' => 'admin'], function() {
    Route::get('/home', 'Admin\HomeController@index')->name('home');
    Route::post('/kendo/piechart', 'Admin\HomeController@get_pie_chart_data')->name('kendo.piechart');
    Route::post('/kendo/barchart', 'Admin\HomeController@get_bar_chart_data')->name('kendo.barchart');
    Route::get('/profile', 'Admin\HomeController@get_profile_setting')->name('profile');
    Route::post('/profile', 'Admin\HomeController@update_profile_setting');
    Route::post('/time_zone', 'Admin\HomeController@update_time_zone')->name('time_zone');
    Route::get('/customer', 'Admin\CustomersController@get_customer_view')->name('customer');
    Route::post('/customer', 'Admin\CustomersController@make_new_customer');
    Route::post('/customer/update', 'Admin\CustomersController@update_customer')->name('update_customer');
    Route::post('/customer/delete', 'Admin\CustomersController@delete_customer')->name('delete_customer');
    Route::post('/customer/status', 'Admin\CustomersController@change_customer_status')->name('change_customer_status');
    Route::post('/customer/capacity', 'Admin\CustomersController@update_customer_capacity')->name('update_customer_capacity');
    Route::post('/customer/capacity/history', 'Admin\CustomersController@customer_capacity_history')->name('customer_capacity_history');
    Route::post('/customer/prefix', 'Admin\PrefixController@set_customer_prefix')->name('customer_prefix');
    Route::post('/customer/prefix/rate', 'Admin\PrefixController@get_customer_prefix_rate')->name('customer_prefix_rate');
    Route::get('/user/login/{customer}', 'HomeController@customer_login')->name('customer_login');
    Route::post('/user/key', 'Admin\CustomersController@update_customer_key')->name('customer_key');

    Route::get('/route', 'Admin\RouteController@get_route_view')->name('route');
    Route::post('/route', 'Admin\RouteController@make_route');
    Route::get('/route/smsbox', 'Admin\RouteController@get_smsbox_route_view')->name('smsbox');

    Route::post('/route/update', 'Admin\RouteController@update_route')->name('update_route');
    Route::post('/route/delete', 'Admin\RouteController@delete_route')->name('delete_route');
    Route::post('/route/status', 'Admin\RouteController@change_status')->name('change_status');
    Route::post('/route/capacity', 'Admin\RouteController@add_route_capacity')->name('add_route_capacity');
    Route::post('/route/capacity/history', 'Admin\RouteController@route_capacity_history')->name('route_capacity_history');
    Route::post('/route/prefix', 'Admin\PrefixController@set_route_prefix')->name('route_prefix');
    Route::post('/route/prefix/rate', 'Admin\PrefixController@get_route_prefix_rate')->name('route_prefix_rate');

    Route::get('/route/assign', 'Admin\RouteController@get_route_assign')->name('route_assign');
    Route::post('/route/assign', 'Admin\RouteController@make_route_assign');
    Route::post('/route/assign/update', 'Admin\RouteController@update_route_assign')->name('update_route_assign');
    Route::post('/route/assign/delete', 'Admin\RouteController@delete_route_assign')->name('delete_route_assign');
    Route::post('/route/assign/update/status', 'Admin\RouteController@update_route_assign_status')->name('update_route_assign_status');

    Route::group(['prefix' => 'setting'], function() {
        Route::get('/', function (){ return "This is Setting"; })->name('setting');
        Route::get('prefix', 'Admin\PrefixController@get_prefix_settings_view')->name('prefix_setting');
        Route::post('prefix', 'Admin\PrefixController@set_prefix_settings');
        Route::post('prefix/update', 'Admin\PrefixController@update_prefix_settings')->name("update_prefix_setting");
        Route::post('prefix/status', 'Admin\PrefixController@change_prefix_status')->name('change_prefix_status');
        Route::group(['prefix' => 'otp'], function() {
            Route::get('/', 'Admin\OtpVendorController@get_otp_vendor')->name('otp_vendor');
            Route::post('/', 'Admin\OtpVendorController@make_otp_vendor');
            Route::post('/vendor/update', 'Admin\OtpVendorController@update_vendor')->name('update_vendor');
            Route::post('/vendor/status', 'Admin\OtpVendorController@change_otp_vendor_status')->name('change_otp_vendor_status');
            Route::post('/vendor/delete', 'Admin\OtpVendorController@delete_otp_vendor')->name('delete_otp_vendor');

            Route::get('/string/input', 'Admin\OtpStringController@otp_input_string')->name('otp_input_string');
            Route::post('/string/input', 'Admin\OtpStringController@make_input_string');
            Route::post('/string/input/status', 'Admin\OtpStringController@change_input_string_status')->name('change_input_string_status');
            Route::post('/string/input/update', 'Admin\OtpStringController@update_input_string')->name('update_input_string');
            Route::post('/string/input/delete', 'Admin\OtpStringController@delete_input_string')->name('delete_input_string');

            Route::get('/string/output', 'Admin\OtpOutputStringController@otp_output_string')->name('otp_output_string');
            Route::post('/string/output', 'Admin\OtpOutputStringController@make_otp_output_string');
            Route::post('/string/output/update', 'Admin\OtpOutputStringController@update_output_string')->name('update_output_string');
            Route::post('/string/output/delete', 'Admin\OtpOutputStringController@delete_output_string')->name('delete_output_string');
            Route::post('/string/output/status', 'Admin\OtpOutputStringController@change_output_string_status')->name('change_output_string_status');


        });
    });
    Route::group(['middleware' => 'admin','prefix' => 'report'], function() {
        Route::get('/overcapacity', 'Admin\OverCapacityController@get_over_capacity')->name('over_capacity');
        Route::get('/smpp', 'Admin\SMPPController@get_smpp_report')->name('smpp');
        Route::get('/', 'Admin\MessageController@get_daily_report')->name('report');
        Route::post('/', 'Admin\MessageController@get_daily_report');
        Route::get('/date/range', 'Admin\MessageController@get_range_report_view')->name('range_report');
        Route::post('/date/range', 'Admin\MessageController@get_range_report');
        Route::get('/customer', 'Admin\MessageController@get_customer_report_view')->name('customer_report');
        Route::post('/customer', 'Admin\MessageController@get_customer_report');
    });

    Route::group(['middleware' => 'admin','prefix' => 'setting/telerivet'], function() {
        Route::get('/', 'API\TelerivetApiController@get_telerivet_info')->name('get_telerivet_info');
        Route::post('/', 'API\TelerivetApiController@make_telerivet_api');
        Route::post('/update', 'API\TelerivetApiController@update_telerivet_api')->name('update_telerivet_info');
        Route::post('/status', 'API\TelerivetApiController@update_telerivet_status')->name('update_telerivet_status');
        Route::post('/delete', 'API\TelerivetApiController@delete_telerivet_api')->name('delete_telerivet_api');

    });


    Route::group(['prefix' => 'shortlink','as'=>'shortlink.'], function () {
        Route::get('/', 'ShortLink\ShortLinkController@get_bitly_view')->name('shortlink');
        Route::post('/', 'ShortLink\ShortLinkController@make_new_bitly');
        Route::post('/update', 'ShortLink\ShortLinkController@update_bitLay_info')->name('bitLay_update');
        Route::post('/delete', 'ShortLink\ShortLinkController@delete_bitLay_info')->name('bitLay_delete');
        Route::post('/status', 'ShortLink\ShortLinkController@update_bitlay_status')->name('change_bit_lay_status');
        Route::get('/test', 'ShortLink\ShortLinkController@test_short_link');
        Route::get('/setting', 'ShortLink\ShortLinkController@get_regx_setting')->name('setting');
        Route::post('/setting', 'ShortLink\ShortLinkController@make_new_regx');
        Route::post('/setting/update', 'ShortLink\ShortLinkController@update_regular_expression')->name('update_setting');
        Route::post('/setting/delete', 'ShortLink\ShortLinkController@delete_regular_expression')->name('delete_setting');
        Route::post('/setting/status', 'ShortLink\ShortLinkController@change_regx_status')->name('change_regx_status');
        Route::get('/links', 'ShortLink\ShortLinkController@get_short_urls')->name('link');

    });
});
