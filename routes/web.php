<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['as'=>'customer.'], function () {
    Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('login');
    Route::get('/', 'CustomerAuth\LoginController@showLoginForm');
    Route::post('/login', 'CustomerAuth\LoginController@login');
    Route::post('/logout', 'CustomerAuth\LoginController@logout')->name('logout');
    Route::get('/logout', 'CustomerAuth\LoginController@logout');
});

Route::group(['prefix' => 'admin','as'=>'admin.'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::get('/', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::get('/logout', 'AdminAuth\LoginController@logout');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

});

Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'CustomerAuth\LoginController@login');
