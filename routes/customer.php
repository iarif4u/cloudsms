<?php

Route::get('/home', 'Customer\ReportController@get_daily_report_view')->name('home');
Route::get('/profile', 'Customer\HomeController@get_customer_profile_view')->name('profile');
Route::post('/profile', 'Customer\HomeController@update_customer_profile')->name('profile');
Route::post('/time_zone', 'Customer\HomeController@update_time_zone')->name('time_zone');

Route::group(['prefix' => 'report', 'as'=>'report.'], function()
{
    /*Route::get('/monthly', 'Customer\ReportController@get_monthly_report_view')->name('monthly_report');
    Route::post('/monthly', 'Customer\ReportController@set_daterange_report');

    Route::get('/prefix', 'Customer\ReportController@get_prefix_report_view')->name('prefix_report');
    Route::post('/prefix', 'Customer\ReportController@get_prefix_report');

    Route::get('/balance/history', 'Customer\ReportController@get_balance_history')->name('balance_history');
    Route::get('/pending', 'Customer\ReportController@get_pending_list')->name('pending_report');
    Route::get('/failed', 'Customer\ReportController@get_failed_list')->name('failed_report');

    Route::get('/over-capacity', 'Customer\ReportController@get_over_capacity')->name('over_capacity');

    Route::get('/date/number', 'Customer\ReportController@get_date_n_number')->name('date_number');
    Route::post('/date/number', 'Customer\ReportController@report_date_n_number');
    */
    Route::get('/combined/report', 'Customer\ReportController@get_combined_report')->name('combined_report');
    Route::post('/combined/report', 'Customer\ReportController@combined_report');
    Route::get('/status', 'Customer\ReportController@get_success_list')->name('success_report');

});
Route::get('/phonebooks', 'Customer\PhoneBookController@get_phone_book')->name('phonebooks');
Route::post('/phonebooks', 'Customer\PhoneBookController@create_phone_book');
Route::post('/phonebooks/update', 'Customer\PhoneBookController@update_phone_book')->name('phonebooks.update');
Route::post('/phonebooks/delete', 'Customer\PhoneBookController@delete_phone_book')->name('phonebooks.delete');
Route::post('/phonebooks/download', 'Customer\PhoneBookController@download_phone_book')->name('phonebooks.download');
Route::post('/contact', 'Customer\ContactController@create_contact_list')->name('contact');
Route::post('/contact/list', 'Customer\ContactController@get_contact_list')->name('get_contact_list');
Route::post('/contact/delete', 'Customer\ContactController@delete_contact_data')->name('delete_contact');
Route::post('/contact/update', 'Customer\ContactController@update_contact_data')->name('update_contact');
Route::post('/contact/excel', 'Customer\ContactController@import_contact_list')->name('upload_excel');

Route::group(['prefix' => 'sms', 'as'=>'sms.'], function()
{
    Route::get('/', 'Customer\SMSSenderController@get_sms_send_view')->name("customer_sms");
    Route::post('/', 'Customer\SMSSenderController@send_customer_sms');
    Route::get('/dynamic', 'Customer\SMSSenderController@get_dynamic_send_view')->name("dynamic_sms");
    Route::post('/dynamic', 'Customer\SMSSenderController@send_dynamic_sms');
    Route::post('/dynamic/excel', 'Customer\SMSSenderController@send_dynamic_excel_sms')->name("dynamic_sms_excel");
    Route::post('/info', 'Customer\SMSSenderController@get_sms_counter')->name("get_counter");
    Route::post('/book/contact', 'Customer\SMSSenderController@get_phonebook_contacts')->name("get_contacts");
    Route::post('/book/contact/file', 'Customer\SMSSenderController@get_file_contacts')->name("get_contacts_file");
});
