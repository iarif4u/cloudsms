<?php
/**
 * User: Md. Arif
 * Date: 6/1/2018
 * Time: 4:58 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo.jpg')}}"/>
    @include('admin.include.header')
    @yield('style')
    <title>@yield('title')</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

{{--include the top menu bar--}}
@include('admin.include.top_sidebar')
{{--include the left side bar--}}
@include('admin.include.left_sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div id="status"></div>
            <h1>
                @yield('header_left')
            </h1>
            <ol class="breadcrumb">
                @yield('header_right')
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <span class="each-error">{{ $error }} </span><br/>
                    @endforeach
                </div>

            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        <!-- Small boxes (Stat box) -->
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{--include the footer--}}
    @include('admin.include.footer')
</div>
<!-- ./wrapper -->
{{--link the js plugin--}}
@include('admin.include.javascript_bar')
@yield('script')
</body>
</html>
