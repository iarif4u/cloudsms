<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{route('home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>S</b>MS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg text-left"><b>SMS</b> Admin</span>
    </a>
    <div class="no-padding hidden-md hidden-lg hidden-sm col-md-4">
        @php
            $time_zone = array();
            $timestamp = time();
            foreach(timezone_identifiers_list() as $key => $zone) {
                date_default_timezone_set($zone);
                $time_zone[$zone] = $zone.' ( ' . date('P', $timestamp)." )";
            }
        @endphp
        {!! Form::open(['route' => 'time_zone','class'=>'time_zone_form']) !!}

        {!! Form::select('time_zone', $time_zone, auth()->user()->time_zone, [
        'class'=>'form-control time_zone js-example-basic-single']); !!}
        {!! Form::close() !!}
    </div>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown tasks-menu hidden-xs padding-top-8 custom-hide">


                    {!! Form::open(['route' => 'time_zone','class'=>'time_zone_form','class'=>'pull-right']) !!}

                    {!! Form::select('time_zone', $time_zone, auth()->user()->time_zone, [
            'class'=>'form-control time_zone js-example-basic-single']); !!}
                    {!! Form::close() !!}


                </li>


                <li @if(URL::current()==route('profile'))  class="profile-active dropdown user user-menu" @else
                class="dropdown user user-menu" @endif>
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        {{--<img src="{{asset('assets/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">--}}
                        <i class="fa fa-user-o"></i>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{asset('assets/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image')}}">
                            <p>
                                {{auth()->user()->email}}

                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" href="{{ route('admin.logout') }}">
                                    {{ __('Logout') }}
                                </a>

                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
