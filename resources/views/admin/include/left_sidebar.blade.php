<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>->
            <!-- Optionally, you can add icons to the links -->
            <li @if(URL::current()==route('home')) class="active" @endif>
                <a href="{{route('home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>
            <li @if(URL::current()==route('customer')) class="active" @endif>
                <a href="{{route('customer')}}">
                    <i class="fa fa-user" aria-hidden="true"></i> <span>Customer</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>


            <li @if(strpos(URL::current(), route('route')) !== false)  class="active treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-retweet" aria-hidden="true"></i> <span>Vendor</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul  class="treeview-menu">
                    <li @if(URL::current()==route('route')) class="active" @endif>
                        <a href="{{route('route')}}">
                            <i class="fa fa-comments" aria-hidden="true"></i> <span>SMS Gateway</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li @if(URL::current()==route('route_assign')) class="active" @endif>
                <a href="{{route('route_assign')}}">
                    <i class="fa fa-random" aria-hidden="true"></i> <span>Routing</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>

            <li @if(strpos(URL::current(), route('report')) !== false)  class="active treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-files-o" aria-hidden="true"></i> <span>Reports</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul  class="treeview-menu">
                    <li @if(URL::current()==route('report'))  class="active" @endif>
                        <a href="{{route('report')}}">
                            <i class="fa fa-circle-o"></i> Daily Report
                        </a>
                    </li>
                    <li @if(URL::current()==route('range_report'))  class="active" @endif>
                        <a href="{{route('range_report')}}">
                            <i class="fa fa-circle-o"></i> Range Report
                        </a>
                    </li>
                    <li @if(URL::current()==route('customer_report'))  class="active" @endif>
                        <a href="{{route('customer_report')}}">
                            <i class="fa fa-circle-o"></i> Report by Customer
                        </a>
                    </li>
                    <li @if(URL::current()==route('smpp'))  class="active" @endif>
                        <a href="{{route('smpp')}}">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>SMPP</span>
                        </a>
                    </li>
                    <li @if(URL::current()==route('over_capacity'))  class="active" @endif>
                        <a href="{{route('over_capacity')}}">
                            <i class="fa fa-hourglass-end" aria-hidden="true"></i> <span>Over Capacity</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li @if(strpos(URL::current(), route('setting')) !== false)  class="active treeview" @else
            class="treeview" @endif>
                <a href="#">
                    <i class="fa fa-gear"></i> <span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                        <li @if(URL::current()==route('prefix_setting'))  class="active" @endif>
                            <a href="{{route('prefix_setting')}}">
                                <i class="fa fa-circle-o"></i> <span>Prefix</span>
                            </a>
                        </li>
                    <li @if(strpos(URL::current(), route('get_telerivet_info')) !== false)  class="active treeview" @else
                    class="treeview" @endif>
                        <a href="javascript:void(0);">
                            <img width="22" height="22" src="{{asset('assets/img/telerivet.png')}}" alt="Telerivet">
                            <span>Telerivet</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                        </a>
                        <ul  class="treeview-menu">
                            <li @if(URL::current()==route('get_telerivet_info'))  class="active" @endif>
                                <a href="{{route('get_telerivet_info')}}">
                                    <i class="fa fa-circle-o"></i> <span>Telerivet API</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li @if(strpos(URL::current(), route('otp_vendor')) !== false)  class="active treeview" @else
                    class="treeview" @endif>
                        <a href="javascript:void(0);">
                            <i class="fa fa-product-hunt" aria-hidden="true"></i>
                            <span>OTP</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                        </a>
                        <ul  class="treeview-menu">
                            <li @if(URL::current()==route('otp_vendor'))  class="active" @endif>
                                <a href="{{route('otp_vendor')}}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Profiling</span>
                                </a>
                            </li>
                            <li @if(URL::current()==route('otp_input_string'))  class="active" @endif>
                                <a href="{{route('otp_input_string')}}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Input String</span>
                                </a>
                            </li>
                            <li @if(URL::current()==route('otp_output_string'))  class="active" @endif>
                                <a href="{{route('otp_output_string')}}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Output String</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li @if(strpos(URL::current(), route('shortlink.shortlink')) !== false)  class="active treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-link" aria-hidden="true"></i> <span>Short Link</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul  class="treeview-menu">
                    <li @if(URL::current()==route('shortlink.shortlink'))  class="active" @endif>
                        <a href="{{route('shortlink.shortlink')}}">
                            <i class="fa fa-circle-o"></i> Bit.ly Link
                        </a>
                    </li>
                    <li @if(URL::current()==route('shortlink.link'))  class="active" @endif>
                        <a href="{{route('shortlink.link')}}">
                            <i class="fa fa-circle-o"></i> Short Links
                        </a>
                    </li>
                    <li @if(URL::current()==route('shortlink.setting'))  class="active" @endif>
                        <a href="{{route('shortlink.setting')}}">
                            <i class="fa fa-circle-o"></i> Setting
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
