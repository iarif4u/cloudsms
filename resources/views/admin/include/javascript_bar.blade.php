<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- select2 -->
<script src="{{asset('assets/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('assets/js/adminlte.min.js')}}"></script>
{{--Select 2 js--}}
<script src="{{asset('assets/js/select2.min.js')}}"></script>
<!-- DC custom -->
<script src="{{asset('assets/js/custom.js')}}"></script>


<script>
    //click for get more details msg
    $(".more").click(function () {
        $(this).parent().find('.hidden').removeClass('hidden');
        $(this).addClass('hidden');
    });

    //click for get more details msg
    $(".less").click(function () {
        $(this).parent().find('.hidden').removeClass('hidden');
        $(this).parent().find('p').addClass('hidden');
        $(this).addClass('hidden');
    });
    $(document).ready(function() {
        $(".time_zone").change(function () {
          //  $(".time_zone_form").submit();
            $(this).parent().submit();
        });
        $('.js-example-basic-single').select2();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="tooltip"]').click(function () {
            $(this).tooltip("hide")
        });


    });


</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
