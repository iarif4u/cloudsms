@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Route
    <small>Route Details</small>
@endsection
@section('style')
    <!-- Datatable style -->
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.css"/>
    <style>
        .fa-circle:hover {
            cursor: pointer;
        }
    </style>
@endsection
@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Route</li>
@endsection

@section('content')
    <div class="box box-primary">

        <div class="box-header">
            <div class="row">
                <div class="col-md-6">

                    <table class="table table-hover table-bordered">
                        <thead class="thead-light">
                        <tr>
                            <th colspan="2">View of Route</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Total Route</td>
                            <td> @if(isset($route_list)) {{$route_list->count()}} @endif</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-new-customer">
                                <i class="fa fa-code-fork" aria-hidden="true"></i> Add New Route
                            </a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#modal-deletedroute">
                                <i class="fa fa-code-fork" aria-hidden="true"></i> Deleted Route
                            </a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-md-12 table table-responsive">
                        <table id="route_list"
                               class="table table-responsive table-bordered table-striped dataTable table-hover">
                            <thead>
                            <tr>
                                <th>SL
                                </th>
                                <th>
                                    Route Name
                                </th>

                                <th>
                                    Method
                                </th>
                                <th>
                                    Capacity
                                </th>
                                <th>
                                    Alert
                                </th>
                                <th>
                                    TPM
                                </th>
                                <th>BAR
                                </th>
                                <th>Status
                                </th>
                                <th>Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($route_list))
                                @foreach($route_list as $route)
                                    <tr>
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td class="route_name">{{$route->route_name}}</td>
                                        <td class="method">{{$route->method}}
                                            @if($route->method == "SMPP")
                                                @if($route->smpp_status)
                                                    <i onclick="check_smpp_status(this,'{{$route->server}}','{{$route->port}}','{{$route->user}}','{{$route->password}}')"
                                                       data-toggle="tooltip" data-html="true" title="<p>
                                                    Server: {{$route->server}} <br>
                                                    Port: {{$route->port}} <br>
                                                    User: {{$route->user}} <br>
                                                    Password: {{$route->password}} <br>
                                                    </p>" class='fa fa-circle fa-lg text-success'></i>
                                                @else
                                                    <i onclick="check_smpp_status(this,'{{$route->server}}','{{$route->port}}','{{$route->user}}','{{$route->password}}')"
                                                       data-toggle="tooltip" data-html="true" title="<p>
                                                    Server: {{$route->server}} <br>
                                                    Port: {{$route->port}} <br>
                                                    User: {{$route->user}} <br>
                                                    Password: {{$route->password}} <br>
                                                    </p>" class='fa fa-circle fa-lg text-danger'></i>
                                                @endif
                                                <i class="fa fa-spinner fa-pulse hidden"></i>
                                            @endif
                                        </td>
                                        <td class="route_capacity">{{$route->route_capacity}}</td>
                                        <td class="alert">{{$route->alert}}</td>
                                        <td class="tpm">{{$route->tpm}}</td>
                                        <td class="bar">{{$route->bar}}</td>
                                        <td class="status">@if($route->status==0) Inactive @else Active @endif</td>
                                        <td style="padding: 5px 0 !important;">
                                            @if($route->method == "SMPP")
                                                <div data-toggle="tooltip" title="Not Available" class="col-md-1">
                                                    <a href="javascript:void(0);"
                                                       class="btn btn-xs btn-primary disabled"> <i
                                                            class='fa fa-code'></i></a>
                                                </div>
                                            @else
                                                <div data-toggle="tooltip" title="Route API" class="col-md-1">
                                                    <a onclick="open_api_modal('{{$route->api}}')"
                                                       href="javascript:void(0);"
                                                       class="btn btn-xs btn-primary"> <i class='fa fa-code'></i></a>
                                                </div>
                                            @endif
                                            <div data-toggle="tooltip" title="Change status"
                                                 class="col-md-1 route_status_{{$route->route}}">
                                                @if($route->status==0)
                                                    <a onclick="change_status
                                                        ('{{$route->route}}','{{$route->status}}')"
                                                       href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                        <i class='fa fa-play-circle'></i>
                                                    </a>
                                                @else
                                                    <a onclick="change_status
                                                        ('{{$route->route}}','{{$route->status}}')"
                                                       href="javascript:void(0);"
                                                       class="btn btn-xs btn-success">
                                                        <i class='fa fa-pause'></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Route Prefix"
                                                   onclick="route_prefix('{{$route->route}}',this)"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs"> <i
                                                        class='fa
                                                    fa-phone-square'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="{{$route->route_name}} capacity add"
                                                   onclick="update_capacity_open('{{$route->route}}',this)"
                                                   href="javascript:void(0);" class="btn btn-success btn-xs"> <i class='fa
                                                   fa-plus-square'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Update route"
                                                   onclick="update_modal_open('{{$route->api}}','{{$route->route}}',
                                                       this,'{{$route->user}}','{{$route->password}}','{{$route->port}}','{{$route->server}}')"
                                                   href="javascript:void(0);" class="btn btn-xs btn-primary"> <i
                                                        class='fa
                                               fa-pencil-square-o'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">

                                                <a data-toggle="tooltip" title="Delete route"
                                                   onclick="delete_modal_open('{{$route->route}}',
                                                       this)"
                                                   href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                    <i class='fa fa-trash'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip"
                                                   onclick="history_route_modal_open('{{$route->route}}',
                                                       '{{$route->route_name}}')"
                                                   title="{{$route->route_name}} capacity history"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs">
                                                    <i class='fa  fa-history'></i>
                                                </a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Route Name</th>
                                <th>Method</th>
                                <th>Capacity</th>
                                <th>Alert</th>
                                <th>TPM</th>
                                <th>Bar</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--Modal start--}}
    {{--New customer modal--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-new-customer">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Route</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'route','autocomplete'=>'off']) !!}
                <input type="hidden" name="box_type" value="1">
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('routeName', 'Route Name') !!}
                                    {!! Form::text('routeName', null,['placeholder'=>"Enter The Route Name",
                                    'id'=>'routeName','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('routeCapacity', 'Route Capacity') !!}
                                    {!! Form::text('routeCapacity', null,['placeholder'=>"Enter Route Capacity",
                                    'id'=>'routeCapacity','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('routeStatus', 'Route Status') !!}
                                    {!! Form::select('routeStatus', ['1'=>'Active','0'=>'Inactive'], "1", [
                            'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;"]); !!}
                                </div>
                                <div class="form-group smpp-group">
                                    {!! Form::label('smpp_server', 'SMPP Server') !!}
                                    {!! Form::text('smpp_server', null,['placeholder'=>"Enter SMPP Server",
                                  'id'=>'smpp_server','class'=>'form-control']) !!}
                                    <input type="hidden" name="routeMethod" value="SMPP">
                                </div>
                                <div class="form-group smpp-group">
                                    {!! Form::label('smpp_port', 'SMPP Port') !!}
                                    {!! Form::text('smpp_port', null,['placeholder'=>"Enter SMPP Port",
                                  'id'=>'smpp_port','class'=>'form-control']) !!}
                                </div>

                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('routeAlert', 'Route Alert') !!}
                                    {!! Form::text('routeAlert', null,['placeholder'=>"Enter Route Alert",
                                    'id'=>'routeAlert','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeBar', 'Route Bar') !!}
                                    {!! Form::text('routeBar', null,['placeholder'=>"Enter Route Bar",
                                    'id'=>'routeBar','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeTPM', 'Route TPM') !!}
                                    {!! Form::text('routeTPM', null,['placeholder'=>"Enter Route TPM",
                                    'id'=>'routeTPM','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group smpp-group">
                                    {!! Form::label('smppUser', 'SMPP User') !!}
                                    {!! Form::text('smppUser', null,['placeholder'=>"SMPP User",
                                    'id'=>'routeTPM','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group smpp-group">
                                    {!! Form::label('smppPassword', 'SMPP Password') !!}
                                    {!! Form::text('smppPassword', null,['placeholder'=>"SMPP Password",
                                    'id'=>'routeTPM','class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--New customer modal end--}}
    {{--update customer modal start--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-update-customer">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Route</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_route','autocomplete'=>'off']) !!}
                {!! Form::hidden('update_route_id', null,['id'=>'update_route_id']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('routeName', 'Route Name') !!}
                                    {!! Form::text('routeName', null,['placeholder'=>"Enter The Route Name",
                                    'id'=>'editRouteName','class'=>'form-control',"readonly"=>""]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('routeCapacity', 'Route Capacity') !!}
                                    {!! Form::text('routeCapacity', null,['placeholder'=>"Enter Route Capacity",
                                    'id'=>'editRouteCapacity','class'=>'form-control','readonly'=>'1']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeTPM', 'Route TPM') !!}
                                    {!! Form::text('routeTPM', null,['placeholder'=>"Enter Route TPM",
                                    'id'=>'editRouteTPM','class'=>'form-control']) !!}
                                </div>

                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('routeAlert', 'Route Alert') !!}
                                    {!! Form::text('routeAlert', null,['placeholder'=>"Enter Route Alert",
                                    'id'=>'editRouteAlert','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeBar', 'Route Bar') !!}
                                    {!! Form::text('routeBar', null,['placeholder'=>"Enter Route Bar",
                                    'id'=>'editRouteBar','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeStatus', 'Route Status') !!}
                                    {!! Form::select('routeStatus', ['1'=>'Active','0'=>'Inactive'], "1", [
                            'class'=>'form-control js-example-basic-single editStatus','style'=>"width: 100% !important;"]);
                             !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('routeMethod', 'Route Method') !!}
                                    {!! Form::select('routeMethod', ['GET'=>'GET','POST'=>'POST',"SMPP"=>"SMPP"], "GET", ["id"=>"update_method",
                            'class'=>'form-control js-example-basic-single update_method','style'=>"width: 100%
                            !important;
                            "]); !!}
                                </div>
                                <div class="form-group smpp-group-update hidden">
                                    {!! Form::label('smpp_port_update', 'SMPP Port') !!}
                                    {!! Form::text('smpp_port', null,['placeholder'=>"Enter SMPP Port",
                                  'id'=>'smpp_port_update','class'=>'form-control',"readonly"=>""]) !!}
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group update_route_api">
                                    {!! Form::label('routeAPI', 'Route API') !!}
                                    {!! Form::textarea('routeAPI', null,['placeholder'=>"Enter Route API",
                                    'id'=>'editRouteId','rows'=>'4','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group smpp-group-update hidden">
                                    {!! Form::label('smppUserUpdate', 'SMPP User') !!}
                                    {!! Form::text('smppUser', null,['placeholder'=>"SMPP User",
                                    'id'=>'smppUserUpdate','class'=>'form-control',"readonly"=>""]) !!}
                                </div>
                                <div class="form-group smpp-group-update hidden">
                                    {!! Form::label('smppUserPassword', 'SMPP Password') !!}
                                    {!! Form::text('smppPassword', null,['placeholder'=>"SMPP Password",
                                    'id'=>'smppUserPassword','class'=>'form-control',"readonly"=>""]) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group smpp-group-update hidden">
                                    {!! Form::label('smpp_server', 'SMPP Server') !!}
                                    {!! Form::text('smpp_server', null,['placeholder'=>"Enter SMPP Server",
                                  'id'=>'smpp_server_update','class'=>'form-control',"readonly"=>""]) !!}
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New route Area -->
    {{--update route modal end--}}
    {{--delete route modal start--}}
    <div class="modal fade" id="modal-deleteRoute">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Route</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'delete_route','autocomplete'=>'off']) !!}
                {!! Form::hidden('route_id', null,['id'=>'deleteRouteId']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this Route???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete route modal end--}}
    {{--route capcity modal start--}}
    <div class="modal fade" id="modal-capacity">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Route Capacity</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'add_route_capacity','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('route_id', 'Route Name') !!}
                                </div>
                                <div class="form-group padding-top-8">
                                    {!! Form::label('routeCapacity', 'Route Capacity',['class'=>'padding-top-8']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::select('route_id', $route_list_select, "Select One", [
                        'class'=>'form-control js-example-basic-single route_list','style'=>"width: 100% !important;
                        "]);
                         !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::text('routeCapacity', null,['placeholder'=>"Enter The Route Capacity",
                                    'id'=>'customerName','class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--route capacity modal end--}}
    {{--histoery customer modal start--}}

    <div class="modal fade" id="modal-capacity-history">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Route capacity history</h4>
                    <b class="text-yellow"> Route Name : <span class="text-green route-name"> Route Name
                        </span></b>
                </div>
                <!-- form start -->
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Capacity</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--histoery customer modal end--}}
    {{--previous route modal start--}}
    <div class="modal fade" id="modal-deletedroute">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Deleted Route</h4>

                </div>
                <!-- form start -->
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">
                                <table id="example" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Route Name</th>
                                        <th>Capacity</th>
                                        <th>Alert</th>
                                        <th>TPM</th>
                                        <th>Bar</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($deleted_routes->count()>0)

                                        @foreach($deleted_routes as $router)
                                            @php
                                                $route = json_decode($router->others_value);
                                            @endphp
                                            <tr>
                                                <td>{{$route->route_name}}</td>
                                                <td>{{$route->route_capacity}}</td>
                                                <td>{{$route->alert}}</td>
                                                <td>{{$route->tpm}}</td>
                                                <td>{{$route->bar}}</td>
                                                <td>
                                                    @if($route->status==0)
                                                        Inactive
                                                    @else
                                                        Active
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--histoery route modal end--}}

    {{--customer prefix modal start--}}
    <div class="modal fade" id="modal-route-prefix">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Route Prefix</h4>
                    <b class="text-yellow"> Route Name : <span class="text-green route-prefix-name"> Route Name
                        </span></b>
                </div>
                <!-- form start -->
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">
                                {!! Form::open(['route' => 'route_prefix','autocomplete'=>'off','id'=>'prefix_form'])
                                 !!}
                                {!! Form::hidden('route_id', null,['id'=>'prefixRouteId']) !!}
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th style="width: 40%">Prefix</th>
                                        <th style="width: 40%">Rate</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody class="tr-prefix-row">

                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12">
                                <div class="col-md-1 row-add">

                                    <a href="javascript:void(0);" class="btn btn-success tr-add">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                                <div class="col-md-1 col-md-offset-9">
                                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--customer prefix modal end--}}

    {{--api view modal start--}}
    <div class="modal fade" id="modal-api-view">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Route API Details</h4>
                </div>
                <!-- form start -->

                <div class="modal-body">
                    <div class="row">

                        <div class="box-body text-center">
                            <code class="api">

                            </code>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
                <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--api view modal end--}}
    {{--Modal End--}}
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/scroller/2.0.0/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript">
        var route_name = "";
        var method = "";
        var route_id = "";
        var route_capacity = "";
        var route_alert = "";
        var tpm = "";
        var bar = "";
        var status = "";
        var delete_route_id = 0;

        function get_route_details(context) {
            route_name = $.trim($(context).parent().parent().parent().find('.route_name').text());
            route_id = $.trim($(context).parent().parent().parent().find('.route_id').text());
            route_capacity = $.trim($(context).parent().parent().parent().find('.route_capacity').text());
            route_alert = $.trim($(context).parent().parent().parent().find('.alert').text());
            tpm = $.trim($(context).parent().parent().parent().find('.tpm').text());
            bar = $.trim($(context).parent().parent().parent().find('.bar').text());
            status = $.trim($(context).parent().parent().parent().find('.status').text());
            method = $.trim($(context).parent().parent().parent().find('.method').text());
        }

        //status check of smpp router
        function check_smpp_status(context, server, port, user, password) {
            $(context).siblings('.fa-spinner').removeClass('hidden');
            $.ajax({
                type: "get",
                dataType: 'JSON',
                url: '{{route('smpp_status')}}',
                data: {'server': server, 'port': port, 'user': user, 'password': password},
                success: function (data) {
                    if (data != false) {
                        let resut = JSON.parse(data)
                        if (resut.status == true) {
                            $(context).addClass('text-success');
                            $(context).removeClass('text-danger');
                            $(context).siblings('.fa-spinner').addClass('hidden');
                        } else {
                            $(context).removeClass('text-success');
                            $(context).addClass('text-danger');
                            $(context).siblings('.fa-spinner').addClass('hidden');
                        }
                    } else {
                        $(context).removeClass('text-success');
                        $(context).addClass('text-danger');
                        $(context).siblings('.fa-spinner').addClass('hidden');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $(context).removeClass('text-success');
                    $(context).addClass('text-danger');
                    $(context).siblings('.fa-spinner').addClass('hidden');
                }
            });
        }

        /*open customer update modal*/
        function update_modal_open(api, routeId, context, user, password, port, server) {
            get_route_details(context);
            if (status.toLowerCase() === 'Active'.toLowerCase()) {
                status = '1';
            } else {
                status = '0';
            }
            $('.editStatus').val(status).trigger('change');
            $('.route_method').val(method).trigger('change');
            // document.getElementById("update_method").options[2].disabled = true;
            if (method == "SMPP") {
                $(".update_method").prop('disabled', 'disabled');
                $("#smpp_port_update").val(port);
                $("#smppUserUpdate").val(user);
                $("#smppUserPassword").val(password);
                $("#smpp_server_update").val(server);
                $(".update_route_api").addClass('hidden');
                $(".smpp-group-update").removeClass('hidden');
            } else {
                $(".update_method").prop('disabled', false);
                $(".smpp-group-update").addClass('hidden');
                $(".update_route_api").removeClass('hidden');
            }
            $("#update_route_id").val(routeId);

            $("#editRouteName").val(route_name);
            $("#editRouteCapacity").val(route_capacity);
            $("#editRouteTPM").val(tpm);
            $("#editRouteId").val(api);
            $("#editRouteAlert").val(route_alert);
            $("#editRouteBar").val(bar);
            $('#modal-update-customer').modal('toggle');

        }

        //open modal to customer delete
        function delete_modal_open(route_id) {
            delete_route_id = route_id;
            $("#deleteRouteId").val(delete_route_id);
            $("#modal-deleteRoute").modal('toggle');
        }

        //change route status
        function change_status(route_id, status) {
            var context = $('.route_status_' + route_id).children();
            if (status == 0) {
                var route_status = 1;
            } else {
                var route_status = 0;
            }
            $(context).attr("disabled", "disabled");
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('change_status')}}',
                data: {'_token': '{{csrf_token()}}', 'route_id': route_id, 'status': route_status},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        update_status_html(route_id, status, context);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
            update_status_html(route_id, status, context);
        }


        //change status update by js
        function update_status_html(route_id, status, context) {
            //take a second
            sleep(1000);
            if (status == 0) {
                var title = "Change status";
                var attr_class = "btn btn-xs btn-success";
                var i_class = "fa fa-pause";
                var update_status = 1;
                $(context).parent().parent().parent().find("td.status").text("Active");
            } else {
                var title = "Change status";
                var attr_class = "btn btn-xs btn-danger";
                var i_class = "fa fa-play-circle";

                var update_status = 0;
                $(context).parent().parent().parent().find("td.status").text("Inactive");
            }
            var data_toggle = "tooltip";
            var on_click = "change_status(" + route_id + "," + update_status + ")";
            $(context).removeAttr('disabled');
            $(context).attr("class", attr_class);
            $(context).attr("onclick", on_click);
            $(context).find('.fa').attr("class", i_class);


        }

        //sleep for milliseconds
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }

        //add capacity event. open add capacity modal
        function update_capacity_open(route_id, context) {

            $(".route_list>option[value='0']").remove();
            $(".route_list").val(route_id).trigger('change');
            $(".route_list>option[value='" + route_id + "']").removeAttr('disabled');
            var current = 0;
            $('.route_list').select2({
                minimumResultsForSearch: -1
            });
            $(".route_list>option").each(function () {
                current = this.value;
                if (current != route_id) {
                    $(".route_list>option[value='" + current + "']").attr('disabled', 'disabled');
                }
            });
            $("#modal-capacity").modal('toggle');
        }

        //open route prefix modal
        function route_prefix(route_id, context) {
            get_route_details(context);
            set_route_prefix_data(route_id);
            $(".route-prefix-name").text(route_name);
            $("#prefixRouteId").val(route_id);
            $("#modal-route-prefix").modal('toggle');

        }

        //set route prefix data rate list
        function set_route_prefix_data(route_id) {
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('route_prefix_rate')}}',
                data: {'_token': '{{csrf_token()}}', 'route_id': route_id},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        var row = null;
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<tr>\n' +
                                '<td>\n' +
                                '<input class="prefix-form"  name="prefix[]" value="' + data.message[i].prefix_info.id + '" type="hidden" >' +
                                '<input disabled class="form-control" value="' + data.message[i].prefix_info.prefix + '" type="text">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<input class="form-control prefix-form" value="' + data.message[i].rate + '" type="text"' +
                                ' name="rate[]">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                                '<i class="fa fa-remove"></i>\n' +
                                '</button>\n' +
                                '</td>\n' +
                                '</tr>';

                        }
                        if (data.message.length == 0) {
                            row += '<tr>\n' +
                                '<td>\n' +
                                '{{Form::select("prefix[]",$prefixList,null,["class"=>"prefix-form form-control"])}}' +
                                '</td>\n' +
                                '<td>\n' +
                                '<input class="form-control prefix-form" type="text"' +
                                ' name="rate[]">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                                '<i class="fa fa-remove"></i>\n' +
                                '</button>\n' +
                                '</td>\n' +
                                '</tr>';
                        }
                        $(".tr-prefix-row").html(row);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Prefix data encoding fail</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
        }

        //get route history details by modal
        function history_route_modal_open(route_id, route_name) {
            $(".route-name").text(route_name);
            $("#modal-capacity-history").modal('toggle');
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing': true,
                "autoWidth": false,
                'ajax': {
                    url: '{{route('route_capacity_history')}}',
                    data: {'_token': '{{csrf_token()}}', 'route_id': route_id},
                    type: "POST",
                },

            });
        }

        //click for get more details msg
        function open_api_modal(api_key) {
            $("code.api").text(api_key);
            $("#modal-api-view").modal('toggle');
        }

        $(".route_method").change(function () {
            if ($(this).val() === "SMPP") {
                $('.smpp-group').removeClass('hidden');
                $('.http-api-group').addClass('hidden');
                $(".basic-auth-group").addClass('hidden');
            } else {
                $('.http-api-group').removeClass('hidden');
                $('.smpp-group').addClass('hidden');
            }
        });
        $(".basicAuth").change(function () {
            if ($(this).val() == 1) {
                $(".basic-auth-group").removeClass('hidden');
            } else {
                $(".basic-auth-group").addClass('hidden');
            }
        });

        $(".basicAuth-update").change(function () {
            if ($(this).val() == 1) {
                $(".basic-auth-group-update").removeClass('hidden');
            } else {
                $(".basic-auth-group-update").addClass('hidden');
            }
        });
        //route prefix row add
        $('.row-add').on('click', '.tr-add', function () {
            var tr_row = '  <tr>\n' +
                '<td>\n' +
                '{{Form::select("prefix[]",$prefixList,null,["class"=>"prefix-form form-control"])}}' +
                '</td>\n' +
                '<td>\n' +
                '<input class="form-control prefix-form" type="text" name="rate[]">\n' +
                '</td>\n' +
                '<td>\n' +
                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                '<i class="fa fa-remove"></i>\n' +
                '</button>\n' +
                '</td>\n' +
                '</tr>';
            $(".tr-prefix-row").append(tr_row);
        });
        //remove parent tr row
        $('.tr-prefix-row').on('click', '.tr-remove', function () {
            $(this).parent().parent().remove();
        });
        //validate prefix form
        //call when prefix form submitted
        $("#prefix_form").submit(function (e) {
            var prefix_lsit = [];
            $(".has_error").removeClass('has_error');
            $(".error_msg").remove();
            $(".rate_error_msg").remove();
            $(".prefix_error_msg").remove();
            $("[name='rate[]']").each(function () {
                if (!$(this).val()) {
                    e.preventDefault();
                    $(this).addClass('has_error');
                    $(this).parent().append("<span class='rate_error_msg'>Prefix rate can't be null</span>")
                }
            });
            $("[name='prefix[]']").each(function () {
                if (!$(this).val()) {
                    e.preventDefault();
                    $(this).addClass('has_error');
                    $(this).parent().append("<span class='prefix_error_msg'>Prefix can't be null</span>");
                    return;
                }
                if (jQuery.inArray(this.value, prefix_lsit) !== -1) {
                    e.preventDefault();
                    $(this).addClass('has_error');
                    $(this).parent().append("<span class='error_msg'>Duplicate prefix found</span>")
                }
                prefix_lsit.push(this.value);
            });


        });
        $("#route_list").dataTable({
            "columns": [
                {"width": "5%"},
                {"width": "20%"},
                null,
                null,
                null,
                null,
                null,
                null,
                {"orderable": false},
            ]
        });
    </script>

@endsection
