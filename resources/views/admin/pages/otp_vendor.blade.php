@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('style')
    <style>
        table.dataTable.no-footer {
            border-bottom: none;
        }

        .fade-scale {
            transform: scale(0);
            opacity: 0;
            -webkit-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;
        }

        .fade-scale.in {
            opacity: 1;
            transform: scale(1);
        }
    </style>
@endsection
@section('header_left')
    Vendor
    <small>Vendor Details</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Vendor</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="row">
                <div class="col-md-6">

                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">View of Vendor</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Total Vendor</th>
                            <td> @if(isset($otp_vendors)) {{$otp_vendors->count()}} @endif</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-newcustomer">
                                <i class="fa fa-user-plus" aria-hidden="true"></i> Add New Vendor
                            </a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#modal-deletedvendor">
                                <i class="fa fa-user-o" aria-hidden="true"></i> Deleted Vendor
                            </a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 20px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Vendor Name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 300px;"
                                    aria-label="Engine version: activate to sort column ascending">Action
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($otp_vendors))
                                @foreach($otp_vendors as $vendor)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td class="vendor_name">{{$vendor->vendor_name}}</td>
                                        <td class="status">@if($vendor->status==0)
                                                Inactive @else Active @endif</td>
                                        <td>
                                            <div data-toggle="tooltip" title="Change status"
                                                 class="col-md-1 route_status_{{$vendor->id}}">
                                                @if($vendor->status==0)
                                                    <a onclick="change_status
                                                        ('{{$vendor->id}}','{{$vendor->status}}')"
                                                       href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                        <i class='fa fa-play-circle'></i>
                                                    </a>
                                                @else
                                                    <a onclick="change_status
                                                        ('{{$vendor->id}}','{{$vendor->status}}')"
                                                       href="javascript:void(0);"
                                                       class="btn btn-xs btn-success">
                                                        <i class='fa fa-pause'></i>
                                                    </a>
                                                @endif
                                            </div>

                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Update Vendor"
                                                   onclick="update_modal_open
                                                       ('{{$vendor->id}}',
                                                       '{{$vendor->vendor_name}}','{{$vendor->status}}')"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs"> <i class='fa
                                               fa-pencil-square-o'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">

                                                <a data-toggle="tooltip" title="Delete Vendor"
                                                   onclick="delete_modal_open('{{$vendor->id}}',this)"
                                                   href="javascript:void(0);" class="btn btn-danger btn-xs">
                                                    <i class='fa fa-trash'></i>
                                                </a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">SL</th>
                                <th rowspan="1" colspan="1">Vendor Name</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--Modal start--}}
    {{--New coustomer modal--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-newcustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Vendor</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'otp_vendor','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('vendorName', 'Vendor Name') !!}
                                    {!! Form::text('vendorName', null,['placeholder'=>"Enter The Vendor Name",
                                    'id'=>'vendorName','class'=>'form-control']) !!}
                                </div>


                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">

                                {!! Form::label('status', 'Select Vendor Status') !!}

                                {!! Form::select('status', [1=>"Active",0=>"Inactive"], 1, [
                        'class'=>'form-control js-example-basic-single vendor_status','style'=>"width: 100% !important;
                        "]); !!}
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--New coustomer modal end--}}
    {{--update coustomer modal start--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-update-vendor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Vendor</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_vendor','autocomplete'=>'off']) !!}
                {!! Form::hidden('vendor_id', null,['id'=>'vendor_id']) !!}
                <div class="modal-body">
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('vendorName', 'Vendor Name') !!}
                                {!! Form::text('vendorName', null,['placeholder'=>"Enter The Vendor Name",
                                'id'=>'editVendorName','class'=>'form-control']) !!}
                            </div>


                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">

                            {!! Form::label('status', 'Select Vendor Status') !!}

                            {!! Form::select('status', [1=>"Active",0=>"Inactive"], 1, [
                    'class'=>'form-control js-example-basic-single vendor_status status_update','style'=>"width: 100%
                    !important;
                    "]); !!}
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--update coustomer modal end--}}
    {{--delete customer modal start--}}

    <div class="modal fade" id="modal-deletecustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Vendor</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'delete_otp_vendor','autocomplete'=>'off']) !!}
                {!! Form::hidden('vendor_id', null,['id'=>'delete_vendor']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this Vendor???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete customer modal end--}}
    {{--deleted vendor list modal start--}}
    <div class="modal fade" id="modal-deletedvendor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Deleted Vendors</h4>
                </div>

                <div class="modal-body table">
                    <table class="deleted_vendor table table-condensed table-hover">
                        <thead>
                        <th>Vendor Name</th>
                        <th>Status</th>
                        </thead>
                        <tbody>
                        @if(isset($deleted_vendors))
                        @if($deleted_vendors->count()>0)
                            @foreach($deleted_vendors as $vendor)
                                @php
                                    $vendor_data = json_decode($vendor->others_value);
                                @endphp
                                <tr>
                                    <td>{{$vendor_data->vendor_name}}</td>
                                    <
                                    <td>
                                        @if($vendor_data->status==0)
                                            Inactive
                                        @else
                                            Active
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @endif
                        </tbody>
                    </table>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}

                </div>

                <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--deleted vendor list modal end--}}

    {{--Modal End--}}
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript">
        /*remove search from select 2*/
        $(document).ready(function () {
            $('.vendor_status').select2({
                minimumResultsForSearch: -1
            });
        });

        /*open customer update modal*/
        function update_modal_open(vendor_id, vendor_name, status) {
            $("#vendor_id").val(vendor_id);
            $("#editVendorName").val(vendor_name);
            $(".status_update").val(status).trigger('change');
            $('#modal-update-vendor').modal('toggle');

        }

        //open modal to customer delete
        function delete_modal_open(vendor_id) {
            $("#delete_vendor").val(vendor_id);
            $("#modal-deletecustomer").modal('toggle');

        }

        //change route status
        function change_status(vendor_id, status) {
            var context = $('.route_status_' + vendor_id).children();
            if (status == 0) {
                var route_status = 1;
            } else {
                var route_status = 0;
            }
            $(context).attr("disabled", "disabled");
            sleep(1000);
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('change_otp_vendor_status')}}',
                data: {'_token': '{{csrf_token()}}', 'vendor_id': vendor_id, 'status': route_status},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        update_status_html(vendor_id, status, context);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
            update_status_html(vendor_id, status, context);
        }

        //chage status update by js
        function update_status_html(vendor_id, status, context) {
            //take a second

            if (status == 0) {
                var title = "Change status to inactive";
                var attr_class = "btn btn-xs btn-success";
                var i_class = "fa fa-pause";
                var update_status = 1;
                $(context).parent().parent().parent().find("td.status").text("Active");
            } else {
                var title = "Change status to active";
                var attr_class = "btn btn-xs btn-danger";
                var i_class = "fa fa-play-circle";
                var update_status = 0;
                $(context).parent().parent().parent().find("td.status").text("Inactive");
            }

            var on_click = "change_status(" + vendor_id + "," + update_status + ")";
            $(context).removeAttr('disabled');
            $(context).attr("title", title);
            $(context).attr("class", attr_class);
            $(context).attr("onclick", on_click);
            ;
            $(context).find('.fa').attr("class", i_class);


        }

        //sleep for milliseconds
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }

        //deleted vendor list datatable
        $(document).ready(function () {
            $(".deleted_vendor").dataTable();
        });
    </script>

@endsection
