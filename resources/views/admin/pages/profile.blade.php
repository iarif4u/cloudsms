<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 9/15/2018
 * Time: 5:17 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Profile</li>
@endsection

@section('header_left')
    Home
    <small>Profile</small>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
          <div class="col-md-12">
              <h4>Admin Profile Setting</h4>
          </div>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                {!! Form::open(['route' => 'profile','autocomplete'=>'off','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::text('name', auth()->user()->name,['placeholder'=>"Enter The Name",
                            'class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::text('email', auth()->user()->email,['placeholder'=>"Enter The Email",
                            'class'=>'form-control'])
                             !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Password',['class'=>'col-sm-2 control-label']) !!}

                        <div class="col-sm-8">
                            {!! Form::password('password', ['placeholder'=>"Enter The Password",
                            'class'=>'form-control']) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Phone Number',['class'=>'col-sm-2 control-label']) !!}

                        <div class="col-sm-8">
                        {!! Form::text('phone', auth()->user()->phone,['placeholder'=>"Enter The Phone Number",
                        'class'=>'form-control'])
                             !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('timezone', 'Time Zone',['class'=>'col-sm-2 control-label']) !!}

                        <div class="col-sm-8">
                            {!! Form::select('time_zone', $time_zone, auth()->user()->time_zone, [
            'class'=>'form-control js-example-basic-single']); !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .profile-active{
            background: #022b44;
            margin-left: 3px;
        }
    </style>
@endsection
