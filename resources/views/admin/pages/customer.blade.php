@extends('admin.layouts.master')

@section('title',"Winnerdevs Message")

@section('style')
    <!-- Datatable style -->
    <link rel="stylesheet" type="text/css"href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.css"/>
@endsection
@section('header_left')
    Customer
    <small>Customer Details</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Customer</li>
@endsection

@section('content')
    <div class="box-primary box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">View of customers</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Total Customer</th>
                            <td> @if(isset($customer_list)) {{$customer_list->count()}} @endif</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-new-customer">
                                <i class="fa fa-user-plus" aria-hidden="true"></i> Add New Customer
                            </a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#modal-previous-customer">
                                <i class="fa fa-user-times" aria-hidden="true"></i> Deleted Customer
                            </a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table-responsive table">
                        <table id="customers" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>
                                    SL
                                </th>
                                <th>
                                    Customer Name
                                </th>
                                <th>
                                    Balance
                                </th>
                                <th>
                                    Alert
                                </th>
                                <th>
                                    TPM
                                </th>
                                <th>
                                    SMPP Status
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($customer_list))
                                @foreach($customer_list as $customer)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>
                                            <div class="hidden customer_password">{{$customer->password}}</div>
                                            <div class="hidden customer_api_key">{{$customer->api_key}}</div>
                                            <div class="customer_name">{{$customer->name}}</div>
                                        </td>
                                        <td class="customer_capacity">{{$customer->user_capacity}}</td>
                                        <td class="customer_alert">{{$customer->alert}}</td>
                                        <td class="customer_tpm">{{$customer->user_tpm}}</td>
                                        <td>
                                            @if($customer->is_smpp)
                                                <a data-toggle="tooltip" data-html="true" title="<p>
                                                    User: {{$customer->system_id}} <br>
                                                    Port: {{$customer->smpp_port}} <br>
                                                    Password: {{$customer->smpp_password}} <br>
                                                    </p>"
                                                   class="btn btn-primary btn-xs" href="javascript:void(0);">
                                                    Active
                                                </a>
                                            @else
                                                Inactive
                                            @endif
                                        </td>
                                        <td class="status">
                                            <div class="customer_timezone hidden">
                                                {{$customer->login_data->time_zone}}
                                            </div>
                                            @if($customer->status==0)
                                                Inactive @else Active @endif</td>
                                        <td class="text-center" style="padding: 5px 0px !important;">
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" target="_blank"
                                                   title="Login to {{$customer->name}} Account"
                                                   class="btn  btn-primary btn-xs"
                                                   href="{{route('customer_login',['customer'=>$customer->id])}}">
                                                    <i class='fa fa-power-off'></i>
                                                </a>
                                            </div>
                                            <div data-toggle="tooltip" title="Change status" class="col-md-1
                                            route_status_{{$customer->id}}">
                                                @if($customer->status==0)
                                                    <a onclick="change_status
                                                        ('{{$customer->id}}','{{$customer->status}}')"
                                                       href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                        <i class='fa fa-play-circle'></i>
                                                    </a>
                                                @else
                                                    <a onclick="change_status
                                                        ('{{$customer->id}}','{{$customer->status}}')"
                                                       href="javascript:void(0);"
                                                       class="btn btn-xs btn-success">
                                                        <i class='fa fa-pause'></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="API"
                                                   onclick="open_customer_api_modal('{{$customer->id}}',this)"
                                                   class="btn btn-primary btn-xs" href="javascript:void(0);">
                                                    <i class='fa fa-code'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Customer Prefix"
                                                   onclick="customer_prefix('{{$customer->id}}',this)"
                                                   href="javascript:void(0);" class="btn btn-success btn-xs">
                                                    <i class='fa fa-phone-square'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Add Capacity"
                                                   onclick="update_capacity_open
                                                       ('{{$customer->id}}',this)"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs"> <i
                                                        class='fa
                                                   fa-plus-square'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Update" onclick="update_modal_open
                                                    ('{{$customer->id}}','{{$customer->email}}',this,'{{$customer->is_smpp}}','{{$customer->smpp_port}}','{{$customer->system_id}}','{{$customer->smpp_password}}')"
                                                   href="javascript:void(0);" class="btn btn-success btn-xs"> <i class='fa
                                               fa-pencil-square-o'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Delete" onclick="delete_modal_open('{{$customer->id}}',this)"
                                                   href="javascript:void(0);" class="btn btn-danger btn-xs">
                                                    <i class='fa fa-trash'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" onclick="history_modal_open('{{$customer->id}}',this)"
                                                   title="{{$customer->name}} capacity history"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs">
                                                    <i class='fa  fa-history'></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Customer Name</th>
                                {{--<th>Password</th>
                                <th>Email</th>
                                --}}
                                <th>Balance</th>
                                <th>Alert</th>
                                <th>TPM</th>
                                <th>SMPP Status</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--customer hidden login  form end --}}

    {{--Modal start--}}
    {{--New customer modal--}}
    <div class="modal fade" id="modal-new-customer">
        <div class="modal-dialog modal-lg">
            <div class="modal-content opacity">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Customer</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('customerName', 'Customer Name') !!}
                                    {!! Form::text('customerName', null,['placeholder'=>"Enter The Customer Name",'id'=>'customerName','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('customerEmail', 'Customer Email') !!}
                                    {!! Form::text('customerEmail', null,['placeholder'=>"Enter Customer Email",
                                    'id'=>'customerEmail','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('customerAlert', 'Customer Alert') !!}
                                    {!! Form::text('customerAlert', null,['placeholder'=>"Enter Customer Alert",
                                    'id'=>'customerAlert','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('is_smpp', 'SMPP Status') !!}
                                    {!! Form::select('is_smpp', [1=>"Enable",0=>"Disable"], "0", [
                        'class'=>'form-control js-example-basic-single input_smpp_status','style'=>"width: 100% !important;"]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('smpp_sys_id', 'System Id') !!}
                                    @if(old('is_smpp')==1)
                                        {!! Form::text('smpp_sys_id', null,['placeholder'=>"Enter System Id.",
                                        'id'=>'input_smpp_sys_id','class'=>'form-control smpp_form']) !!}
                                    @else
                                        {!! Form::text('smpp_sys_id', null,['placeholder'=>"Enter System Id.",
                                        'id'=>'input_smpp_sys_id','class'=>'form-control smpp_form disabled',"disabled"=>""]) !!}
                                    @endif
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('customerPassword', 'Customer Password') !!}
                                    {!! Form::text('customerPassword', null,['placeholder'=>"Enter Customer Password",
                                    'id'=>'customerPassword','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('customerBalance', 'Customer Balance') !!}
                                    {!! Form::text('customerBalance', null,['placeholder'=>"Enter Customer Balance",
                                    'id'=>'customerBalance','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('customerTPM', 'Customer TPM') !!}
                                    {!! Form::text('customerTPM', null,['placeholder'=>"Enter Customer Alert",
                                    'id'=>'customerTPM','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('smpp_port', 'SMPP Port') !!}
                                    @if(old('is_smpp')==1)
                                        {!! Form::text('smpp_port', null,['placeholder'=>"Enter SMPP Port No.",
                                        'id'=>'input_smpp_port','class'=>'form-control smpp_form']) !!}
                                    @else
                                        {!! Form::text('smpp_port', null,['placeholder'=>"Enter SMPP Port No.",
                                        'id'=>'input_smpp_port','class'=>'form-control smpp_form disabled',"disabled"=>""]) !!}
                                    @endif
                                </div>
                                <div class="form-group">
                                    {!! Form::label('smpp_password', 'SMPP Password') !!}
                                    @if(old('is_smpp')==1)
                                        {!! Form::text('smpp_password', null,['placeholder'=>"Enter SMPP Password.",
                                        'id'=>'input_smpp_password','class'=>'form-control smpp_form']) !!}
                                    @else
                                        {!! Form::text('smpp_password', null,['placeholder'=>"Enter SMPP Password.",
                                        'id'=>'input_smpp_password','class'=>'form-control smpp_form disabled',"disabled"=>""]) !!}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                {!! Form::label('timeZone', 'Select Time Zone for Customer') !!}

                                {!! Form::select('timeZone', $tzlist, "Asia/Dhaka", [
                        'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;"]); !!}
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--New customer modal end--}}

    {{--update customer modal start--}}
    <div class="modal fade" id="modal-update-customer">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Customer</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_customer','autocomplete'=>'off']) !!}
                {!! Form::hidden('customer_id', null,['id'=>'editCustomerId']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('customerName', 'Customer Name') !!}
                                    {!! Form::text('customerName', null,['placeholder'=>"Enter The Customer Name",
                                    'id'=>'editCustomerName','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('customerEmail', 'Customer Email') !!}
                                    {!! Form::text('customerEmail', null,['placeholder'=>"Enter Customer Email",
                                    'id'=>'editCustomerEmail','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('customerAlert', 'Customer Alert') !!}
                                    {!! Form::text('customerAlert', null,['placeholder'=>"Enter Customer Alert",
                                    'id'=>'editCustomerAlert','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('is_smpp', 'SMPP Status') !!}
                                    {!! Form::select('is_smpp', [1=>"Enable",0=>"Disable"], "0", [
                        'class'=>'form-control js-example-basic-single update_smpp_status','style'=>"width: 100% !important;"]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('smpp_sys_id', 'System Id') !!}
                                    {!! Form::text('smpp_sys_id', null,['placeholder'=>"Enter System Id.",
                                        'class'=>'form-control smpp_form update_smpp update_smpp_sys_id']) !!}
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('customerPassword', 'Customer Password') !!}
                                    {!! Form::text('customerPassword', null,['placeholder'=>"Enter Customer Password",
                                    'id'=>'editCustomerPassword','class'=>'form-control ']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('customerBalance', 'Customer Balance') !!}
                                    {!! Form::text('customerBalance', null,['readonly '=>'on','id'=>'editCustomerBalance','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('customerTPM', 'Customer TPM') !!}
                                    {!! Form::text('customerTPM', null,['placeholder'=>"Enter Customer Alert",
                                    'id'=>'editCustomerTPM','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('smpp_port', 'SMPP Port') !!}
                                    @if(old('is_smpp')==1)
                                        {!! Form::text('smpp_port', null,['placeholder'=>"Enter SMPP Port No.",
                                        'id'=>'update_smpp_port','class'=>'form-control']) !!}
                                    @else
                                        {!! Form::text('smpp_port', null,['placeholder'=>"Enter SMPP Port No.",
                                        'id'=>'update_smpp_port','class'=>'form-control']) !!}
                                    @endif
                                </div>
                                <div class="form-group">
                                    {!! Form::label('smpp_password', 'SMPP Password') !!}
                                    {!! Form::text('smpp_password', null,['placeholder'=>"Enter SMPP Password.",
                                         'class'=>'form-control update_smpp_password update_smpp smpp_form']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                {!! Form::label('timeZone', 'Select Time Zone for Customer') !!}
                                {!! Form::select('timeZone', $tzlist, "Asia/Dhaka", [
                        'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;",
                        'id'=>'editTimezone']); !!}
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--update coustomer modal end--}}

    {{--delete customer modal start--}}
    <div class="modal fade" id="modal-deletecustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Customer</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'delete_customer','autocomplete'=>'off']) !!}
                {!! Form::hidden('customer_id', null,['id'=>'deleteCustomerId']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this customer???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete customer modal end--}}

    {{--Previous customer modal start--}}
    <div class="modal fade" id="modal-previous-customer">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Previous Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table id="previous_customer" class="table table-bordered table-condensed table-hover"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th>
                                    Customer Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    SMS Balance
                                </th>
                                <th>
                                    SMS Alert
                                </th>
                                <th>TPM
                                </th>
                                <th>Delete Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($previous_customers))
                                @foreach($previous_customers as $customer)
                                    @php
                                        $customer_data = json_decode($customer->others_value);
                                    @endphp
                                    <tr>
                                        <td>{{$customer_data->name}}</td>

                                        <td>{{$customer_data->email}}</td>
                                        <td>{{$customer_data->user_capacity}}</td>
                                        <td>{{$customer_data->alert}}</td>
                                        <td>{{$customer_data->user_tpm}}</td>
                                        <td>{{date('d-m-Y  h:i A',strtotime($customer->created_at))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                </div>
                <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Previous customer modal end--}}

    {{--Capacity update modal start--}}
    <div class="modal fade" id="modal-customercapacity">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer Capacity</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_customer_capacity','autocomplete'=>'off']) !!}
                {!! Form::hidden('customer_id', null,['id'=>'deleteCustomerId']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-6">
                                <div class="form-group text-left">
                                    {!! Form::label('customer_id', 'Customer Name') !!}
                                    {!! Form::select('customer_id', (isset($customer_select))
                                    ?$customer_select:[0=>"Select One"],
                                    0, [
                          'class'=>'form-control js-example-basic-single customer_capacity_select','style'=>"width: 100%
                          !important;"]); !!}
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group text-left">
                                    {!! Form::label('customer_capacity', 'Capacity') !!}
                                    {!! Form::text('customer_capacity', null,['placeholder'=>"Enter Customer Balance",
                                    'id'=>'editCustomerBalance','class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-left">
                                    {!! Form::label('capacity_type', 'Select Paid/Due/Return') !!}
                                    {!! Form::select('capacity_type', [0=>'Select One',1=>'Paid',2=>'Due',3=>'Return'],
                                    0, [
                          'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;"]); !!}

                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Capacity update modal end--}}

    {{--Generated Api View Modal--}}
    <div class="modal fade" id="modal-api-generate">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Generated Customer API</h4>
                </div>
                <!-- form start -->

                <div class="modal-body">
                    <div class="row">

                        <div class="box-body text-center">
                            <code class="">
                                {{route('send_api_sms')}}?api_key=<span
                                    class="api_password"></span>&phone_no=[TO]&msg=[MESSAGE]
                            </code>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary pull-left new-api-key" href="javascript:void(0);">New Key</a>
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
                <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Generated Api View Modal End--}}

    {{--histoery customer modal start--}}
    <div class="modal fade" id="modal-capacity-history">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer capacity history</h4>
                    <b class="text-yellow"> Customer Name : <span class="text-green customer-name"> Customer Name
                        </span></b>
                </div>
                <!-- form start -->
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Balance</th>
                                        <th>Type</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--capacity history modal end--}}

    {{--customer prefix modal start--}}
    <div class="modal fade" id="modal-customer-prefix">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer Prefix</h4>
                    <b class="text-yellow"> Customer Name : <span class="text-green customer-name"> Customer Name
                        </span></b>
                </div>
                <!-- form start -->
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">
                                {!! Form::open(['route' => 'customer_prefix','autocomplete'=>'off','id'=>'prefix_form'])
                                 !!}
                                {!! Form::hidden('customer_id', null,['id'=>'prefixCustomerId']) !!}
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th style="width: 40%">Prefix</th>
                                        <th style="width: 40%">Rate</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody class="tr-prefix-row">

                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12">
                                <div class="col-md-1 row-add">

                                    <a href="javascript:void(0);" class="btn btn-success tr-add">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                                <div class="col-md-1 col-md-offset-9">
                                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--customer prefix modal end--}}
    {{--Modal End--}}
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/scroller/2.0.0/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript">
        var customer_name = "";
        var customer_password = "";
        var customer_api_key = "";
        var customer_email = "";
        var customer_capacity = "";
        var customer_alert = "";
        var customer_tpm = "";
        var customer_timezone = "";
        var delete_customer_id = 0;

        function get_customer_details(context) {
            customer_name = $.trim($(context).parent().parent().parent().find('.customer_name').text());
            customer_password = $.trim($(context).parent().parent().parent().find('.customer_password').text());
            customer_api_key = $.trim($(context).parent().parent().parent().find('.customer_api_key').text());
            customer_email = $.trim($(context).parent().parent().parent().find('.customer_email').text());
            customer_capacity = $.trim($(context).parent().parent().parent().find('.customer_capacity').text());
            customer_alert = $.trim($(context).parent().parent().parent().find('.customer_alert').text());
            customer_tpm = $.trim($(context).parent().parent().parent().find('.customer_tpm').text());
            customer_timezone = $.trim($(context).parent().parent().parent().find('.customer_timezone').text());
        }

        /*open customer update modal*/
        function update_modal_open(customer_id, customer_mail,context, smpp_status, smpp_port,sys_id,sys_pass) {
            $(".update_smpp_status").val(smpp_status).trigger('change');
            if (smpp_status == 0){
                $(".update_smpp_status").parent().addClass('hidden');
                $(".update_smpp_sys_id").parent().addClass('hidden');
                $(".update_smpp_password").parent().addClass('hidden');
                $("#update_smpp_port").parent().addClass('hidden');
            }else{
                $(".update_smpp_sys_id").parent().removeClass('hidden');
                $(".update_smpp_password").parent().removeClass('hidden');
                $(".update_smpp_status").parent().removeClass('hidden');
                $("#update_smpp_port").parent().removeClass('hidden');
                $("#update_smpp_port").val(smpp_port);
                $(".update_smpp_sys_id").val(sys_id);
                $(".update_smpp_password").val(sys_pass);
            }
            get_customer_details(context);

            $('#editTimezone').val(customer_timezone).trigger('change');
            $("#editCustomerId").val(customer_id);
            $("#editCustomerName").val(customer_name);
            $("#editCustomerTPM").val(customer_tpm);
            $("#editCustomerBalance").val(customer_capacity);
            $("#editCustomerPassword").val(customer_password);
            $("#editCustomerAlert").val(customer_alert);
            $("#editCustomerEmail").val(customer_mail);
            $('#modal-update-customer').modal('toggle');

        }

        //open modal to customer delete
        function delete_modal_open(customer_id) {
            delete_customer_id = customer_id;
            $("#deleteCustomerId").val(delete_customer_id);
            $("#modal-deletecustomer").modal('toggle');

        }

        //open capacity update modal
        function update_capacity_open(customer_id, context) {
            get_customer_details(context);
            $("#modal-customercapacity").modal('toggle');
            $(".customer_capacity_select>option[value='0']").remove();
            $(".customer_capacity_select").val(customer_id).trigger('change');
            var current = 0;
            $(".customer_capacity_select>option").each(function () {
                current = this.value;
                if (current != customer_id) {
                    $(".customer_capacity_select>option[value='" + current + "']").attr('disabled', 'disabled');
                }
                if (current == customer_id) {
                    $(".customer_capacity_select>option[value='" + current + "']").removeAttr('disabled');
                }
            });
            $('.customer_capacity_select').select2({
                minimumResultsForSearch: -1
            });
        }

        function history_modal_open(customer_id, context) {
            $("#modal-capacity-history").modal('toggle');
            get_customer_details(context);
            $(".customer-name").text(customer_name);
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing': true,
                "autoWidth": false,
                'ajax': {
                    url: '{{route('customer_capacity_history')}}',
                    data: {'_token': '{{csrf_token()}}', 'customer_id': customer_id},
                    type: "POST",
                },

            });
        }

        $(document).ready(function () {
            //deleted customer datatable
            $("#previous_customer").DataTable();
            $(".input_smpp_status").change(function () {
                if ($(this).val() == 1) {
                    $(".smpp_form").removeAttr('disabled');
                }
                if ($(this).val() == 0) {
                    $(".smpp_form").val('');
                    $(".smpp_form").attr('disabled', '');
                }
            });
            $(".update_smpp_status").change(function () {
                if ($(this).val() == 1) {
                    $("#update_smpp_port").removeAttr('disabled');
                    $(".update_smpp").removeAttr('disabled');
                }
                if ($(this).val() == 0) {
                    $("#update_smpp_port").attr('disabled', '');
                    $(".update_smpp").attr('disabled', '');
                }
            });
        });

        //open customer api modal when click api btn
        function open_customer_api_modal(customer_id, context) {
            get_customer_details(context);
            $(".api_password").text(customer_api_key);
            $("#modal-api-generate").modal('toggle');

        }

        //get new api key for customer
        $(".new-api-key").click(function () {
            var key = $(".api_password").text();
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('customer_key')}}',
                data: {'_token': '{{csrf_token()}}', 'customer_key': key},
                success: function (data) {
                    if (data.status == true) {
                        $(".api_password").text(data.key);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">New key generate failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
        });

        //get customer prefix details
        function customer_prefix(customer_id, context) {
            set_customer_prefix_data(customer_id);
            get_customer_details(context);
            $("#prefixCustomerId").val(customer_id);
            $("#modal-customer-prefix").modal('toggle');
            $(".customer-name").text(customer_name);
        }

        //set customer prefix data rate list
        function set_customer_prefix_data(customer_id) {
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('customer_prefix_rate')}}',
                data: {'_token': '{{csrf_token()}}', 'customer_id': customer_id},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        var row = null;
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<tr>\n' +
                                '<td>\n' +
                                '<input name="prefix[]" type="hidden" value="' + data.message[i].prefix_info.id + '" class="prefix-form">' +
                                '<input disabled class="form-control" value="' + data.message[i].prefix_info.prefix + '" type="text">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<input class="form-control prefix-form" value="' + data.message[i].rate + '" type="text"' +
                                ' name="rate[]">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                                '<i class="fa fa-remove"></i>\n' +
                                '</button>\n' +
                                '</td>\n' +
                                '</tr>';
                        }
                       /* if (data.message.length == 0) {
                            row += '<tr>\n' +
                                '<td>\n' +
                                '<input class="form-control prefix-form" type="text" ' +
                                'name="prefix[]">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<input class="form-control prefix-form" type="text"' +
                                ' name="rate[]">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                                '<i class="fa fa-remove"></i>\n' +
                                '</button>\n' +
                                '</td>\n' +
                                '</tr>';
                        }*/
                        $(".tr-prefix-row").html(row);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Prefix data encoding fail</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
        }

        //change customer status
        function change_status(route_id, status) {
            var context = $('.route_status_' + route_id).children();
            if (status == 0) {
                var route_status = 1;
            } else {
                var route_status = 0;
            }
            $(context).attr("disabled", "disabled");
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('change_customer_status')}}',
                data: {'_token': '{{csrf_token()}}', 'customer_id': route_id, 'status': route_status},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        update_status_html(route_id, status, context);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
            update_status_html(route_id, status, context);
        }

        function update_status_html(route_id, status, context) {
            //take a second
            sleep(1000);
            if (status == 0) {
                var title = "Change status";
                var attr_class = "btn btn-xs btn-success";
                var i_class = "fa fa-pause";
                var update_status = 1;
                $(context).parent().parent().parent().find("td.status").text("Active");
            } else {
                var title = "Change status";
                var attr_class = "btn btn-xs btn-danger";
                var i_class = "fa fa-play-circle";

                var update_status = 0;
                $(context).parent().parent().parent().find("td.status").text("Inactive");
            }
            var on_click = "change_status(" + route_id + "," + update_status + ")";
            $(context).removeAttr('disabled');
            $(context).attr("class", attr_class);
            $(context).attr("onclick", on_click);
            $(context).find('.fa').attr("class", i_class);


        }

        //sleep for milliseconds
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }


        $('.row-add').on('click', '.tr-add', function () {
            var tr_row = '  <tr>\n' +
                '<td>\n' +
                '{{Form::select("prefix[]",$prefixList,null,["class"=>"prefix-form form-control"])}}' +
                '</td>\n' +
                '<td>\n' +
                '<input class="form-control prefix-form" type="text" name="rate[]">\n' +
                '</td>\n' +
                '<td>\n' +
                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                '<i class="fa fa-remove"></i>\n' +
                '</button>\n' +
                '</td>\n' +
                '</tr>';
            $(".tr-prefix-row").append(tr_row);
        });
        //remove parent tr row
        $('.tr-prefix-row').on('click', '.tr-remove', function () {
            $(this).parent().parent().remove();
        });
        //call when prefix form submitted
        $("#prefix_form").submit(function (e) {
            var prefix_lsit = [];
            $(".has_error").removeClass('has_error');
            $(".error_msg").remove();
            $(".rate_error_msg").remove();
            $(".prefix_error_msg").remove();
            $("[name='rate[]']").each(function () {
                if (!$(this).val()) {
                    e.preventDefault();
                    $(this).addClass('has_error');
                    $(this).parent().append("<span class='rate_error_msg'>Prefix rate can't be null</span>")
                }
            });
            $("[name='prefix[]']").each(function () {
                if (!$(this).val()) {
                    e.preventDefault();
                    $(this).addClass('has_error');
                    $(this).parent().append("<span class='prefix_error_msg'>Prefix can't be null</span>");
                    return;
                }
                if (jQuery.inArray(this.value, prefix_lsit) !== -1) {
                    e.preventDefault();
                    $(this).addClass('has_error');
                    $(this).parent().append("<span class='error_msg'>Duplicate prefix found</span>")
                }
                prefix_lsit.push(this.value);
            });

        });

        //login to customer account
        function login_to_customer(email, password) {
            $(".customer_login_email").val(email);
            $(".customer_login_password").val(password);
            $("#customer_login").submit();
        }

        $("#customers").DataTable({
            "columns": [
                {"name": "SL", "orderable": "true",'width':'2%'},
                {"name": "Customer Name", "orderable": "true",'width':'15%'},
                {"name": "Balance", "orderable": "true",'width':'8%'},
                {"name": "Alert", "orderable": "true",'width':'6%'},
                {"name": "TPM", "orderable": "true",'width':'6%'},
                {"name": "SMPP Status", "orderable": "true",'width':'12%'},
                {"name": "Status", "orderable": "true",'width':'6%'},
                {"name": "Action", "orderable": false,'width':'20%'}
            ]
        });
    </script>
@endsection
