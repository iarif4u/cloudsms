@extends('admin.layouts.master')

@section('title',"Cloud Message")
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
@endsection

@section('header_left')
    Route Assign
    <small>Route Assign Details</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Route Assign Details</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="row">
                <div class="col-md-6">

                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">View of Route Assign List</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td scope="row">Total Route Assign</td>
                            <td> @if($route_assign_list){{$route_assign_list->count()}}@endif</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-newcustomer">
                                <i class="fa fa-code-fork" aria-hidden="true"></i> Assign Route
                            </a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover">
                            <thead>
                            <tr>
                                <th>SL
                                </th>
                                <th>
                                    Customer Name
                                </th>
                                <th>
                                    Route Name
                                </th>

                                <th>
                                    TPM
                                </th>
                                <th>
                                    Total Send
                                </th>
                                <th>Status
                                </th>
                                <th>Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($route_assign_list))
                                @foreach($route_assign_list as $route)
                                    @if($route->route && $route->customer)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{$i++}}</td>
                                            <td class="customer_name">@if($route->customer)
                                                    {{$route->customer->name}}@else Unknown @endif</td>
                                            <td class="route_name">{{$route->route->route_name}}</td>

                                            <td class="tpm">{{$route->tpm}}</td>
                                            <td class="tpm">@if($route->total_send){{$route->total_send}}
                                                @else 0 @endif
                                            </td>
                                            <td class="status">@if($route->status==0)
                                                    Inactive @else Active @endif</td>
                                            <td>
                                                <div data-toggle="tooltip" title="Change status" class="col-md-2
                                                 route_status_{{$route->id}}">
                                                    @if($route->status==0)
                                                        <a onclick="change_status
                                                            ('{{$route->id}}','{{$route->status}}')"
                                                           href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                            <i class='fa fa-play-circle'></i>
                                                        </a>
                                                    @else
                                                        <a onclick="change_status
                                                            ('{{$route->id}}','{{$route->status}}')"
                                                           href="javascript:void(0);"
                                                           class="btn btn-xs btn-success">
                                                            <i class='fa fa-pause'></i>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="col-md-2">
                                                    @php
                                                        $get_customer =0;
                                                        if($route->customer){
                                                          $get_customer=  $route->customer->id;
                                                        }
                                                    @endphp
                                                    <a data-toggle="tooltip" title="Update routing"
                                                       onclick="update_modal_open
                                                           ('{{$route->id}}',
                                                           this,'{{$route->status}}','{{$get_customer}}',
                                                           '{{$route->route->route}}','{{$route->tpm}}')"
                                                       href="javascript:void(0);" class="btn btn-xs btn-primary"> <i
                                                            class='fa
                                               fa-pencil-square-o'></i>
                                                    </a>
                                                </div>
                                                <div class="col-md-2">

                                                    <a data-toggle="tooltip" title="Delete routing"
                                                       onclick="delete_modal_open('{{$route->id}}',
                                                           this)"
                                                       href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                        <i class='fa fa-trash'></i>
                                                    </a>
                                                </div>

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Customer Name</th>
                                <th>Route Name</th>
                                <th>TPM</th>
                                <th>Total Send</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--Modal start--}}
    {{--New coustomer modal--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-newcustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Route Assign</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'route_assign','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('customer_id', 'Customer Name') !!}
                                    {!! Form::select('customer_id', $customer_list, 0, [
                            'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;"]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeTPM', 'Route Assign TPM') !!}
                                    {!! Form::text('routeTPM', null,['placeholder'=>"Enter The TPM",
                                    'id'=>'routeName','class'=>'form-control']) !!}
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('route_id', 'Route Name') !!}
                                    {!! Form::select('route_id', $route_list, 0, [
                            'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;"]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeStatus', 'Route Status') !!}
                                    {!! Form::select('routeStatus', [1=>'Active',0=>'Inactive'], 1, [
                            'class'=>'form-control js-example-basic-single','style'=>"width: 100% !important;"]); !!}
                                </div>

                            </div>


                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--New coustomer modal end--}}
    {{--update coustomer modal start--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-updatecustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Customer Route Assign</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_route_assign','autocomplete'=>'off']) !!}
                {!! Form::hidden('update_route_assign_id', null,['id'=>'update_route_assign_id']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('customer_id', 'Customer Name') !!}
                                    {!! Form::select('customer_id', $customer_list, 0, [
                            'class'=>'form-control js-example-basic-single route_assign_customer','style'=>"width: 100%
                            !important;"]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeTPM', 'Route Assign TPM') !!}
                                    {!! Form::text('routeTPM', null,['placeholder'=>"Enter The TPM",
                                    'id'=>'routeName','class'=>'form-control route_assign_tpm']) !!}
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('route_id', 'Route Name') !!}
                                    {!! Form::select('route_id', $route_list, 0, [
                            'class'=>'form-control js-example-basic-single route_assign_id','style'=>"width: 100%
                            !important;
                            "]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('routeStatus', 'Route Status') !!}
                                    {!! Form::select('routeStatus', ['1'=>'Active','0'=>'Inactive'], 1, [
                            'class'=>'form-control js-example-basic-single route_assign_status','style'=>"width: 100%
                            !important;"]); !!}
                                </div>

                            </div>


                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--update coustomer modal end--}}
    {{--delete customer modal start--}}

    <div class="modal fade" id="modal-deleteRoute">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Customer Route Assign</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'delete_route_assign','autocomplete'=>'off']) !!}
                {!! Form::hidden('route_assign_id', null,['id'=>'route_assign_id']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this Route Assign???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    {{--delete customer modal end--}}
    {{--Modal End--}}
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $("#example1").dataTable();

        /*open customer update modal*/
        function update_modal_open(routeAssignId, context, current_status, customer_id, route, route_tpm) {
            if (current_status == 0) {
                var status = '1';
            } else {
                var status = '0';
            }

            $('.route_assign_customer').val(customer_id).trigger('change');
            $('.route_assign_status').val(current_status).trigger('change');
            $('.route_assign_id').val(route).trigger('change');
            $('.route_assign_tpm').val(route_tpm);

            $("#update_route_assign_id").val(routeAssignId);

            $('#modal-updatecustomer').modal('toggle');

        }

        //open modal to customer delete
        function delete_modal_open(route_id) {
            delete_route_id = route_id;
            $("#route_assign_id").val(route_id);
            $("#modal-deleteRoute").modal('toggle');
        }

        //change route status
        function change_status(route_id, status) {
            var context = $('.route_status_' + route_id).children();
            if (status == 0) {
                var route_status = 1;
            } else {
                var route_status = 0;
            }
            $(context).attr("disabled", "disabled");
            //take a second
            sleep(1000);
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('update_route_assign_status')}}',
                data: {'_token': '{{csrf_token()}}', 'route_id': route_id, 'status': route_status},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                        $(context).removeAttr('disabled');
                    } else {
                        update_status_html(route_id, status, context);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
        }

        //chage status update by js
        function update_status_html(route_id, status, context) {

            if (status == 0) {
                var title = "Change status to inactive";
                var attr_class = "btn btn-xs btn-success";
                var i_class = "fa fa-pause";
                var update_status = 1;
                $(context).parent().parent().parent().find("td.status").text("Active");
            } else {
                var title = "Change status to active";
                var attr_class = "btn btn-xs btn-danger";
                var i_class = "fa fa-play-circle";
                $(context).parent().parent().parent().find("td.status").text("Inactive");
                var update_status = 0;
            }

            var on_click = "change_status(" + route_id + "," + update_status + ")";
            $(context).removeAttr('disabled');
            $(context).attr("title", title);
            $(context).attr("class", attr_class);
            $(context).attr("onclick", on_click);
            ;
            $(context).find('.fa').attr("class", i_class);
        }

        //sleep for milliseconds
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }
    </script>

@endsection
