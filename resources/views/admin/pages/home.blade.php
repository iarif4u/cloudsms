<?php
/**
 * User: Md. Arif
 * Date: 6/1/2018
 * Time: 5:57 PM
 */
?>
@extends('admin.layouts.master')

@section('title')
    SMS || Cloud Message
@endsection

@section('header_left')
    Dashboard
    <small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{App\Customer::count()}}</h3>

                    <p>Customer</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{route('customer')}}" class="small-box-footer">More info <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{App\Route::where(['status'=>1])->count()}}</h3>
                    <p>Active Route</p>
                </div>
                <div class="icon padding-top-8">
                    <i class="fa fa-retweet"></i>
                </div>
                <a href="{{route('route')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{App\OtpVendor::where(['status'=>1])->count()}}</h3>

                    <p>OTP Vendor</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user-secret"></i>
                </div>
                <a href="{{route('otp_vendor')}}" class="small-box-footer">More info <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green-active">
                <div class="inner">
                    <h3>{{App\QueueMessage::count()}}</h3>

                    <p>SMS Send</p>
                </div>
                <div class="icon">
                    <i class="fa fa-envelope-o"></i>
                </div>
                <a href="{{route('report')}}" class="small-box-footer">More info <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    {{--Charts start--}}
    <div class="row">
        <div class="col-md-7">
            {!! $lineChart->render() !!}
            <h4 class="text-center  margin-top-10 padding-top-8"> {{date('F')}} month daily report </h4>
        </div>

        <div class="col-md-5">
            {!! $pieChart->render()!!}
            <div class="demo-section k-content wide">
                <div id="chart" style="background: #ecf0f5"></div>
            </div>
            <h4 class="text-center  margin-top-10 padding-top-8">
                Total Status Report
            </h4>
        </div>
    </div>
    <div class="row">
        {!! $barChart->render() !!}
    </div>
    {{--Charts end--}}
@endsection

@section('script')
    <script src="{{asset('assets/js/chart.min.js')}}"></script>
@endsection
