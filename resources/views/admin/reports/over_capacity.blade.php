@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>Over Capacity Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li>Report</li>
    <li class="active">Over Capacity</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">
                <div class="table table-responsive">
                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">

                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Total Over Capacity SMS</th>
                            <td>@if(isset($over_capacity)){{$over_capacity->count()}}@endif</td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box-header with-border">
                    <h3 class="box-title">SMS details by prefix</h3>
                </div>
                <div class="table table-responsive">
                    <table class="table table-condensed table-hover">
                        <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Prefix</th>
                            <th>Progress</th>
                            <th style="width: 40px">Percent</th>
                            <th style="width: 40px">Total</th>
                        </tr>
                        {{--array_sum($total_prefix)--}}
                        @if(isset($total_prefix))
                            @php
                                $i=1;
                            @endphp
                            @foreach($total_prefix as $prefix => $value)
                                @if(array_sum($total_prefix)>0)
                                    @php
                                        $percent = floor($value*100/array_sum($total_prefix));
                                    @endphp
                                @else
                                    @php
                                        $percent = 0;
                                    @endphp
                                @endif
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$prefix}}</td>
                                    <td>
                                        <div class="progress progress-xs active progress-striped">
                                            <div class="progress-bar
                                        @if($percent>70)
                                                    progress-bar-success
@elseif($percent>=50)
                                                    progress-bar-primary
@elseif($percent>=30)
                                                    progress-bar-yellow
@else
                                                    progress-bar-danger
@endif
                                                    " style="width: {{$percent}}%"></div>
                                        </div>
                                    </td>
                                    <td><span class="badge
                                    @if($percent>70)
                                                bg-green
@elseif($percent>=50)
                                                bg-light-blue
@elseif($percent>=30)
                                                bg-yellow
@else
                                                bg-red
@endif
                                                ">{{$percent}}%</span>
                                    </td>
                                    <td>{{$value}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="col-sm-12 table-responsive table">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 181.217px;" aria-sort="ascending"
                                aria-label="Rendering engine: activate to sort column descending">SL
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 223.533px;" aria-label="Browser: activate to sort column ascending">
                                User
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 223.533px;" aria-label="Browser: activate to sort column ascending">
                                Message
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                Mobile Number
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 154.8px;"
                                aria-label="Engine version: activate to sort column ascending">Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                Time
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i=1;
                        @endphp
                        @if($over_capacity)
                            @foreach($over_capacity as $message)
                                @php
                                    $triggerOn = $message->created_at;
                                    $schedule_date = new DateTime($triggerOn);
                                    $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                    $msg_date =  $schedule_date->format('Y-m-d H:i:s');
                                @endphp
                                <tr role="row" class="odd">
                                    <td>{{$i++}}</td>
                                    <td>{{$message->user->name}}</td>
                                    <td>{{$message->message}}</td>
                                    <td>{{$message->phone}}</td>
                                    <td>{{date('d-m-Y',strtotime($msg_date))}}</td>
                                    <td>{{date('h:i A',strtotime($msg_date))}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">SL</th>
                            <th rowspan="1" colspan="1">User</th>
                            <th rowspan="1" colspan="1">Message</th>
                            <th rowspan="1" colspan="1">Mobile Number</th>
                            <th rowspan="1" colspan="1">Date</th>
                            <th rowspan="1" colspan="1">Time</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection