@extends('admin.layouts.master')

@section('title',"Cloud Message")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/daterangepicker.css')}}">
    <!-- Datatable style -->
    <link rel="stylesheet" type="text/css"href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.css"/>
@endsection
@section('header_left')
    Report
    <small>Report by Customer</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-4">
                {{--<h6 class="box-title text-center">Daily report view for : {{ucfirst(auth()->user()->name)}} </h6>
                <h5>Current capacity : {{$user_capacity->user_capacity}}</h5>
                <h5>Total SMS send : {{array_sum($total_prefix)}}</h5>--}}
                {!! Form::open(['route' => 'customer_report','autocomplete'=>'off','class'=>"customer_form"]) !!}
                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th class="col-md-1">Date:</th>
                        <th class="col-md-11">
                            @php
                                $date = (isset($date_range))?$date_range:null;
                            @endphp

                            {!! Form::text('selectDate',$date ,['class'=>'form-control
                            text-center','style'=>"width: 110%;margin-left: -10%;"])
                             !!}

                        </th>
                    </tr>
                    <tr>
                        <th class="col-md-1">Customer:</th>
                        <td class="col-md-11">
                            @php
                                $select_customer = (isset($customer_id)) ? $customer_id : 0;
                            @endphp
                            {!! Form::select('customer_id', $customer_list, $select_customer, [
                            'class'=>'form-control customer_list js-example-basic-single','style'=>"width: 100%
                            !important;"]); !!}
                        </td>
                    </tr>
                    <tr>
                        <th  class="col-md-5" scope="col">Total SMS Send</th>
                        <th class="col-md-7">@if(isset($reportData)){{$reportData->count()}}@endif</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Submit" class="btn btn-primary">
                        </td>
                    </tr>
                    </thead>

                </table>
                {!! Form::close() !!}
            </div>
            <div class="col-md-8">
                <div class="table table-responsive">
                    @if(isset($chartjs))
                        {!! $chartjs->render() !!}
                    @endif
                </div>
            </div>


        </div>
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable table-responsive"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                    style="width: 20px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting" style="width: 40px !important;">
                                    Customer
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 30px;" aria-label="Platform(s): activate to sort column ascending">
                                    Number
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 100px !important;" aria-label="Platform(s): activate to sort column
                                    ascending">
                                    Message
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 20px;" aria-label="Platform(s): activate to sort column ascending">
                                    Unit
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 50px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 50px;"
                                    aria-label="Engine version: activate to sort column ascending">Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 40px;" aria-label="CSS grade: activate to sort column ascending">
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if($reportData->count()>0)
                                @foreach($reportData as $message)
                                    @php

                                        $schedule_date = new DateTime($message->created_at);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $msg_date =  $schedule_date->format('Y-m-d H:i:s');

                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->user->name}}
                                        </td>
                                        <td>{{$message->phone_number}}</td>

                                        <td>
                                            {{$message->message}}
                                        </td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>@if($message->status==3) Success @elseif($message->status==2) Fail
                                            @elseif($message->status==1) Success
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($msg_date))}}</td>
                                        <td>{{date('h:i A',strtotime($msg_date))}}</td>
                                    </tr>

                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Customer</th>
                                <th>Number</th>
                                <th>Message</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
@section('script')
    <script src="{{asset('assets/js/chart.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function() {
            $("#example1").dataTable();
            $('input[name="selectDate"]').daterangepicker({
                showDropdowns: true,
                autoApply:false,
                locale: { 'format': "DD/MM/YYYY"}
            });
        });
    </script>
@endsection
