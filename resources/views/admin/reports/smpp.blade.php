@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>SMPP Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li>Report</li>
    <li class="active">SMPP</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">
                <div class="table table-responsive">
                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">

                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Total SMPP SMS</th>
                            <td>@if(isset($messages)){{$messages->count()}}@endif</td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="col-sm-12 table-responsive table">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 181.217px;" aria-sort="ascending"
                                aria-label="Rendering engine: activate to sort column descending">SL
                            </th>
                            <th>
                                Guid
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 223.533px;" aria-label="Browser: activate to sort column ascending">
                                User
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 223.533px;" aria-label="Browser: activate to sort column ascending">
                                Message
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                Mobile Number
                            </th>
                            <th>Status Code</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 154.8px;"
                                aria-label="Engine version: activate to sort column ascending">Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                Time
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i=1;
                        @endphp
                        @if($messages)
                            @foreach($messages as $message)
                                @php
                                    $schedule_date = new DateTime($message->created_at);
                                    $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                    $msg_date =  $schedule_date->format('Y-m-d H:i:s');
                                @endphp
                                @if($message->message_data)
                                    <tr role="row" class="odd">
                                        <td>{{$i++}}</td>
                                        <td>{{$message->message_guid}}</td>
                                        <td>{{$message->message_data->customer->name}}</td>
                                        <td>{{$message->message}}</td>
                                        <td>{{$message->to}}</td>
                                        <td>
                                            @if($message->status==0)
                                                Queue
                                            @elseif($message->status==1)
                                                Pending
                                            @elseif($message->status==2)
                                                Processing
                                            @else
                                                Processing
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($msg_date))}}</td>
                                        <td>{{date('h:i A',strtotime($msg_date))}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">SL</th>
                            <th rowspan="1" colspan="1">Guid</th>
                            <th rowspan="1" colspan="1">User</th>
                            <th rowspan="1" colspan="1">Message</th>
                            <th rowspan="1" colspan="1">Mobile Number</th>
                            <th rowspan="1" colspan="1">Status Code</th>
                            <th rowspan="1" colspan="1">Date</th>
                            <th rowspan="1" colspan="1">Time</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
