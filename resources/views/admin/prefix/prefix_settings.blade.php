@extends('admin.layouts.master')

@section('title',"Cloud Message")
@section('header_left')
    Settings
    <small>Prefix</small>
@endsection
@section('style')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.css"/>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Prefix</li>
@endsection

@section('content')
    <div class="box-primary box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">View of Prefix</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Total Prefix</th>
                            <td> @if(isset($customer_list)) {{$customer_list->count()}} @endif</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-new-prefix">
                                <i class="fa fa-user-plus" aria-hidden="true"></i> Add New Prefix
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered prefix-table">
                        <thead>
                        <tr>
                            <th>Lable</th>
                            <th>Prefix</th>
                            <th>Color</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($prefixList->count()>0)
                            @foreach($prefixList as $prefix)
                                <tr>
                                    <td>
                                        {{$prefix->label}}
                                    </td>
                                    <td>
                                        {{$prefix->prefix}}
                                    </td>
                                    <td class="text-center">
                                        {{$prefix->color}}
                                        <div style="background: {{$prefix->color}};width:100%;height: 20px"></div>
                                    </td>
                                    <td class="td_{{$prefix->id}}">
                                        {{($prefix->status)?"Active":"Inactive"}}
                                    </td>
                                    <td>
                                        <div data-toggle="tooltip" title="Change status"
                                             class="col-md-1 prefix_status_{{$prefix->id}}">
                                            @if($prefix->status==0)
                                                <a onclick="change_status
                                                    ('{{$prefix->id}}','{{$prefix->status}}')"
                                                   href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                    <i class='fa fa-play-circle'></i>
                                                </a>
                                            @else
                                                <a onclick="change_status
                                                    ('{{$prefix->id}}','{{$prefix->status}}')"
                                                   href="javascript:void(0);"
                                                   class="btn btn-xs btn-success">
                                                    <i class='fa fa-pause'></i>
                                                </a>
                                            @endif
                                        </div>
                                        <div data-toggle="tooltip" title="Prefix Update" class="col-md-1">
                                            <a onclick="update_prefix('{{$prefix->id}}','{{$prefix->label}}','{{$prefix->prefix}}','{{$prefix->color}}')"
                                               href="javascript:void(0);" class="btn btn-xs btn-primary">
                                                <i class='fa fa-edit'></i>
                                            </a>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--  Start New Prefix -->
    <div class="modal fade" id="modal-new-prefix">
        <div class="modal-dialog">
            <div class="modal-content opacity">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Prefix</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'prefix_setting','autocomplete'=>'off']) !!}
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body ">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        Label
                                    </th>
                                    <th>
                                        Prefix
                                    </th>
                                    <th>
                                        Color
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="prefix-body">
                                <tr>
                                    <td>
                                        {!! Form::text("label[]",null,["class"=>"form-control"]) !!}
                                    </td>
                                    <td> {!! Form::text("prefix[]",null,["class"=>"form-control"]) !!}</td>
                                    <td>
                                        <label for="prefix_color">
                                            <input id="prefix_color" name="color" type="color">
                                        </label>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-12">
                                <br>
                                <a href="javascript:void(0);" class="btn btn-success" id="add-row"><i
                                        class="fa fa-plus"></i></a>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Prefix -->
    <!--  Update Prefix-->
    <div class="modal fade" id="modal-update-prefix">
        <div class="modal-dialog">
            <div class="modal-content opacity">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Prefix</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_prefix_setting','autocomplete'=>'off']) !!}
                {{csrf_field()}}
                <input name="prefix_id" class="prefix_id" type="hidden">
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body ">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        Label
                                    </th>
                                    <th>
                                        Prefix
                                    </th>
                                    <th>
                                        Color
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="prefix-body">
                                <tr>
                                    <td>
                                        {!! Form::text("label",null,["class"=>"form-control prefix_update_label"]) !!}
                                    </td>
                                    <td> {!! Form::text("prefix",null,["class"=>"form-control prefix_update_prefix"]) !!}</td>
                                    <td>
                                        <label for="prefix_color">
                                            <input id="prefix_update_color" name="color" type="color">
                                        </label>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End Update Prefix -->
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/scroller/2.0.0/js/dataTables.scroller.min.js"></script>
    <script>
        $("#add-row").click(function () {
            let row = ' <tr>\n' +
                '                                    <td>\n' +
                '                                        {!! Form::text("label[]",null,["class"=>"form-control"]) !!}\n' +
                '                                    </td>\n' +
                '                                    <td> {!! Form::text("prefix[]",null,["class"=>"form-control"]) !!}</td>\n' +

                '<td>\n' +
                '                                        <label for="prefix_color">\n' +
                '                                            <input id="prefix_color" name="color" type="color">\n' +
                '                                        </label>\n' +
                '                                    </td>' +
                '                                    <td>\n' +
                '                                        <a onclick="remove_row(this)" href="javascript:void(0);" class="btn btn-danger row-remove">\n' +
                '                                            <i class="fa fa-times"></i>\n' +
                '                                        </a>\n' +
                '                                    </td>\n' +
                '                                </tr>';

            $(".prefix-body").append(row);
        });

        function remove_row(context) {
            $(context).closest('tr').remove();
        }

        $(".prefix-table").dataTable({
            "columns": [
                {"name": "Label", "orderable": "true"},
                {"name": "Prefix", "orderable": "true"},
                {"name": "Color", "orderable": false},
                {"name": "Status", "orderable": "true"},
                {"name": "Action", "orderable": false}
            ]
        });

        function update_prefix(prefix_id, label, prefix, color) {
            $("#modal-update-prefix").modal("show");
            $("#prefix_update_color").val(color);
            $(".prefix_update_prefix").val(prefix);
            $(".prefix_id").val(prefix_id);
            $(".prefix_update_label").val(label);

        }

        //change prefix status
        function change_status(prefix_id, status) {
            if (status == 0) {
                var current_status = 1;
            } else {
                var current_status = 0;
            }
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('change_prefix_status')}}',
                data: {'_token': '{{csrf_token()}}', 'prefix_id': prefix_id, 'status': current_status},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        var row = '<div class="alert alert-success">';
                        row += '<span class="">Status update success</span><br/>';
                        row += '</div>';
                        $("#status").children().remove();
                        $("#status").html(row);
                        let inactive_html = ' <a onclick="change_status(' + prefix_id + ',' + current_status + ')"\n' +
                            '                                                   href="javascript:void(0);" class="btn btn-xs btn-danger">\n' +
                            '                                                    <i class="fa fa-play-circle"></i>\n' +
                            '                                                </a>';
                        let active_html = ' <a onclick="change_status(' + prefix_id + ',' + current_status + ')"\n' +
                            '                                                   href="javascript:void(0);" class="btn btn-xs btn-success">\n' +
                            '                                                    <i class="fa fa-pause"></i>\n' +
                            '                                                </a>';
                        let html = (current_status == 1) ? active_html : inactive_html;
                        let update_status = (current_status == 1) ? 'Active' : 'Inactive';
                        $(".prefix_status_" + prefix_id).html(html);
                        $(".td_" + prefix_id).html(update_status);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });

        }
    </script>
@endsection
