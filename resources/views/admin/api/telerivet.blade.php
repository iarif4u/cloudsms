@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('style')
    <style>
        table.dataTable.no-footer {
            border-bottom: none;
        }
        .fade-scale {
            transform: scale(0);
            opacity: 0;
            -webkit-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;
        }

        .fade-scale.in {
            opacity: 1;
            transform: scale(1);
        }
    </style>
@endsection
@section('header_left')
   API
    <small>Telerivet</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Telerivet API</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="row">
                <div class="col-md-6">

                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        </thead>
                        <tbody>
                       {{-- <tr>
                            <th>Active Telerivet API</th>
                            <td class="total_status_active">@if(isset($string_status)){{$string_status[1]}}@endif</td>
                        </tr>
                        <tr>
                            <th>Inactive Telerivet API</th>
                            <td class="total_status_inactive">@if(isset($string_status)){{$string_status[0]}}@endif</td>
                        </tr>--}}
                        <tr>
                            <th scope="row">Total Telerivet API </th>
                            <td> @if(isset($telerivetList)) {{$telerivetList->count()}} @endif</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-newcustomer">
                                <i class="fa  fa-paper-plane-o" aria-hidden="true"></i> New Telerivet API
                            </a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#modal-deletedvendor">
                                <i class="fa fa-send" aria-hidden="true"></i> Deleted Telerivet API
                            </a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 20px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting " tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Telerivet Label
                                </th>
                                <th class="sorting hidden" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Telerivet API Key
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width:50px;" aria-label="Platform(s): activate to sort column ascending">
                                    Telerivet Project Id
                                </th>
                                <th>Telerivet Route Id</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width:50px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>

                                <th class="sorting action_colum" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 300px;"
                                    aria-label="Engine version: activate to sort column ascending">Action
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($telerivetList))
                                @foreach($telerivetList as $telerivet)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td class="label_name_{{$telerivet->id}}">{{$telerivet->label}}</td>
                                        <td class="hidden api_{{$telerivet->id}}">{{$telerivet->api_key}}</td>
                                        <td class="project_{{$telerivet->id}}">{{$telerivet->project_id}}</td>
                                        <td class="route_id_{{$telerivet->id}}">{{$telerivet->route_id}}</td>
                                        <td class="status">
                                            @if($telerivet->status==0)
                                            Inactive @else Active @endif</td>
                                        <td>
                                            <div data-toggle="tooltip" title="API"  class="col-md-1">
                                                <a class="btn btn-xs btn-primary" onclick="generated_api_view
                                                        ('{{$telerivet->random_key}}')"
                                                    href="javascript:void(0);">
                                                    <i class='fa fa-code'></i>
                                                </a>
                                            </div>
                                            <div data-toggle="tooltip" title="Change Status" class="col-md-1
                                            route_status_{{$telerivet->id}}">
                                                @if($telerivet->status==0)
                                                    <a  title="Change status to active"  onclick="change_status
                                                            ('{{$telerivet->id}}','{{$telerivet->status}}')"
                                                        href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                        <i class='fa fa-play-circle'></i>
                                                    </a>
                                                @else
                                                    <a title="Change status to inactive" onclick="change_status
                                                            ('{{$telerivet->id}}','{{$telerivet->status}}')"
                                                       href="javascript:void(0);"
                                                       class="btn btn-xs btn-success">
                                                        <i class='fa fa-pause'></i>
                                                    </a>
                                                @endif
                                            </div>

                                            <div data-toggle="tooltip" title="Update" class="col-md-1">
                                                @php
                                                    $string_status =($telerivet->vendor)?$telerivet->vendor->id : 0;
                                                @endphp
                                                <a onclick="update_modal_open('{{$telerivet->id}}','{{$string_status}}','{{$telerivet->status}}')"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs"> <i class='fa
                                               fa-pencil-square-o'></i>
                                                </a>
                                            </div>
                                            <div data-toggle="tooltip" title="Delete" class="col-md-1">

                                                <a onclick="delete_modal_open('{{$telerivet->id}}',this)"
                                                   href="javascript:void(0);" class="btn btn-danger btn-xs">
                                                    <i class='fa fa-trash'></i>
                                                </a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">SL</th>
                                <th rowspan="1" colspan="1">Telerivet Label</th>
                                <th class="hidden" rowspan="1" colspan="1">Telerivet API Key</th>
                                <th rowspan="1" colspan="1">Telerivet Project Id</th>
                                <th rowspan="1" colspan="1">Telerivet Route Id</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--Modal start--}}
    {{--New coustomer modal--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-newcustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Telerivet API</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'get_telerivet_info','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">

                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('telerivet_label', 'Telerivet Label') !!}
                                    {!! Form::text('telerivet_label', null,['placeholder'=>"Enter The Telerivet Label",
                                    'id'=>'telerivet_label','class'=>'form-control']) !!}
                                </div>

                            </div>
                            <!-- /.col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                {!! Form::label('telerivt_api_key', 'Telerivet API Key') !!}
                                {!! Form::text('telerivt_api_key', null,['placeholder'=>"Enter The Telerivet API Key",
                                   'id'=>'telerivt_api_key','class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                {!! Form::label('telerivt_project_id', 'Telerivet Project Id') !!}
                                {!! Form::text('telerivt_project_id', null,['placeholder'=>"Enter The Telerivet Project Id",'id'=>'telerivt_project_id','class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('telerivt_route_id', 'Telerivet Route Id') !!}
                                    {!! Form::text('telerivt_route_id', null,['id'=>'telerivt_route_id',
                                   'placeholder'=>"Enter The Telerivet Route Id",'class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('status', 'Telerivet API Status') !!}

                                    {!! Form::select('status', [1=>"Active",0=>"Inactive"], 1, [
                            'class'=>'form-control js-example-basic-single update_string_status','style'=>"width: 100%
                            !important;
                            "]); !!}
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--New coustomer modal end--}}
    {{--update coustomer modal start--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-update-vendor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Telerivet API</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'update_telerivet_info','autocomplete'=>'off']) !!}
                {!! Form::hidden('telerivet_id', null,['id'=>'telerivet_id']) !!}
                <div class="modal-body">
                    <div class="box-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('telerivet_label', 'Telerivet Label') !!}
                                {!! Form::text('telerivet_label', null,['placeholder'=>"Enter The Telerivet Label",
                                'id'=>'editTelerivet_label','class'=>'form-control']) !!}
                            </div>

                        </div>
                        <!-- /.col -->
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('telerivt_api_key', 'Telerivet API Key') !!}
                                {!! Form::text('telerivt_api_key', null,['placeholder'=>"Enter The Telerivet API Key",
                                   'id'=>'editTelerivt_api_key','class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                {!! Form::label('telerivt_project_id', 'Telerivet Project Id') !!}
                                {!! Form::text('telerivt_project_id', null,['placeholder'=>"Enter The Telerivet Project Id",
                                                           'id'=>'editTelerivt_project_id',
                                                           'class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('telerivt_route_id', 'Telerivet Route Id') !!}
                                {!! Form::text('telerivt_route_id', null,['id'=>'edit_telerivt_route_id',
                               'placeholder'=>"Enter The Telerivet Route Id",'class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('status', 'Telerivet API Status') !!}

                                {!! Form::select('status', [1=>"Active",0=>"Inactive"], 1, [
                        'class'=>'form-control js-example-basic-single update_telerivet_status',
                        'style'=>"width: 100%
                        !important;
                        "]); !!}
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--update coustomer modal end--}}
    {{--delete customer modal start--}}

    <div class="modal fade" id="modal-deletecustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Telerivet API</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'delete_telerivet_api','autocomplete'=>'off']) !!}
                {!! Form::hidden('string_id', null,['id'=>'delete_string']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this Telerivet API???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete customer modal end--}}

    {{--Generated Api View Modal--}}

    <div class="modal fade" id="modal-apigenerate">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Generated Telerivet API</h4>
                </div>
                <!-- form start -->

                <div class="modal-body">
                    <div class="row">

                        <div class="box-body text-center">
                            <code class="">
                                {{route('telerivet_sms_send')}}?random_key=<span
                                        class="random_key"></span>&number=[number]&msg=[message]
                            </code>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Generated Api View Modal End--}}

    {{--deleted vendor list modal start--}}
    <div class="modal fade" id="modal-deletedvendor">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Telerivet API</h4>
                </div>
                <div class="modal-body table">
                    <table class="deleted_vendor table table-condensed table-hover">
                        <thead>
                        <th>Telerivet API Label</th>
                        <th>Telerivet API Key</th>
                        <th>Telerivet Project Id</th>
                        <th>Status</th>
                        </thead>
                        <tbody>
                        @if(isset($deletedTelerivet))
                            @foreach($deletedTelerivet as $telerivet)
                                @php
                                    $raw_data = json_decode($telerivet->others_value);

                                @endphp
                                <tr>
                                    <td>{{$raw_data->label}}</td>
                                    <td>{{$raw_data->api_key}}</td>
                                    <td>{{$raw_data->project_id}}</td>
                                    <td>
                                        @if($raw_data->status==0)
                                            Inactive
                                        @else
                                            Active
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}

                </div>

                <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--deleted vendor list modal end--}}

    {{--Modal End--}}
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript">
        /*remove search from select 2*/
        $(document).ready(function () {
            $('.vendor_status').select2({
                minimumResultsForSearch: -1
            });
        });
        /*open customer update modal*/
        function update_modal_open(string_id,vendor_id,status) {
            var label = $.trim($(".label_name_"+string_id).text());
            var api_key = $.trim($(".api_"+string_id).text());
            var project_id = $.trim($(".project_"+string_id).text());
            var route_id = $.trim($(".route_id_"+string_id).text());
            $("#editTelerivet_label").val(label);
            $("#editTelerivt_api_key").val(api_key);
            $("#editTelerivt_project_id").val(project_id);
            $("#edit_telerivt_route_id").val(route_id);
            $("#telerivet_id").val(string_id);
            $('#modal-update-vendor').modal('toggle');

        }

        //open modal to customer delete
        function delete_modal_open(string_id) {
            $("#delete_string").val(string_id);
            $("#modal-deletecustomer").modal('toggle');

        }

        //change route status
        function change_status(vendor_id,status) {
            var context = $('.route_status_'+vendor_id).children();
            if (status==0){
                var route_status =1;
            }else{
                var route_status =0;
            }
            $(context).attr("disabled", "disabled");
            sleep(1000);
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('update_telerivet_status')}}',
                data: {'_token':'{{csrf_token()}}','telerivet_id':vendor_id,'status':route_status},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    }else{
                        update_status_html(vendor_id,status,context);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
        }

        //chage status update by js
        function update_status_html(vendor_id,status,context) {
            //take a second

            if (status==0){
                var title =  "Change status to inactive";
                var attr_class =  "btn btn-xs btn-success";
                var i_class =  "fa fa-pause";
                var update_status =1;
                var status_active = parseInt($(".total_status_active").text())
                var status_inactive = parseInt($(".total_status_inactive").text())
                status_active+=1;
                status_inactive-=1;
                $(".total_status_active").text(status_active);
                $(".total_status_inactive").text(status_inactive);
                $(context).parent().parent().parent().find("td.status").text("Active");
            }else{
                var title =  "Change status to active";
                var attr_class =  "btn btn-xs btn-danger";
                var i_class =  "fa fa-play-circle";
                var update_status =0;
                var status_active = parseInt($(".total_status_active").text())
                var status_inactive = parseInt($(".total_status_inactive").text())
                status_active-=1;
                status_inactive+=1;
                $(".total_status_active").text(status_active);
                $(".total_status_inactive").text(status_inactive);
                $(context).parent().parent().parent().find("td.status").text("Inactive");
            }

            var on_click =  "change_status("+vendor_id+","+update_status+")";
            $(context).removeAttr('disabled');
            $(context).attr("title", title);
            $(context).attr("class", attr_class);
            $(context).attr("onclick", on_click);;
            $(context).find('.fa').attr("class", i_class);


        }
        //sleep for milliseconds
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                    break;
                }
            }
        }
        //deleted vendor list datatable
        $(document).ready(function () {
            $(".deleted_vendor").dataTable();
        });

        //view ganerated api view
        function generated_api_view(random_key) {
            $(".random_key").html(random_key);
            $("#modal-apigenerate").modal('toggle');

        }


    </script>

@endsection
