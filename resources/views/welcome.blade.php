<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>



<!-- /.col -->
<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li><a href="#timeline" data-toggle="tab">Timeline</a></li>--}}
            <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
            <li><a href="#password" data-toggle="tab">Change Password</a></li>
            <li><a href="#profession" data-toggle="tab">Profession</a></li>
        </ul>
        <div class="tab-content">

            <div class="tab-pane active" id="profile">
                {!! Form::open(['route' => 'customer.profile.profile', 'autocomplete'=>"off",'class'=>"form-horizontal"]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('name', auth()->user()->name,['placeholder'=>"Name", 'class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('emal', 'Email',['class'=>'col-sm-2 control-label']) !!}

                    <div class="col-sm-10">
                        {!! Form::email('email', auth()->user()->email,['placeholder'=>"Email", 'class'=>'form-control','disabled']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('phone', 'Phone',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('phone', auth()->user()->phone,['placeholder'=>"Phone Number", 'class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('address', 'Address',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::textarea('address', auth()->user()->address,['placeholder'=>"Address", 'class'=>'form-control','rows'=>2,'cols'=>3]) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('birth_date', 'Date of birth',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('birth_date', auth()->user()->birth_date,['placeholder'=>"Date of Birth", 'class'=>'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Blood Group</label>

                    <div class="col-sm-10">
                        @php
                            $blood_group = [
                            'a+'=>"A+",
                            'a-'=>"A-",
                            'b+'=>"B+",
                            'b-'=>"B-",
                            'o-'=>"O-",
                            'o+'=>"O+",
                            'ab+'=>"AB+",
                            'ab-'=>"AB-",
                            ]
                        @endphp
                        {{ Form::select('blood_group',$blood_group , auth()->user()->blood_group, array('class'=>'form-control blood-group', 'placeholder'=>'Blood Group')) }}
                        {{--<input type="text" class="form-control" id="inputSkills" placeholder="Skills">--}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="password">
                <h4 class="text-center text-info">Change Password</h4>
                <br>
                {!! Form::open(['route' => 'customer.profile.password', 'autocomplete'=>"off",'class'=>"form-horizontal"]) !!}
                <div class="form-group">
                    {!! Form::label('password', 'Password',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!!  Form::password('password', ['class'=>"form-control underlined",'placeholder'=>'Password']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirm',['class'=>'col-sm-2 control-label']) !!}

                    <div class="col-sm-10">
                        {!!  Form::password('password_confirmation', ['placeholder'=>'Retype password','class'=>"form-control underlined"]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="profession">
                {!! Form::open(['route' => 'customer.profile.profession', 'autocomplete'=>"off",'class'=>"form-horizontal"]) !!}
                <div class="row">
                    {{-- <div class="col-md-4">
                         <label for="profession" class="radio-inline control-label">Student</label>
                         <input name="profession" class="customer-profession" type="radio" value="Student"/>
                     </div>
                     <div class="col-md-4">
                         <input name="profession" class="customer-profession" type="radio" value="Service">
                         <label for="profession" class="radio-inline control-label">Service</label>

                     </div>
                     <div class="col-md-4">
                         <input name="profession" class="customer-profession" type="radio" value="Others">
                         <label for="profession" class="radio-inline control-label">Service</label>
                     </div>--}}
                    <div class="form-group profession-status">
                        {!! Form::label('profession', 'Profession',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            <input @if($profession)  @if($profession->profession=='Student') checked
                                   @endif @endif  name="profession" class="customer-profession" type="radio"
                                   value="Student">
                            <label style="padding: 0px 4px;padding-bottom: 8px;" for="profession"
                                   class="radio-inline control-label">Student</label>
                            <br>
                            <input @if($profession)  @if($profession->profession=='Service') checked
                                   @endif @endif  name="profession" class="customer-profession" type="radio"
                                   value="Service">
                            <label style="padding: 0px 4px;padding-bottom: 8px;" for="profession"
                                   class="radio-inline control-label">Service</label>
                            <br>
                            <input @if($profession)  @if($profession->profession=='Others') checked
                                   @endif @endif  name="profession" class="customer-profession" type="radio"
                                   value="Others">
                            <label style="padding: 0px 4px;padding-bottom: 8px;" for="profession"
                                   class="radio-inline control-label">Others</label>
                        </div>
                    </div>

                </div>
                <div @if($profession)  @if($profession->profession!='Student') style="display: none;"
                     @endif @endif class="content-part">
                    <div class="form-group">
                        {!! Form::label('class', 'Class',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!!  Form::text('class',($profession) ? $profession->class:null, ['class'=>"form-control underlined",'placeholder'=>'Class']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('institute', 'Current Institute',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!!  Form::text('institute',($profession) ? $profession->institute:null, ['placeholder'=>'Current Institute','class'=>"form-control underlined"]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('district', 'District',['class'=>'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {{ Form::select('district',$district_list , ($profession) ? $profession->district:null, array('class'=>'form-control district-list', 'placeholder'=>'Select District')) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
