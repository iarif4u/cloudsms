@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Short Link
    <small>Short Link List</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li>Short Link</li>
    <li class="active">Link List</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">
                <div class="table table-responsive">
                    <table class="table table-hover borderless table-condensed">
                        <tr>
                            <th scope="row">Total Short Link Create</th>
                            <td>@if(isset($short_links)){{$short_links->count()}}@endif</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="col-sm-12 table-responsive table">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 181.217px;" aria-sort="ascending"
                                aria-label="Rendering engine: activate to sort column descending">SL
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 223.533px;" aria-label="Browser: activate to sort column ascending">
                                Short Link
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 223.533px;" aria-label="Browser: activate to sort column ascending">
                                Long Link
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                Customer
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 154.8px;"
                                aria-label="Engine version: activate to sort column ascending">Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                Time
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i=1;
                        @endphp
                        @if($short_links)
                            @foreach($short_links as $short_links)
                                @php
                                    $triggerOn = $short_links->created_at;
                                    $schedule_date = new DateTime($triggerOn);
                                    $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                    $link_date =  $schedule_date->format('Y-m-d H:i:s');
                                @endphp
                                <tr role="row" class="odd">
                                    <td>{{$i++}}</td>
                                    <td>{{$short_links->url}}</td>
                                    <td>{{$short_links->long_url}}</td>
                                    <td>{{$short_links->customer->name}}</td>
                                    <td>{{date('d-m-Y',strtotime($link_date))}}</td>
                                    <td>{{date('h:i A',strtotime($link_date))}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">SL</th>
                            <th rowspan="1" colspan="1">User</th>
                            <th rowspan="1" colspan="1">Message</th>
                            <th rowspan="1" colspan="1">Mobile Number</th>
                            <th rowspan="1" colspan="1">Date</th>
                            <th rowspan="1" colspan="1">Time</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
