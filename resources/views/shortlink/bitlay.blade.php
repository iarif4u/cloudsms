@extends('admin.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Short Link
    <small>Bit.ly</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li>Short Link</li>
    <li class="active">Bit.ly</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="row">
                <div class="col-md-6">

                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">View of Bit.ly Accout</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Total Account</th>
                            <th scope="row">@if(isset($bitLays)){{$bitLays->count()}}@endif</th>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: -1%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modal-newcustomer">
                                <i class="fa fa-link" aria-hidden="true"></i> Add New Bit.ly Access Token
                            </a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#modal-deletedvendor">
                                <i class="fa fa-unlink" aria-hidden="true"></i> Deleted Bit.ly Access Token
                            </a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 20px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Label
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Token
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    TPM
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Short Links
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 300px;"
                                    aria-label="Engine version: activate to sort column ascending">Action
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($bitLays))
                                @foreach($bitLays as $bitLay)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td class="vendor_name">{{$bitLay->label}}</td>
                                        <td class="vendor_name">{{$bitLay->access_token}}</td>
                                        <td class="vendor_name">{{$bitLay->tpm}}</td>
                                        <td class="vendor_name">{{$bitLay->total}}</td>
                                        <td class="status_{{$bitLay->id}}">
                                            @if($bitLay->status==0)
                                                Inactive @else Active @endif
                                        </td>
                                        <td>
                                            <div data-toggle="tooltip" title="Change status"
                                                 class="col-md-1 route_status_{{$bitLay->id}}">
                                                @if($bitLay->status==0)
                                                    <a onclick="change_status
                                                        ('{{$bitLay->id}}','{{$bitLay->status}}')"
                                                       href="javascript:void(0);" class="btn btn-xs btn-danger">
                                                        <i class='fa fa-play-circle'></i>
                                                    </a>
                                                @else
                                                    <a onclick="change_status
                                                        ('{{$bitLay->id}}','{{$bitLay->status}}')"
                                                       href="javascript:void(0);"
                                                       class="btn btn-xs btn-success">
                                                        <i class='fa fa-pause'></i>
                                                    </a>
                                                @endif
                                            </div>

                                            <div class="col-md-1">
                                                <a data-toggle="tooltip" title="Update Vendor"
                                                   onclick="update_modal_open
                                                       ('{{$bitLay->id}}',
                                                       '{{$bitLay->label}}','{{$bitLay->status}}',
                                                       '{{$bitLay->tpm}}','{{$bitLay->access_token}}')"
                                                   href="javascript:void(0);" class="btn btn-primary btn-xs"> <i class='fa
                                               fa-pencil-square-o'></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">

                                                <a data-toggle="tooltip" title="Delete Vendor"
                                                   onclick="delete_modal_open('{{$bitLay->id}}',this)"
                                                   href="javascript:void(0);" class="btn btn-danger btn-xs">
                                                    <i class='fa fa-trash'></i>
                                                </a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">SL</th>
                                <th rowspan="1" colspan="1">Label</th>
                                <th rowspan="1" colspan="1">Token</th>
                                <th rowspan="1" colspan="1">TPM</th>
                                <th rowspan="1" colspan="1">Short Links</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--Modal start--}}
    {{--New Bit.ly Access Token modal--}}
    <!--  Start New Bit.ly Access Token -->
    <div class="modal fade" id="modal-newcustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Bit.ly Access Token</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'shortlink.shortlink','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('bitly_name', 'Bit.ly Label') !!}
                                    {!! Form::text('bitly_name', null,['placeholder'=>"Enter The Bit.ly Label",
                                    'id'=>'bitly_name','class'=>'form-control']) !!}
                                </div>


                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">


                                <div class="form-group">
                                    {!! Form::label('bitly_tpm', 'Bit.ly TPM') !!}
                                    {!! Form::text('bitly_tpm', null,['placeholder'=>"Enter The Bit.ly TPM",
                                    'class'=>'form-control']) !!}
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('bitly_token', 'Bit.ly Access Token') !!}
                                    {!! Form::text('bitly_token', null,['placeholder'=>"Enter The Bit.ly Access Token",
                                    'id'=>'bitly_name','class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('status', 'Select Status') !!}

                                    {!! Form::select('status', [1=>"Active",0=>"Inactive"], 1, [
                            'class'=>'form-control js-example-basic-single vendor_status','style'=>"width: 100% !important;
                        "]); !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Bit.ly Access Token Area -->
    {{--New Bit.ly Access Token modal end--}}
    {{--update coustomer modal start--}}
    <!--  Start New Customer -->
    <div class="modal fade" id="modal-update-vendor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Bit.ly Info</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'shortlink.bitLay_update','autocomplete'=>'off']) !!}
                {!! Form::hidden('bit_lay_id', null,['id'=>'bit_lay_id']) !!}
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('edit_bitly_name', 'Bit.ly Label') !!}
                            {!! Form::text('edit_bitly_name', null,['placeholder'=>"Enter The Bit.ly Label",
                            'class'=>'form-control bitly-name-edit']) !!}
                        </div>


                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">


                        <div class="form-group">
                            {!! Form::label('edit_bitly_tpm', 'Bit.ly TPM') !!}
                            {!! Form::text('edit_bitly_tpm', null,['placeholder'=>"Enter The Bit.ly TPM",
                            'class'=>'form-control bitly-tpm-edit']) !!}
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-8">
                        <div class="form-group">
                            {!! Form::label('edit_bitly_token', 'Bit.ly Access Token') !!}
                            {!! Form::text('edit_bitly_token', null,['placeholder'=>"Enter The Bit.ly Access Token",
                            'id'=>'bitly_name','class'=>'form-control bitly-token-edit']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('edit_status', 'Select Status') !!}

                            {!! Form::select('edit_status', [1=>"Active",0=>"Inactive"], 1, [
                    'class'=>'form-control js-example-basic-single vendor_status','style'=>"width: 100% !important;
                "]); !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--  End New Customer Area -->
    {{--update coustomer modal end--}}
    {{--delete customer modal start--}}

    <div class="modal fade" id="modal-deletecustomer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Bit.ly Access Token</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'shortlink.bitLay_delete','autocomplete'=>'off']) !!}
                {!! Form::hidden('bit_lay_id', null,['id'=>'delete_bitly_id']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this bit.ly account???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete customer modal end--}}
    {{--deleted vendor list modal start--}}
    <div class="modal fade" id="modal-deletedvendor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Deleted Vendors</h4>
                </div>

                <div class="modal-body table">
                    <table class="deleted_vendor table table-condensed table-hover">
                        <thead>
                        <th>Vendor Name</th>
                        <th>Status</th>
                        </thead>
                        <tbody>
                        @if(isset($deleted_bitlays))
                        @if($deleted_bitlays->count()>0)
                            @foreach($deleted_bitlays as $bitlay)
                                @php
                                    $bitlay_data = json_decode($bitlay->others_value);
                                @endphp
                                <tr>
                                    <td>{{$bitlay_data->name}}</td>
                                    <
                                    <td>
                                        @if($bitlay_data->status==0)
                                            Inactive
                                        @else
                                            Active
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @endif
                        </tbody>
                    </table>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}

                </div>

                <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--deleted vendor list modal end--}}

    {{--Modal End--}}
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript">
        /*remove search from select 2*/
        $(document).ready(function () {
            $('.vendor_status').select2({
                minimumResultsForSearch: -1
            });
        });

        /*open customer update modal*/
        function update_modal_open(bit_lay_id, vendor_name, status,tpm,bitly_token) {
            $("#bit_lay_id").val(bit_lay_id);
            $(".bitly-name-edit").val(vendor_name);
            $(".bitly-tpm-edit").val(tpm);
            $(".bitly-token-edit").val(bitly_token);
            $(".status_update").val(status).trigger('change');
            $('#modal-update-vendor').modal('toggle');

        }

        //open modal to customer delete
        function delete_modal_open(bit_lay_id) {
            $("#delete_bitly_id").val(bit_lay_id);
            $("#modal-deletecustomer").modal('toggle');

        }

        //change route status
        function change_status(bit_lay_id, status) {
            var context = $('.route_status_' + bit_lay_id).children();
            if (status == 0) {
                status = 1;
            } else {
                status = 0;
            }
            $(context).attr("disabled", "disabled");
            sleep(1000);
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('shortlink.change_bit_lay_status')}}',
                data: {'_token': '{{csrf_token()}}', 'bit_lay_id': bit_lay_id, 'status': status},
                success: function (data) {
                    if (data.error == "true") {
                        var row = '<div class="alert alert-danger">';
                        for (var i = 0; i < data.message.length; i++) {
                            row += '<span class="each-error">' + data.message[i] + '</span><br/>';
                        }
                        row += '</div>';
                        $("#status").html(row);
                    } else {
                        update_status_html(bit_lay_id, status, context);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Status update failed</span><br/>';
                    row += '</div>';

                    $("#status").html(row);
                }
            });
        }

        //chage status update by js
        function update_status_html(bit_lay_id, status, context) {
            //take a second
            if (status == 1) {
                var title = "Change status to inactive";
                var attr_class = "btn btn-xs btn-success";
                var i_class = "fa fa-pause";
                var update_status = 1;
                $(".status_"+bit_lay_id).text("Active");
            } else {
                var title = "Change status to active";
                var attr_class = "btn btn-xs btn-danger";
                var i_class = "fa fa-play-circle";
                var update_status = 0;
                $(".status_"+bit_lay_id).text("Inactive");
            }

            var on_click = "change_status(" + bit_lay_id + "," + update_status + ")";
            $(context).removeAttr('disabled');
            $(context).attr("title", title);
            $(context).attr("class", attr_class);
            $(context).attr("onclick", on_click);
            ;
            $(context).find('.fa').attr("class", i_class);


        }

        //sleep for milliseconds
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }

        //deleted vendor list datatable
        $(document).ready(function () {
            $(".deleted_vendor").dataTable();
        });
    </script>

@endsection
