<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>-->
            <!-- Optionally, you can add icons to the links -->
            <li @if(URL::current()==route('customer.home')) class="active" @endif>
                <a href="{{route('customer.home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>
            <li @if(strpos(URL::current(), route('customer.sms.customer_sms')) !== false)  class="active treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>SMS</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>

                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==route('customer.sms.customer_sms')) class="active" @endif>
                        <a href="{{route('customer.sms.customer_sms')}}">
                            <i class="fa fa-circle-o"></i> Send SMS
                        </a>
                    </li>
                    <li @if(URL::current()==route('customer.sms.dynamic_sms')) class="active" @endif>
                        <a href="{{route('customer.sms.dynamic_sms')}}">
                            <i class="fa fa-circle-o"></i> Send Dynamic SMS
                        </a>
                    </li>
                </ul>
            </li>
            <li @if(URL::current()==route('customer.phonebooks')) class="active" @endif>
                <a href="{{route('customer.phonebooks')}}">
                    <i class="fa  fa-book" aria-hidden="true"></i> <span>Address Book</span>
                </a>
            </li>
            <li @if(URL::current()==route('customer.report.combined_report')) class="active" @endif>
                <a href="{{route('customer.report.combined_report')}}">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i> <span>Combined Report</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>


          {{--  <li @if(URL::current()==route('customer.report.success_report')) class="active" @endif>
                <a href="{{route('customer.report.success_report')}}">
                    <i class="fa  fa-files-o" aria-hidden="true"></i> <span>Report</span>
                </a>
            </li>--}}


            {{--
              <li @if(URL::current()==route('customer.report.prefix_report')) class="active" @endif>
                <a href="{{route('customer.report.prefix_report')}}">
                    <i class="fa fa-user-secret" aria-hidden="true"></i> <span>Report by Prefix</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li @if(URL::current()==route('customer.report.pending_report')) class="active" @endif>
                <a href="{{route('customer.report.pending_report')}}">
                    <i class="fa fa-street-view" aria-hidden="true"></i> <span>Pending Report</span>
                </a>
            </li>
           <li @if(URL::current()==route('customer.report.failed_report')) class="active" @endif>
                <a href="{{route('customer.report.failed_report')}}">
                    <i class="fa fa-street-view" aria-hidden="true"></i> <span>Failed Report</span>
                </a>
            </li>
            <li @if(URL::current()==route('customer.report.balance_history')) class="active" @endif>
                <a href="{{route('customer.report.balance_history')}}">
                    <i class="fa fa-dollar" aria-hidden="true"></i> <span>Balance History</span>
                </a>
            </li>
            <li @if(URL::current()==route('customer.report.over_capacity')) class="active" @endif>
                <a href="{{route('customer.report.over_capacity')}}">
                    <i class="fa fa-dollar" aria-hidden="true"></i> <span>Over Capacity</span>
                </a>
            </li>--}}
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
