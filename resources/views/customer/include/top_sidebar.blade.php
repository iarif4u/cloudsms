<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <div class="no-padding hidden-xs">

    <a href="{{route('customer.home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <b class="">DLR</b>

        </span>


    </a>
    </div>
    <div class="no-padding hidden-md hidden-lg hidden-sm col-md-4">

        {!! Form::open(['route' => 'customer.time_zone','class'=>'time_zone_form']) !!}

        {!! Form::select('time_zone', $tzlist, auth()->user()->time_zone, [
        'class'=>'form-control time_zone']); !!}
        {!! Form::close() !!}
    </div>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown tasks-menu hidden-xs padding-top-8">
                    {!! Form::open(['route' => 'customer.time_zone','class'=>'time_zone_form', 'class'=>'pull-right']) !!}
                    {!! Form::select('time_zone', $tzlist, auth()->guard('customer')->user()->time_zone, [ 'class'=>'form-control time_zone']); !!}
                    {!! Form::close() !!}
                </li>
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <i class="fa fa-user"></i>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{asset('assets/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image')}}">
                            <p>
                                {{auth()->user()->email}}
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('customer.profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" href="{{ route('customer.logout') }}">
                                    {{ __('Logout') }}
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
