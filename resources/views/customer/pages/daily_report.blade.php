@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>Daily Report</small>
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
@endsection
@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">
                {{--<h6 class="box-title text-center">Daily report view for : {{ucfirst(auth()->user()->name)}} </h6>
                <h5>Current capacity : {{$user_capacity->user_capacity}}</h5>
                <h5>Total SMS send : {{array_sum($total_prefix)}}</h5>--}}
                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Daily report view for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Current Balance</th>
                        <td> {{$user_capacity->user_capacity}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Total SMS send</th>
                        <td>{{$queue_success->sum('msg_count') + $message_send->sum('msg_count')}}</td>

                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <div class="table table-responsive">
                    <table class="table table-condensed table-hover">
                        <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Prefix</th>
                            <th>Progress</th>
                            <th style="width: 40px">Percent</th>
                            <th style="width: 40px">Total</th>
                        </tr>
                        {{--array_sum($total_prefix)--}}
                        @if($total_prefix)
                            @php
                                $i=1;
                            @endphp
                            @foreach($total_prefix as $prefix => $value)
                                @if(array_sum($total_prefix)>0)
                                    @php
                                        $percent = floor($value*100/array_sum($total_prefix));
                                    @endphp
                                @else
                                    @php
                                        $percent = 0;
                                    @endphp
                                @endif
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$prefix}}</td>
                                    <td>
                                        <div class="progress progress-xs active progress-striped">
                                            <div class="progress-bar
                                        @if($percent>70)
                                                progress-bar-success
@elseif($percent>=50)
                                                progress-bar-primary
@elseif($percent>=30)
                                                progress-bar-yellow
@else
                                                progress-bar-danger
@endif
                                                " style="width: {{$percent}}%"></div>
                                        </div>
                                    </td>
                                    <td><span class="badge
                                    @if($percent>70)
                                            bg-green
@elseif($percent>=50)
                                            bg-light-blue
@elseif($percent>=30)
                                            bg-yellow
@else
                                            bg-red
@endif
                                            ">{{$percent}}%</span>
                                    </td>
                                    <td>{{$value}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable table-responsive"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th>SL
                                </th>
                                {{--<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Message ID
                                </th>--}}
                                <th>
                                    Mobile Number
                                </th>
                                <th>
                                    Unit
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>Date
                                </th>
                                <th>
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if($queue_success)
                                @foreach($queue_success as $message)
                                    @php
                                        $triggerOn = $message->created_at;

                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        {{-- <td>
                                             @if($message->send_id)
                                                 {{$message->send_id}}
                                             @else
                                                 {{ "MSG-".(((0x0000FFFF & $message->id) << 16) + ((0xFFFF0000 & $message->id) >> 16))}}
                                             @endif
                                         </td>--}}
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>@if($message->status==3) Success @elseif($message->status==2) Fail
                                            @elseif($message->status==1) Processing
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            @if($message_send)
                                @foreach($message_send as $message)
                                    @php
                                        $triggerOn = $message->created_at;
                                        $schedule_date = new DateTime($triggerOn, new DateTimeZone(auth()->user()->time_zone) );
                                        $schedule_date->setTimeZone(new DateTimeZone('UTC'));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->send_id}}</td>
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>@if($message->status==3) Success @elseif($message->status==2) Fail
                                            @elseif($message->status==1) Processing
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                {{--<th>Message ID</th>--}}
                                <th>Mobile Number</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").dataTable();
    </script>
@endsection
