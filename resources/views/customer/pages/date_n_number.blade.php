@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>Date And Number Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">

                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Date And Number view for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Current capacity </th>
                        <td> {{$user_capacity->user_capacity}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Total SMS send</th>
                        <td> @if(isset($message_send)) {{$message_send->sum('msg_count')+$queue_success->sum
                        ('msg_count')}} @endif</td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <form autocomplete="off" action="{{route('report.date_number')}}" method="post">
                {{csrf_field()}}
                <div class="col-md-2">
                    <h4>Select Date : </h4>
                    <br>
                    <h4 class="margin-none" style="margin: 0;">Number : </h4>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input @if(isset($date)) value="{{$date}}" @endif class="form-control pull-right"
                               id="reservation"
                               name="daterange"
                               type="text">
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div>
                        <input @if(isset($date)) value="{{$number}}" @endif class="form-control pull-right" id="" name="number" type="text">
                    </div>
                    <div class="input-group text-center margin-top-10">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>

                </div>

            </form>

        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 181.217px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Mobile Number
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Unit
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 154.8px;"
                                    aria-label="Engine version: activate to sort column ascending">Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($queue_success))
                                @foreach($queue_success as $message)
                                    @php
                                        $triggerOn = $message->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            @if(isset($message_send))
                                @foreach($message_send as $message)
                                    @php
                                        $triggerOn = $message->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">SL</th>
                                <th rowspan="1" colspan="1">Mobile Number</th>
                                <th rowspan="1" colspan="1">Unit</th>
                                <th rowspan="1" colspan="1">Date</th>
                                <th rowspan="1" colspan="1">Time</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            var start = moment().subtract(29, 'days');
            var end = moment();
            $('#reservation').daterangepicker({
                showDropdowns: true,
                alwaysShowCalendars:true,
                locale: {
                    format: 'DD/MM/YYYY'
                },
                linkedCalendars: true,
                cancelButtonClasses: 'btn btn-danger',
                "autoApply": true,
                singleDatePicker: true
            });
        });
    </script>

@endsection
