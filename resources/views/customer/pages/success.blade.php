@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>SMS Delivery Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('customer.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-12 table table-responsive">

                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Message list for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-12">
                <div class="box-header with-border">

                    <h3 class="box-title">Message Delivery Information </h3>
                </div>
                <div class="col-sm-12 table table-responsive">
                    <table id="example1" class="table table-bordered table-striped dataTable table-hover" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 20px;" aria-sort="ascending"
                                aria-label="Rendering engine: activate to sort column descending">SL
                            </th>
                            <th>Message Id</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 50px;" aria-label="Platform(s): activate to sort column ascending">
                                Mobile Number
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                Message
                            </th>
                            <th>Sender</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 20px;" aria-label="Platform(s): activate to sort column ascending">
                                Unit
                            </th>
                            <th>Status</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 50px;"
                                aria-label="Engine version: activate to sort column ascending">Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                style="width: 40px;" aria-label="CSS grade: activate to sort column ascending">
                                Time
                            </th>
                            <th>Delivery Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($messageList))
                            @php
                                $i =1;
                            $status = [
                                0=>'Queue',
                                1=>'Pending',
                                2=>'Fail',
                                3=>'Success',
                            ];
                               $deliver_time=false;
                            @endphp
                            @foreach($messageList as $message)
                                @php
                                    if($message->queue){
                                        $msg = $message->queue;
                                    }else{
                                        $msg = $message->send;
                                    }
                                    $triggerOn = $message->created_at;
                                    $schedule_date = new DateTime($triggerOn);
                                    if ($message->delivery_at):
                                    $deliver_time = new DateTime(date('h:i:s A d-m-Y',$message->delivery_at));
                                    $deliver_time->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                    $deliver_time = $deliver_time->format('Y-m-d H:i:s');
                                    else:
                                    $deliver_time =false;
                                    endif;
                                    $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                    $timestamp =  $schedule_date->format('Y-m-d H:i:s');

                                @endphp
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>
                                        @php
                                            $guid =\App\CallBack::where(['message_id'=>$msg->id])->select
                                            ('message_guid')
                                            ->first();
                                        @endphp
                                        @if($guid)
                                            {{$guid->message_guid}}
                                        @else
                                            {{$msg->send_id}}
                                        @endif


                                    </td>
                                    <td>{{$msg->phone_number}}</td>
                                    <td>{{$message->incoming_msg}}</td>
                                    <td>@if($msg->route){{$msg->route->route_name}}@endif</td>
                                    <td>{{$msg->msg_count}}</td>
                                    <td>{{$status[$msg->status]}}</td>
                                    <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                    <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    <td>
                                        @if($deliver_time)
                                            {{date('h:i A',strtotime($deliver_time))}}
                                        @endif
                                    </td>
                                </tr>
                                @php
                                    @endphp
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">SL</th>
                            <th rowspan="1" colspan="1">Message Id</th>
                            <th rowspan="1" colspan="1">Mobile Number</th>
                            <th rowspan="1" colspan="1">Message</th>
                            <th rowspan="1" colspan="1">Sender</th>
                            <th rowspan="1" colspan="1">Unit</th>
                            <th rowspan="1" colspan="1">Status</th>
                            <th rowspan="1" colspan="1">Date</th>
                            <th rowspan="1" colspan="1">Time</th>
                            <th rowspan="1" colspan="1">Delivery Time</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
