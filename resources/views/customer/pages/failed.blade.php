@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>SMS Failed Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-12 table table-responsive">

                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Message failed list for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Total Failed </th>
                        <th scope="row">
                            @if(isset($msg_list)&&isset($failed_msg))
                                {{$msg_list->sum('msg_count')+$failed_msg->sum('msg_count')}}
                            @else
                                Failed to get info
                            @endif
                        </th>
                    </tr>

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-12">
                <div class="box-header with-border">

                    <h3 class="box-title">Message Pending Information  </h3>
                </div>
                <div class="col-md-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Phone Number</th>
                                <th>Unit</th>
                                <th>Error Message</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($msg_list))
                                @php
                                    $i =1;
                                @endphp
                                @foreach($msg_list as $msg)
                                    @php
                                        $triggerOn = $msg->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$msg->phone_number}}</td>
                                        <td>{{$msg->error_message->message}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            @if(isset($failed_msg))
                                @foreach($failed_msg as $msg)
                                    @php
                                        $triggerOn = $msg->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    @if($msg->error_message)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$msg->phone_number}}</td>
                                        <td>{{$msg->msg_count}}</td>
                                        <td>{{$msg->error_message->message}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                    @endif
                                @endforeach

                            @endif
                            </tbody>
                        </table>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
@endsection
