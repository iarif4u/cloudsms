@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>Balance History Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">

                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Balance history view for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Current capacity </th>
                        <td> {{$user_capacity->user_capacity}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Total SMS send</th>
                        <td>@if(isset($message_send)){{$message_send->sum('msg_count')+$queue_msg->sum('msg_count')}}
                            @else 0
                            @endif</td>

                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <div class="box-header with-border">
                    <h3 class="box-title">SMS details by prefix</h3>
                </div>
                <div class="table table-responsive">
                <table class="table table-condensed table-hover">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Prefix</th>
                        <th>Progress</th>
                        <th style="width: 40px">Percent</th>
                        <th style="width: 40px">Total</th>
                    </tr>
                    @if(isset($total_prefix))
                        @php
                            $i=1;
                        @endphp
                        @foreach($total_prefix as $prefix => $value)
                            @php
                                if (array_sum($total_prefix)!=0){
                                    $percent = floor($value*100/array_sum($total_prefix));
                                }else{
                                    $percent=0;
                                }

                            @endphp
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$prefix}}</td>
                                <td>
                                    <div class="progress progress-xs active progress-striped">
                                        <div class="progress-bar
                                        @if($percent>70)
                                                progress-bar-success
                                        @elseif($percent>=50)
                                                progress-bar-primary
                                        @elseif($percent>=30)
                                                progress-bar-yellow
                                        @else
                                                progress-bar-danger
                                        @endif
                                                " style="width: {{$percent}}%"></div>
                                    </div>
                                </td>
                                <td>
                                    <span class="badge
                                    @if($percent>70)
                                            bg-green
                                    @elseif($percent>=50)
                                            bg-light-blue
                                    @elseif($percent>=30)
                                            bg-yellow
                                    @else
                                            bg-red
                                    @endif
                                            ">{{$percent}}%</span>
                                </td>
                                <td>{{$value}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-12">
                <div class="box-header with-border">

                    <h3 class="box-title">Capacity Information  </h3>
                </div>
                <div>
                    <div class="col-md-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover" role="grid"
                               aria-describedby="example1_info">
                           <thead>
                            <tr>
                                <th>SL</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Capacity</th>
                            </tr>
                           </thead>
                            <tbody>
                            @if(isset($capacity_list))
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($capacity_list as $capacity)
									@php
                                        $triggerOn = $capacity->created_at;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{auth()->user()->name}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                        <td>{{$capacity->capacity}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
@endsection
