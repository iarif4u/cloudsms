@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>Prefix Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-4">
                <div class="table table-responsive">
                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">Daily report view for</th>
                            <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Current capacity</th>
                            <td> {{$user_capacity->user_capacity}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Total SMS send</th>
                            <td>@if(isset($total_prefix)){{array_sum($total_prefix)}} @else 0 @endif</td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-2">
                <h4>Select date range : </h4>
            </div>
            <form class="form-inline" action="{{route('customer.report.prefix_report')}}" method="post">
                {{csrf_field()}}
                <div class="col-md-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input class="form-control pull-right" id="reservation" name="daterange" type="text">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="input-group">
                        @if($user_capacity->prefix_list->count())
                            <select class="form-control select-prefix" name="prefix" id="prefix">
                                <option value="">Select Prefix</option>
                                @foreach($user_capacity->prefix_list as $prefix)
                                    <option value="{{$prefix->prefix_info->id}}">{{$prefix->prefix_info->label}} ({{$prefix->prefix_info->prefix}})</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
                <div class="col-md-1">
                    <input type="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 50px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 150px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">Message ID
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Mobile Number
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 50px;" aria-label="Platform(s): activate to sort column ascending">
                                    Unit
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 154.8px;"
                                    aria-label="Engine version: activate to sort column ascending">Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($queue_success))
                                @foreach($queue_success as $message)
                                    @php
                                        $triggerOn = $message->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->send_id}}</td>
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>@if($message->status==3) Success @elseif($message->status==2) Fail
                                            @elseif($message->status==1) Processing
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">SL</th>
                                <th rowspan="1" colspan="1">Message ID</th>
                                <th rowspan="1" colspan="1">Mobile Number</th>
                                <th rowspan="1" colspan="1">Unit</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">Date</th>
                                <th rowspan="1" colspan="1">Time</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <script>
        $(".select-prefix").select2();
        var start = moment().subtract(29, 'days');
        var end = moment();
        //Date range picker
        $('#reservation').daterangepicker({
            showDropdowns: true,
            alwaysShowCalendars: true,
            startDate: start,
            endDate: end,
            linkedCalendars: true,
            cancelButtonClasses: 'btn btn-danger',
            locale: {
                format: 'DD/MM/YYYY'
            },

            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        @if(isset($daterange))
        $('#reservation').val('{{$daterange}}');
        @endif
    </script>
@endsection
