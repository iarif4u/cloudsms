@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>Monthly Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">

                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Daily report view for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Current capacity </th>
                        <td> {{$user_capacity->user_capacity}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Total SMS send</th>
                        <td> @if(isset($total_send)) {{$total_send}} @endif</td>

                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="col-md-2">
                <h4>Select date range : </h4>
            </div>
            <form action="{{route('report.monthly_report')}}" method="post">
                {{csrf_field()}}
            <div class="col-md-3">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control pull-right" id="reservation" name="daterange" type="text">
                </div>
            </div>
            <div class="col-md-1">
                <input type="submit" value="Submit" class="btn btn-primary">
            </div>
            </form>

        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-6">
                <div class="box-header with-border">

                    <h3 class="box-title">Capacity Information  </h3>
                </div>
                <div id="table-wrapper">
                    <div id="table-scroll" class="table-responsive table">
                        <table class="table table-hover table-responsive">
                            <tbody>
                            <tr>
                                <th>User</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Capacity</th>
                            </tr>
                            @if(isset($capacity_list))
                                @foreach($capacity_list as $capacity)
                                    <tr>
                                        <td>{{auth()->user()->name}}</td>
                                        <td>{{date('d-m-Y',strtotime($capacity->date))}}</td>
                                        <td>{{date('h:i A',strtotime($capacity->date))}}</td>
                                        <td>{{$capacity->capacity}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box-header with-border">
                    <h3 class="box-title">SMS details by prefix</h3>
                </div>
                <div class="table table-responsive">
                <table class="table table-condensed table-hover">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Prefix</th>
                        <th>Progress</th>
                        <th style="width: 40px">Percent</th>
                        <th style="width: 40px">Total</th>
                    </tr>
                    @if(isset($total_prefix))
                        @php
                            $i=1;
                        @endphp
                        @foreach($total_prefix as $prefix => $value)
                            @php
                                if (array_sum($total_prefix)!=0){
                                    $percent = floor($value*100/array_sum($total_prefix));
                                }else{
                                    $percent=0;
                                }

                            @endphp
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$prefix}}</td>
                                <td>
                                    <div class="progress progress-xs active progress-striped">
                                        <div class="progress-bar
                                        @if($percent>70)
                                                progress-bar-success
                                        @elseif($percent>=50)
                                                progress-bar-primary
                                        @elseif($percent>=30)
                                                progress-bar-yellow
                                        @else
                                                progress-bar-danger
                                        @endif
                                                " style="width: {{$percent}}%"></div>
                                    </div>
                                </td>
                                <td>
                                    <span class="badge
                                    @if($percent>70)
                                            bg-green
                                    @elseif($percent>=50)
                                            bg-light-blue
                                    @elseif($percent>=30)
                                            bg-yellow
                                    @else
                                            bg-red
                                    @endif
                                            ">{{$percent}}%</span>
                                </td>
                                <td>{{$value}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table-responsive table">
                        <table id="example1" class="table table-bordered table-striped dataTable table-hover" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 20px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Message ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Mobile Number
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 50px;" aria-label="Platform(s): activate to sort column ascending">
                                    Unit
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 154.8px;"
                                    aria-label="Engine version: activate to sort column ascending">Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($queue_success))
                                @foreach($queue_success as $message)
                                    @php
                                        $triggerOn = $message->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->send_id}}</td>
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            @if(isset($message_send))
                                @foreach($message_send as $message)
                                    @php
                                        $triggerOn = $message->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>
                                        <td>{{$message->send_id}}</td>
                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>@if($message->status==3) Success @elseif($message->status==2) Fail
                                            @elseif($message->status==1) Processing
                                                @else
                                                Pending
                                            @endif</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">SL</th>
                                <th rowspan="1" colspan="1">Message ID</th>
                                <th rowspan="1" colspan="1">Mobile Number</th>
                                <th rowspan="1" colspan="1">Unit</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">Date</th>
                                <th rowspan="1" colspan="1">Time</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            var start = moment().subtract(29, 'days');
            var end = moment();
            $('#reservation').daterangepicker({
                showDropdowns: true,
                alwaysShowCalendars:true,
                startDate: start,
                endDate: end,
                linkedCalendars: true,
                cancelButtonClasses: 'btn btn-danger',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
        });

        $('#reservation').on('apply.daterangepicker', function(ev, picker) {
           $("form").submit();
        });
    </script>

@endsection
