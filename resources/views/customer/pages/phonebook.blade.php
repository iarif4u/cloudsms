<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 9/16/2018
 * Time: 3:43 PM
 */
?>
@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Home
    <small>Address Books</small>
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Address Books</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-12 table table-responsive">
                <a href="javascript:void(0);" class="btn btn-primary new-phone-book">New Address Book</a>
                <br>
                <br>
                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Address Book list for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Total Address Books</td>
                        <td> {{$phonebooks->count()}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-12">
                <div class="box-header with-border">

                    <h3 class="box-title">Phone Book Information </h3>
                </div>
                <div class="col-md-12 table table-responsive">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Address Book Name</th>
                            <th>Description</th>
                            <th>Total Contact</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($phonebooks))
                            @php
                                $i =1;
                            @endphp
                            @foreach($phonebooks as $book)
                                @php
                                    $triggerOn = $book->created_at;
                                    $schedule_date = new DateTime($triggerOn);
                                    $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                    $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                @endphp
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td class="book-name">{{$book->name}}</td>
                                    <td class="book-description">{{$book->description}}</td>
                                    <td>@if($book->contact){{$book->contact->count()}} @else 0 @endif</td>
                                    <td>{{date('d-m-Y h:i A',strtotime($timestamp))}}</td>
                                    <td>
                                        <div class="col-md-1">
                                            <a data-toggle="tooltip" title="Excel file upload"
                                               onclick="open_file_upload_modal('{{$book->id}}',this)"
                                               href="javascript:void(0);" class="btn btn-primary btn-xs"> <i
                                                    class='fa fa-file-excel-o'></i>
                                            </a>
                                        </div>
                                        <div class="col-md-1">
                                            <a data-toggle="tooltip" title="Add Contact ot Address Book"
                                               onclick="add_contact_to_phonebook('{{$book->id}}',this)"
                                               href="javascript:void(0);" class="btn btn-success btn-xs"> <i
                                                    class='fa
                                                        fa-plus-square'></i>
                                            </a>
                                        </div>
                                        <div class="col-md-1">
                                            <a class="btn btn-xs btn-primary" data-toggle="tooltip"
                                               title="Show Contact List"
                                               href="javascript:void(0);"
                                               onclick="get_contact_list('{{$book->id}}',
                                                   '{{$book->name}}')">
                                                <i class="fa fa-th-list"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-1">
                                            <a data-toggle="tooltip" title="Update Address Book"
                                               class="btn btn-success btn-xs"
                                               href="javascript:void(0);"
                                               onclick="update_phone_book('{{$book->id}}',
                                                   '{{$book->name}}','{{$book->description}}')">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-1">
                                            <a data-toggle="tooltip" title="Delete Address Book"
                                               class="btn btn-danger btn-xs"
                                               href="javascript:void(0);"
                                               onclick="delete_phone_book
                                                   ('{{$book->id}}')">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-1">
                                            <a data-toggle="tooltip" title="Download Address Book" class="btn
                                            btn-success btn-xs"
                                               href="javascript:void(0);"
                                               onclick="download_phone_book
                                                   ('{{$book->id}}')">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>

    {{--Contact excel file modal start--}}
    <div class="modal fade" id="modal-excel-uploader">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Contact List by Excel/CSV File</h4>
                    <b class="text-yellow"> Phone Book : <span class="text-white phone-book-name"> Phone Book Name
                        </span></b>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.upload_excel','autocomplete'=>'off','files'=>true]) !!}
                {!! Form::hidden('phone_book_id', null,['id'=>'book_id_file']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">

                                <table class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <td>
                                            {!! Form::file('file',['class'=>'form-control-file']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="text text-primary text-left">Contact name column name must be
                                                <b>Name</b></p>
                                            <p class="text text-primary text-left">Contact number column name must be
                                                <b>Number</b></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="text text-danger text-left">Excel and CSV file only allow</p>
                                        </td>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary pull-right']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Contact excel file modal end--}}

    {{--Contact modal start--}}
    <div class="modal fade" id="modal-new-contact">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Contact List</h4>
                    <b class="text-yellow"> Phone Book : <span class="phone-book-name"> Phone Book Name
                        </span></b>
                </div>
                <!-- form start -->
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">
                                {!! Form::open(['route' => 'customer.contact','autocomplete'=>'off'])
                                 !!}
                                {!! Form::hidden('phone_book_id', null,['id'=>'phone_book_id']) !!}
                                <table class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th style="width: 40%">Name</th>
                                        <th style="width: 50%">Number</th>
                                        <th style="width: 10%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody class="tr-prefix-row">

                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12">
                                <div class="col-md-1 row-add">

                                    <a href="javascript:void(0);" class="btn btn-success tr-add">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                                <div class="col-md-1 col-md-offset-9">
                                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Contact modal end--}}
    {{--phone book modal start--}}
    <div class="modal fade" id="modal-phone-book">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer new address book</h4>
                    <b class="text-yellow"> Customer Name : <span class="text-white customer-name"> {{auth()->user()->name}}
                        </span></b>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.phonebooks','autocomplete'=>'off']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="row">
                                <div class="col-md-3">
                                    {!! Form::label('name', 'Address Book Name') !!}
                                </div>
                                <div class="col-md-8">
                                    {!! Form::text('name', null,['placeholder'=>"Enter Address Book Name",'class'=>'form-control']) !!}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    {!! Form::label('description', 'Description') !!}
                                </div>
                                <div class="col-md-8">
                                    {!! Form::textarea('description', null,['cols'=>5, 'rows'=>3,
                                    'class'=>'form-control' ,
                                    'placeholder'=>"Enter Phonebook  Description"]) !!}
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- phone book modal end--}}
    {{--phone book update modal start--}}
    <div class="modal update fade" id="modal-phone-book-update">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer update address book</h4>
                    <b class="text-yellow"> Customer Name : <span class="text-white customer-name"> {{auth()->user()->name}}
                        </span></b>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.phonebooks.update','autocomplete'=>'off']) !!}
                {!! Form::hidden('book_id', null,['id'=>'phone_book_update_id']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="row">
                                <div class="col-md-3">
                                    {!! Form::label('name', 'Address Book Name') !!}
                                </div>
                                <div class="col-md-8">
                                    {!! Form::text('name', null,['placeholder'=>"Enter Address Book Name",
                                    'class'=>'form-control book-name-update']) !!}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    {!! Form::label('description', 'Description') !!}
                                </div>
                                <div class="col-md-8">
                                    {!! Form::textarea('description', null,['cols'=>5, 'rows'=>3,
                                    'class'=>'form-control book-description-update' ,
                                    'placeholder'=>"Enter Address Book  Description"]) !!}
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- phone book modal end--}}
    {{--contact list streaming modal start--}}
    <div class="modal fade" id="modal-contact-list">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Contact list</h4>
                </div>
                <div class="modal-body">


                    <!-- Start History Table -->
                    <div class="box">
                        <b class="text-yellow"> Address Book Name : <span class="text-green book-name-modal"> Book Name
                            </span></b>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="Table_id"
                                   class="table table-responsive-md table-bordered table-hover" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Number</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.End History Table -->

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    {{--contact list streaming modal end--}}
    {{--delete contact modal start--}}
    <div class="modal fade" id="modal-delete-contact">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Contact</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.delete_contact','autocomplete'=>'off']) !!}
                {!! Form::hidden('contact_id', null,['id'=>'deleteContactId']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this contact???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete contact modal end--}}
    {{--delete contact modal start--}}
    <div class="modal fade" id="modal-delete-book">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Address Book</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.phonebooks.delete','autocomplete'=>'off']) !!}
                {!! Form::hidden('book_id', null,['id'=>'deleteBookId']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            Are you sure to delete this address book???
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}

            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--delete contact modal end--}}
    {{--download contact modal start--}}
    <div class="modal fade" id="modal-download-book">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Download Address Book</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.phonebooks.download','autocomplete'=>'off','id'=>'download-excel'])
                 !!}
                {!! Form::hidden('book_id', null,['id'=>'downloadBookId']) !!}
                {!! Form::hidden('format_type', null,['id'=>'format_type']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-2">
                                <a onclick="set_download_format('csv')" href="javascript:void(0);" class="btn
                                btn-success">
                                    CSV
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a onclick="set_download_format('xls')" href="javascript:void(0);" class="btn
                                btn-primary">
                                    xls
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a onclick="set_download_format('xlsx')" href="javascript:void(0);" class="btn
                                btn-success">
                                    xlsx
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a onclick="set_download_format('xlt')" href="javascript:void(0);" class="btn
                                btn-primary">
                                    xlt
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a onclick="set_download_format('txt')" href="javascript:void(0);" class="btn
                                btn-success">
                                    Text
                                </a>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>'modal']) !!}
                </div>
            {!! Form::close() !!}

            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--download contact modal end--}}
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        })
        //set excel file download format and send download request
        function set_download_format(format) {
            $("#modal-download-book").modal('toggle');
            $("#format_type").val(format);
            $("#download-excel").submit();
        }

        //excel file download modal open
        function download_phone_book(book_id) {
            $("#downloadBookId").val(book_id);
            $("#modal-download-book").modal('toggle');
        }

        $(".new-phone-book").click(function () {
            $("#modal-phone-book").modal('toggle');

        });

        //update phone book
        function update_phone_book(id, name, description) {
            $(".book-name-update").val(name);
            $("#phone_book_update_id").val(id);
            $(".book-description-update").val(description);
            $("#modal-phone-book-update").modal('toggle');
        }

        //open excel file uploader modal
        function open_file_upload_modal(book_id, context) {
            $("#book_id_file").val(book_id);
            $("#modal-excel-uploader").modal('toggle');
        }

        //add contact list to phone book
        function add_contact_to_phonebook(phone_book_id, context) {
            var description = $(context).parent().parent().parent().find('.book-description').text();
            var name = $(context).parent().parent().parent().find('.book-name').text();
            $(".phone-book-name").text(name);
            $("#phone_book_id").val(phone_book_id);
            $("#modal-new-contact").modal('toggle');
        }

        //add new row to create new contact list row
        //route prefix row add
        $('.row-add').on('click', '.tr-add', function () {
            var tr_row = '  <tr>\n' +
                '<td>\n' +
                '<input class="form-control prefix-form" type="text" name="contact_name[]">\n' +
                '</td>\n' +
                '<td>\n' +
                '<input class="form-control prefix-form" type="text" name="number[]">\n' +
                '</td>\n' +
                '<td>\n' +
                '<button class="btn btn-danger btn-xs tr-remove">\n' +
                '<i class="fa fa-remove"></i>\n' +
                '</button>\n' +
                '</td>\n' +
                '</tr>';
            $(".tr-prefix-row").append(tr_row);
        });

        //remove parent tr row
        $('.tr-prefix-row').on('click', '.tr-remove', function () {
            $(this).parent().parent().remove();
        });

        //get the contact list of phone boook by phonebook id
        function get_contact_list(phone_book_id, contact_name) {
            $(".book-name-modal").text(contact_name);
            $("#modal-contact-list").modal('toggle');
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing': true,
                'ajax': {
                    url: '{{route('customer.get_contact_list')}}',
                    data: {'_token': '{{csrf_token()}}', 'phone_book_id': phone_book_id},
                    type: "POST",
                },
            });
        }

        //update contact info
        function update_contact(contact_id, contact_name, contact_number) {
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '{{route('customer.update_contact')}}',
                data: {
                    '_token': '{{csrf_token()}}', 'contact_id': contact_id, 'contact_name': contact_name,
                    'contact_number': contact_number
                },
                success: function (data) {
                    $(".alert").hide();
                    var row = '<div class="alert alert-success">Contact update successfully done</div>';
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Contact update fail</span><br/>';
                    row += '</div>';
                    $(".status").html(row);
                }
            });
        }

        $(document).ready(function () {
            //confirm box for delete contact
            $('#Table_id').on('click', '.contact-drop-btn', function () {
                var contact_id = $(this).attr("value");
                if ($.isNumeric(contact_id)) {
                    $("#deleteContactId").val(contact_id)
                    $("#modal-delete-contact").modal('toggle');
                }
            });
            $('#Table_id').on('click', '.check-up-btn', function () {
                var contact_id = $(this).parent().parent().parent().find('.span-cnt-id').text();
                var new_name = $(this).parent().parent().parent().parent().find('.cnt-txt-name').val();
                var new_number = $(this).parent().parent().parent().parent().find('.cnt-txt-number').val();
                if ($.isNumeric(contact_id) && $.isNumeric(new_number)) {
                    update_contact(contact_id, new_name, new_number);
                    $(this).parent().parent().parent().parent().find('.cnt-name p').text(new_name);
                    $(this).parent().parent().parent().parent().find('.cnt-number p').text(new_number);
                    $(this).parent().parent().parent().parent().find('.hidden').removeClass('hidden');
                    $(this).parent().parent().parent().parent().find('.cnt-txt-name').remove();
                    $(this).parent().parent().parent().parent().find('.cnt-txt-number').remove();
                    $(this).remove();
                } else {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Input data invalid or not found</span><br/>';
                    row += '</div>';
                    $(".status").html(row);
                }

            });


            $('#Table_id').on('click', '.contact-update-btn', function () {
                var txt_name = '{!! Form::text("cnt_name", null,["class"=>"form-control cnt-txt-name"]) !!}';
                var txt_number = '{!! Form::text("cnt_number", null,["class"=>"form-control cnt-txt-number"]) !!}';
                var check_btn = '<button class="btn btn-success check-up-btn btn-xs"><i class="fa ' +
                    'fa-check"></i></button>';
                var contact_id = $(this).attr("value");
                var contact_name = $(this).parent().parent().parent().find('.cnt-name p').text();
                var contact_no = $(this).parent().parent().parent().find('.cnt-number p').text();
                if ($.isNumeric(contact_id)) {


                    $(this).parent().parent().parent().find('.cnt-number p').addClass('hidden');
                    $(this).parent().parent().parent().find('.cnt-name p').addClass('hidden');
                    $(this).parent().parent().parent().find('.cnt-name span').html(txt_name);
                    $(this).parent().parent().parent().find('.cnt-number span').html(txt_number);
                    $(this).parent().parent().parent().find('.cnt-txt-name').val(contact_name);
                    $(this).parent().parent().parent().find('.cnt-txt-number').val(contact_no);
                    $(this).parent().find('span').html(check_btn);
                    $(this).addClass('hidden');
                }
            });
        });

        //delete phone book
        function delete_phone_book(book_id) {
            //modal-delete-book
            $("#deleteBookId").val(book_id);
            $("#modal-delete-book").modal('toggle');
        }
    </script>
@endsection
@section('style')
    <style>
        #Table_id {
            width: 100% !important;
        }
    </style>
@endsection
