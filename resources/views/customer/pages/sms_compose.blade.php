@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    SMS
    <small>Compose SMS</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">
        <i class="fa fa-envelope-o"></i>
        SMS
    </li>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-10 sms-box col-md-offset-1">
                    <h3 class="text text-bold text-primary text-center compose-text">
                        Compose SMS
                        <i class="fa fa-envelope-o"></i>
                    </h3>

                    <p class="alert text-status  alert-danger hidden"></p>

                    <!-- form start -->
                    {!! Form::open(['route' => 'customer.sms.customer_sms','autocomplete'=>'off',
                    'class'=>'sms-compose']) !!}
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::label('sender_id', 'Sender ID:') !!}
                        </div>
                        <div class="col-md-8">
                            {!! Form::text('sender_id', null,['placeholder'=>"Enter The Sender ID",'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::label('recipients', 'Recipients:') !!}
                            <p>Seprate Numbers by Comma. (include country code for international destinations)</p>

                        </div>
                        <div class="col-md-8">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-padding btn-primary btn-sm">Type Number</button>
                                <button type="button" class="btn btn-padding btn-secondary btn-sm phone-book-popup">Use
                                    Phone
                                    Book
                                </button>
                                <button type="button" class="btn  btn-padding btn-success btn-sm contact-file">Upload
                                    File
                                </button>
                            </div>
                            <br>

                            {!! Form::textarea('recipients', null,['placeholder'=>"Enter Recipients Numbers",
                            'class'=>'form-control recipients','rows'=>5,"id"=>"mytextarea"]) !!}
                            <p class="text-danger recipients-empty hidden">
                                Recipients Can't be empty
                            </p>
                            <p class="recipients-info">
                                <span class="contacts">0</span> Total Recipients
                                <span class="text text-red hidden error-text"></span>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::label('message', 'Message:') !!}
                            @if (\Illuminate\Support\Facades\Route::currentRouteName() == 'customer.sms.unicode_sms')
                                <p>70 Characters = 1 SMS</p>
                            @else
                                <p>160 Characters = 1 SMS</p>
                            @endif
                        </div>
                        <div class="col-md-8">

                            {!! Form::textarea('message', null,['placeholder'=>"Type The Desired Message",
                            'class'=>'form-control text-message','rows'=>5]) !!}
                            <p class="text-danger msg-empty hidden">
                                Message Can't be empty
                            </p>
                            <p class="msg-info">
                                <span class="characters">0</span> Characters Used and <span
                                    class="message-count">0</span> Message Count
                            </p>
                            {{ Form::button('<i class="fa  fa-send-o"></i> Send', ['type' => 'submit', 'class' => 'btn
                            btn-primary'] )  }}
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- / form -->
                </div>
            </div>

        </div>
        <!-- /.box-header -->
    </div>
    {{--Address Book list Modal Start--}}
    <div class="modal fade" id="modal-address-book">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Select Phone Book</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.sms.get_contacts','autocomplete'=>'off','class'=>'phonebook_form'])
                 !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <table class="table table-responsive table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th class="text-center">
                                        Name
                                    </th>
                                    <th class="text-center">
                                        Description
                                    </th>
                                    <th>
                                        Total Contacts
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($phonebooks))
                                    @foreach($phonebooks as $phonebook)
                                        <tr>
                                            <td>
                                                {!! Form::checkbox('book_id[]', $phonebook->id) !!}

                                            </td>
                                            <td>
                                                {{$phonebook->name}}
                                            </td>
                                            <td>
                                                {{$phonebook->description}}
                                            </td>
                                            <td>
                                                @if($phonebook->contact){{$phonebook->contact->count()}}@else 0 @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Address Book list Modal End--}}

    {{--Address Book list Modal Start--}}
    <div class="modal fade" id="modal-contact-file">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Contact Book Upload</h4>
                </div>
                <!-- form start -->
                {!! Form::open(['route' => 'customer.sms.get_contacts_file','autocomplete'=>'off',
                'class'=>'phonebook_file_form','files'=>true])
                 !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="box-body text-center">
                            <div class="col-md-12">

                                <table class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <td>
                                            <input class="form-control-file" name="file" type="file">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="text text-primary text-left">Contact name column name must be
                                                <b>Name</b></p>
                                            <p class="text text-primary text-left">Contact number column name must be
                                                <b>Number</b></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="text text-danger text-left">Excel and CSV file only allow</p>
                                        </td>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Yes',['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            <!-- / form -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Address Book list Modal End--}}
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            calculate_recipients();
            calculate_sms_counter();
            $(".recipients").keyup(function () {
                calculate_recipients();
            });
            $(".text-message").keyup(function () {
                calculate_sms_counter();
            });

            //add red border when input empty
            $("textarea").blur(function () {
                if ($(this).val().length == 0) {
                    $(this).addClass('input-empty')
                }
            });

            //calculate sms by it's characters
            function calculate_sms_counter() {
                var sms = $(".text-message").val();
                $(".characters").text(sms.length);
                if (sms.length > 0) {
                    $(".msg-empty").addClass('hidden');
                    $(".text-message").removeClass('input-empty');
                    $(".msg-info").removeClass('hidden');
                }
                if (isUnicode(sms)) {
                    get_sms_count(sms);
                } else {
                    if (sms.length > 150) {
                        get_sms_count(sms);
                    } else {
                        $(".message-count").text(1);
                    }
                }
                if (sms.length == 0) {
                    $(".message-count").text(0);
                }
            }

            //calculate and verify recipients data
            function calculate_recipients() {
                var recipients = $(".recipients").val();

                $(".error-text").addClass('hidden');
                if (recipients.match(/[,]/) === null) {
                    if (!$.isNumeric(recipients)) {
                        $('.recipients').val('');
                        $(".contacts").text(0);
                    } else {
                        $(".contacts").text(1);
                        $(".recipients-empty").addClass('hidden');
                        $(".recipients-info").removeClass('hidden');
                        $(".recipients").removeClass('input-empty');
                    }
                } else {
                    var error_no = 0;
                    var numbers = recipients.split(',');
                    for (var i = 0; i < (numbers.length); i++) {
                        if (!$.isNumeric(numbers[i])) {
                            error_no++;
                            $(".error-text").removeClass('hidden');

                            $(".error-text").html('<span>' + error_no + '</span> recipients number is invalid');
                            if (error_no > 1) {
                                $(".error-text").text(error_no + ' recipients number are invalid');
                            }
                        }
                    }

                    $(".contacts").text(numbers.length - error_no);
                }
            }

            //get sms count by ajax request
            function get_sms_count(sms) {
                $.ajax({
                    type: "POST",
                    dataType: 'JSON',
                    url: '{{route('customer.sms.get_counter')}}',
                    data: {'_token': '{{csrf_token()}}', 'sms': sms},
                    success: function (data) {
                        $(".characters").text(data.characters);
                        $(".message-count").text(data.sms_count);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        var row = '<div class="alert alert-danger">';
                        row += '<span class="each-error">Status update failed</span><br/>';
                        row += '</div>';

                        $("#status").html(row);
                    }
                });
            }
        });

        $(".sms-compose").submit(function (event) {
            $(".input-empty").removeClass('input-empty');
            $(".recipients-empty").addClass('hidden');
            $(".msg-empty").addClass('hidden');
            var message_zero = "<p>Message can't be empty</p>";
            var recipients_zero = "<p>Recipients list can't be empty</p>";
            var recipients = $(".recipients").val();
            var message = $(".text-message").val();
            if (recipients.length == 0) {
                event.preventDefault();
                $(".recipients").addClass('input-empty');
                $(".recipients-info").addClass('hidden');
                $(".recipients-empty").removeClass('hidden');

            }
            if (message.length == 0) {
                event.preventDefault();
                $(".text-message").addClass('input-empty');
                $(".msg-empty").removeClass('hidden');
                $(".msg-info").addClass('hidden');
            }
        });

        //open phone book list
        $(".phone-book-popup").click(function () {
            $("#modal-address-book").modal('toggle');
        });

        //check sms unicode or not
        function isUnicode(str) {
            for (var i = 0, n = str.length; i < n; i++) {
                if (str.charCodeAt(i) > 255) {
                    return true;
                }
            }
            return false;
        }

        //open contact file upload modal
        $(".contact-file").click(function () {
            $("#modal-contact-file").modal('toggle');
        });
        //submit phone book form
        $(".phonebook_form").submit(function (event) {
            event.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function (data) {
                    $(".recipients").val(data);
                    $(".recipients").keyup();
                }
            });
            $("#modal-address-book").modal('toggle');
        });
        //file submit by ajax
        $(".phonebook_file_form").submit(function (e) {
            e.preventDefault();
            var numbers = $("#mytextarea").val();
            var formData = new FormData(this);
            var url = $(this).attr('action');
            formData.append(numbers, numbers);
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                success: function (data) {
                    if (data.error == false) {
                        $(".recipients").val(data.message);
                        $(".recipients").keyup();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
            $("#modal-contact-file").modal('toggle');
        });
    </script>
@endsection
