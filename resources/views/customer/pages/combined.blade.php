@extends('customer.layouts.master')

@section('title',"Cloud Message")
@section('style')
    <!-- Datatable style -->
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.css"/>
@endsection
@section('header_left')
    Report
    <small>Combined Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('customer.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-6">
                <div class="table table-responsive">
                    <table class="table table-hover borderless table-condensed">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">Combined report view for</th>
                            <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Current capacity</th>
                            <td> {{$user_capacity->user_capacity}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Total SMS send</th>
                            <td> @if(isset($send_collection)) {{$send_collection->sum('msg_count')+$queue_collection->sum('msg_count')}} @endif</td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <form autocomplete="off" action="{{route('customer.report.combined_report')}}" method="post">
                {{csrf_field()}}
                <div class="col-md-2">
                    <h4 style="padding-bottom: 10px">Select Date : </h4>

                    <div style="padding-bottom: 4px" class="margin-top-10"><h4> Select Prefix : </h4></div>
                    <div class="margin-top-10"><h4> Select Status : </h4></div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input class="form-control pull-right"
                               id="reservation"
                               name="daterange"
                               type="text">
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon invisible">
                            <i class="fa fa-search"></i>
                        </div>

                        {!! Form::select('prefix', $total_prefix, $select_prefix,['id'=>"prefix","class"=>"form-control margin-top-10"]); !!}

                    </div>
                    <div class="input-group">
                        <div class="input-group-addon invisible">
                            <i class="fa fa-search"></i>
                        </div>
                        <select class="form-control margin-top-10" name="status" id="status">
                            <option value="">Select All</option>
                            <option value="3">Success</option>
                            <option value="0">Pending</option>
                            <option value="2">Fail</option>
                        </select>
                    </div>
                    <div class="input-group text-center margin-top-10 pull-right">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>

                </div>

            </form>

        </div>

    </div>
    {{--Start data table--}}
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                <div class="row">
                    <div class="col-sm-12 table table-responsive">

                        <table id="example1" class="table table-bordered table-striped dataTable table-hover"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 20px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Mobile Number
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 50px;" aria-label="Platform(s): activate to sort column ascending">
                                    Unit
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 154.8px;"
                                    aria-label="Engine version: activate to sort column ascending">Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(isset($queue_collection))
                                @foreach($queue_collection as $message)
                                    @php
                                        $triggerOn = $message->created_at;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$i++}}</td>

                                        <td>{{$message->phone_number}}</td>
                                        <td>{{$message->msg_count}}</td>
                                        <td>@if($message->status==3) Success @elseif($message->status==2) Fail
                                            @elseif($message->status==0) Pending
                                            @else
                                                Success
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Mobile Number</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    {{--End data table--}}
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").dataTable();
    </script>
    <script type="text/javascript">
        $(function () {
            var start = moment().subtract(29, 'days');
            var end = moment();
            $('#reservation').daterangepicker({
                showDropdowns: true,
                alwaysShowCalendars: true,
                startDate: start,
                endDate: end,
                linkedCalendars: true,
                cancelButtonClasses: 'btn btn-danger',
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            @if(isset($prefix))
            $('#prefix option[value="{{$prefix}}"]').attr("selected", "selected");
            @endif
            @if(isset($status))
            $('#status option[value="{{$status}}"]').attr("selected", "selected");
            @endif
            @if(isset($daterange))
            $('#reservation').val('{{$daterange}}');
            @endif
        });
    </script>

@endsection
