@extends('customer.layouts.master')

@section('title',"Cloud Message")

@section('header_left')
    Report
    <small>SMS Pending Report</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
@endsection

@section('content')
    <div class="box">

        <div class="box-header">
            <div class="col-md-12 table table-responsive">

                <table class="table table-hover borderless table-condensed">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Message pendign list for</th>
                        <th scope="col">{{ucfirst(auth()->user()->name)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Total Pending </th>
                        <th scope="row">@if(isset($msg_list)){{$msg_list->sum('msg_count')}}@endif</th>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-12">
                <div class="box-header with-border">

                    <h3 class="box-title">Message Pending Information  </h3>
                </div>
                <div class="col-md-12 table table-responsive">
                        <table id="example1" class="table table-bordered table-striped table-hover dataTable" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 181.217px;" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">SL
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Mobile Number
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 197.4px;" aria-label="Platform(s): activate to sort column ascending">
                                    Unit
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 154.8px;"
                                    aria-label="Engine version: activate to sort column ascending">Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 110.05px;" aria-label="CSS grade: activate to sort column ascending">
                                    Time
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($msg_list))
                                @php
                                    $i =1;
                                @endphp
                                @foreach($msg_list as $msg)
                                    @php
                                        $triggerOn = $msg->date;
                                        $schedule_date = new DateTime($triggerOn);
                                        $schedule_date->setTimeZone(new DateTimeZone(auth()->user()->time_zone));
                                        $timestamp =  $schedule_date->format('Y-m-d H:i:s');
                                    @endphp
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$msg->phone_number}}</td>
                                        <td>{{$msg->msg_count}}</td>
                                        <td>{{date('d-m-Y',strtotime($timestamp))}}</td>
                                        <td>{{date('h:i A',strtotime($timestamp))}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
@endsection
