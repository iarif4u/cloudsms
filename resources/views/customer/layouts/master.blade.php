<?php
/**
 * User: Md. Arif
 * Date: 6/1/2018
 * Time: 4:58 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo.png')}}"/>
    @include('customer.include.header')
    <title>@yield('title')</title>
    @yield('style')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
{{--include the top menu bar--}}
@include('customer.include.top_sidebar')
{{--include the left side bar--}}
@include('customer.include.left_sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('header_left')
            </h1>
            <ol class="breadcrumb">
                @yield('header_right')
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span class="each-error">{{ $error }} </span><br/>
                    </div>
                @endforeach

            @endif
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('message') }}
                </div>
            @endif
        <!-- Small boxes (Stat box) -->
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{--include the footer--}}
    @include('customer.include.footer')
</div>
<!-- ./wrapper -->
{{--link the js plugin--}}
@include('customer.include.javascript_bar')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@yield('script')
<script>
    $(document).ready(function () {
        $('.time_zone').select2();
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>
