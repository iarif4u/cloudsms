<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 9/15/2018
 * Time: 5:17 PM
 */
?>
@extends('customer.layouts.master')

@section('title',"WinnerDevs Message")

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Profile</li>
@endsection

@section('header_left')
    Home
    <small>Profile</small>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-12">
                <h4>Profile Setting</h4>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                {!! Form::open(['route' => 'customer.profile','autocomplete'=>'off','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('name', auth()->user()->name,['placeholder'=>"Enter The Name",
                        'class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password',['class'=>'col-sm-2 control-label']) !!}

                    <div class="col-sm-8">
                        {!! Form::password('password', ['placeholder'=>"Enter The Password", 'class'=>'form-control']) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Confirm Password',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::password('password_confirmation', ['placeholder'=>"Confirm Password", 'class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .profile-active {
            background: #022b44;
            margin-left: 3px;
        }
    </style>
@endsection

