-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 05, 2019 at 10:55 AM
-- Server version: 5.7.26-0ubuntu0.19.04.1
-- PHP Version: 7.2.17-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mysms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Asia/Dhaka',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `time_zone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$j8yvXiAoSHzItegT102ywu8p2Mxxby.k7MY8ZQgscdx9SwGjgHZ7G', '01742589634', 'Asia/Dhaka', NULL, '2019-04-17 18:00:00', '2019-04-17 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `call_backs`
--

CREATE TABLE `call_backs` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_guid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateway` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connector` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) NOT NULL,
  `name` varchar(191) NOT NULL,
  `number` varchar(191) NOT NULL,
  `phonebook_id` int(10) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `number`, `phonebook_id`, `updated_at`, `created_at`) VALUES
(1, 'Own', '8801740963472', 1, '2019-04-21 06:55:32', '2019-04-21 06:55:32'),
(2, 'Hasan', '8801740963587', 1, '2019-04-21 06:56:13', '2019-04-21 06:56:13'),
(3, 'Hasib', '8801740963588', 1, '2019-04-21 06:56:13', '2019-04-21 06:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `time_zone` varchar(191) NOT NULL DEFAULT 'Asia/Dhaka',
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `time_zone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tanvir', 'Tanvir321@gmail.com', 'Asia/Dhaka', '$2y$10$X0.gk.97aTYVnbN4YjCpqe0dP0vMMG/nZBOULsbY04XjIyhsQUai2', NULL, '2019-04-21 04:37:59', '2019-04-21 04:40:02'),
(4, 'Hasan', 'Hasan123@gmail.com', 'Asia/Dhaka', '$2y$10$mEOis9OsTZMe.zPqGFH7Qe9OBuMq2hAyosioCLAcSd3/b9NkTlxYu', NULL, '2019-04-23 00:02:39', '2019-04-23 00:02:39'),
(15, 'Arif', 'arif123@gmail.com', 'Asia/Dhaka', '$2y$10$NnDxhq.oCzh6qsyLv.gxqOC.FC3OD647lAtL02svYUZitB33tfkXi', NULL, '2019-04-27 02:44:04', '2019-04-27 02:44:04'),
(16, 'SMS', 'mysms123@gmail.com', 'Asia/Dhaka', '$2y$10$8SQEbRpHcRUTaTaJdCmUr.JvkxZ72yZJqvM.YwBOuy5MExVXlDwga', NULL, '2019-04-27 02:45:22', '2019-04-27 02:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `deletes`
--

CREATE TABLE `deletes` (
  `id` int(10) UNSIGNED NOT NULL,
  `primary_value` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `others_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deletes`
--

INSERT INTO `deletes` (`id`, `primary_value`, `name`, `others_value`, `created_at`, `updated_at`) VALUES
(1, 5, 'route_api', '{\"route\":5,\"route_name\":\"My SMPP\",\"api\":null,\"method\":\"SMPP\",\"user\":\"arif\",\"password\":\"1234\",\"port\":2775,\"smpp_status\":1,\"route_capacity\":2000,\"alert\":500,\"tpm\":20,\"bar\":100,\"status\":1,\"updated_at\":\"2019-04-24 11:08:23\",\"created_at\":\"2019-04-24 04:39:27\"}', '2019-04-23 23:09:07', '2019-04-23 23:09:07'),
(2, 3, 'route_api', '{\"route\":3,\"route_name\":\"SMPP Server\",\"api\":null,\"server\":\"192.168.0.106\",\"method\":\"SMPP\",\"user\":\"arif\",\"password\":\"arif123\",\"port\":2779,\"smpp_status\":0,\"route_capacity\":20000,\"alert\":20,\"tpm\":15,\"bar\":10,\"status\":1,\"updated_at\":\"2019-04-24 10:00:21\",\"created_at\":\"2019-04-23 10:42:41\"}', '2019-04-24 04:05:24', '2019-04-24 04:05:24'),
(3, 4, 'route_api', '{\"route\":4,\"route_name\":\"New Route\",\"api\":\"http:\\/\\/localhost\\/sms\\/public\\/admin\\/route\",\"server\":null,\"method\":\"GET\",\"user\":null,\"password\":null,\"port\":null,\"smpp_status\":0,\"route_capacity\":5000,\"alert\":500,\"tpm\":15,\"bar\":100,\"status\":1,\"updated_at\":\"2019-04-23 12:34:15\",\"created_at\":\"2019-04-23 12:34:15\"}', '2019-04-24 04:05:33', '2019-04-24 04:05:33');

-- --------------------------------------------------------

--
-- Table structure for table `error_message`
--

CREATE TABLE `error_message` (
  `id` int(11) NOT NULL,
  `msg_id` int(11) NOT NULL,
  `send_id` varchar(20) NOT NULL,
  `message` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `message_replace_report`
--

CREATE TABLE `message_replace_report` (
  `id` int(20) NOT NULL,
  `msg_id` int(10) NOT NULL DEFAULT '0',
  `incoming_msg` mediumtext NOT NULL,
  `phone` varchar(15) NOT NULL,
  `user_id` int(20) NOT NULL,
  `route_id` varchar(191) NOT NULL,
  `delivery_at` varchar(100) DEFAULT NULL,
  `feedback` int(10) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message_replace_report`
--

INSERT INTO `message_replace_report` (`id`, `msg_id`, `incoming_msg`, `phone`, `user_id`, `route_id`, `delivery_at`, `feedback`, `updated_at`, `created_at`) VALUES
(1, 1, '[MESSAGE]', '8801740963472', 1, '0', NULL, 0, '2019-04-21 00:56:41', '2019-04-21 00:56:41'),
(2, 2, '[MESSAGE]', '8801740963472', 4, '0', NULL, 0, '2019-04-23 00:37:45', '2019-04-23 00:37:45'),
(3, 3, '[MESSAGE]', '8801740963472', 4, '0', NULL, 0, '2019-04-23 01:01:06', '2019-04-23 01:01:06'),
(4, 4, 'hello9', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:11:30', '2019-04-28 07:11:30'),
(5, 6, '15-20', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:13:22', '2019-04-28 07:13:22'),
(6, 7, '15-20', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:13:22', '2019-04-28 07:13:22'),
(7, 5, '15-20', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:13:21', '2019-04-28 07:13:21'),
(8, 8, '15-20', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:13:22', '2019-04-28 07:13:22'),
(9, 9, '15-20', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:13:22', '2019-04-28 07:13:22'),
(10, 10, '15-20', '880172222222', 16, '0', NULL, 0, '2019-04-28 07:13:22', '2019-04-28 07:13:22'),
(11, 11, 'Test Message', '01740963472', 16, '0', NULL, 0, '2019-04-30 04:04:09', '2019-04-30 04:04:09'),
(12, 12, 'Test Message', '01740963472', 16, '0', NULL, 0, '2019-04-30 04:04:09', '2019-04-30 04:04:09'),
(13, 13, 'Hello World', '8801921345016', 16, '0', NULL, 0, '2019-04-30 04:24:59', '2019-04-30 04:24:59'),
(14, 14, 'Test Message', '01740963472', 16, '0', NULL, 0, '2019-04-30 04:26:54', '2019-04-30 04:26:54'),
(15, 15, 'New Test Message', '01740963472', 16, '0', NULL, 0, '2019-04-30 04:26:54', '2019-04-30 04:26:54'),
(16, 16, 'New Test Message', '01740963472', 16, '0', NULL, 0, '2019-04-30 04:32:23', '2019-04-30 04:32:23'),
(17, 17, 'New Data Test Message', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:32:23', '2019-04-30 04:32:23'),
(18, 18, 'Data Test Message', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:34:19', '2019-04-30 04:34:19'),
(19, 19, 'Bangla', '8801921345016', 16, '0', NULL, 0, '2019-04-30 04:38:53', '2019-04-30 04:38:53'),
(20, 20, 'english', '01921345016', 16, '0', NULL, 0, '2019-04-30 04:38:53', '2019-04-30 04:38:53'),
(21, 21, 'Math', '01921345016', 16, '0', NULL, 0, '2019-04-30 04:40:52', '2019-04-30 04:40:52'),
(22, 22, 'Data Test Message', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:42:48', '2019-04-30 04:42:48'),
(23, 23, 'Data Test Message.....................', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:44:44', '2019-04-30 04:44:44'),
(24, 24, 'Physics', '01921345016', 16, '0', NULL, 0, '2019-04-30 04:45:10', '2019-04-30 04:45:10'),
(25, 25, 'Data Test Message', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:45:29', '2019-04-30 04:45:29'),
(26, 26, 'Data Test Message', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:45:34', '2019-04-30 04:45:34'),
(27, 27, 'Data Test Message.....................', '017777884968', 16, '0', NULL, 0, '2019-04-30 04:45:48', '2019-04-30 04:45:48'),
(28, 28, '!@', '8801921345016', 16, '0', NULL, 0, '2019-05-02 04:53:56', '2019-05-02 04:53:56'),
(29, 29, '!!@', '8801921345016', 16, '0', NULL, 0, '2019-05-02 04:54:45', '2019-05-02 04:54:45'),
(30, 30, '!!@', '8801921345016', 16, '0', NULL, 0, '2019-05-02 04:55:15', '2019-05-02 04:55:15'),
(31, 31, '02!!@#$%^&*()_++20', '8801921345016', 16, '0', NULL, 0, '2019-05-02 05:46:08', '2019-05-02 05:46:08'),
(32, 32, '!!@#$%^&*()_++', '8801921345016', 16, '0', NULL, 0, '2019-05-02 05:46:31', '2019-05-02 05:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `message_send`
--

CREATE TABLE `message_send` (
  `id` int(20) NOT NULL,
  `send_id` varchar(100) NOT NULL,
  `message` mediumtext NOT NULL,
  `phone_number` varchar(150) NOT NULL,
  `user_id` int(10) NOT NULL,
  `route_id` varchar(191) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `msg_count` int(10) NOT NULL DEFAULT '0',
  `msg_id` int(10) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `otp_outgoing_string`
--

CREATE TABLE `otp_outgoing_string` (
  `id` int(20) NOT NULL,
  `incoming_string_id` int(20) NOT NULL,
  `replace_string` varchar(250) NOT NULL,
  `counter` int(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `otp_string`
--

CREATE TABLE `otp_string` (
  `id` int(11) NOT NULL,
  `incoming_string` varchar(155) NOT NULL,
  `vendor_id` int(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `otp_vendors`
--

CREATE TABLE `otp_vendors` (
  `id` int(20) NOT NULL,
  `vendor_name` varchar(50) NOT NULL,
  `status` int(6) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `over_capacity`
--

CREATE TABLE `over_capacity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `phone` varchar(150) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `over_capacity`
--

INSERT INTO `over_capacity` (`id`, `user_id`, `message`, `phone`, `updated_at`, `created_at`) VALUES
(1, 16, 'Physics', '01921345016', '2019-04-30 04:45:48', '2019-04-30 04:45:48'),
(2, 16, 'Data Test Message', '017777884968', '2019-04-30 04:45:49', '2019-04-30 04:45:49'),
(3, 16, 'Data Test Message', '017777884968', '2019-04-30 04:45:49', '2019-04-30 04:45:49'),
(4, 16, 'Data Test Message.....................', '017777884968', '2019-04-30 04:46:14', '2019-04-30 04:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `phone_books`
--

CREATE TABLE `phone_books` (
  `id` int(101) NOT NULL,
  `name` varchar(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `description` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_books`
--

INSERT INTO `phone_books` (`id`, `name`, `user_id`, `description`, `updated_at`, `created_at`) VALUES
(1, 'New Phone Book', 1, 'New Phone Book Details', '2019-04-21 06:21:40', '2019-04-21 06:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `prefixes`
--

CREATE TABLE `prefixes` (
  `id` int(191) NOT NULL,
  `customer_id` int(191) NOT NULL,
  `prefix` varchar(191) NOT NULL,
  `rate` varchar(191) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prefixes`
--

INSERT INTO `prefixes` (`id`, `customer_id`, `prefix`, `rate`, `status`, `updated_at`, `created_at`) VALUES
(1, 1, '88017', '2', 1, '2019-04-18 01:24:52', '2019-04-18 01:24:52'),
(2, 1, '88018', '1.5', 1, '2019-04-21 06:13:31', '2019-04-18 01:25:13'),
(3, 4, '88017', '1.5', 1, '2019-04-23 01:01:00', '2019-04-23 00:37:26'),
(4, 16, '88017', '2', 1, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(5, 16, '88019', '3', 1, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(6, 16, '88018', '1', 1, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(7, 16, '88016', '2', 1, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(8, 16, '017', '1', 1, '2019-04-30 04:03:14', '2019-04-30 04:03:14'),
(9, 16, '016', '1', 1, '2019-04-30 04:03:14', '2019-04-30 04:03:14'),
(10, 16, '019', '2', 1, '2019-04-30 04:03:15', '2019-04-30 04:03:15');

-- --------------------------------------------------------

--
-- Table structure for table `prefix_histories`
--

CREATE TABLE `prefix_histories` (
  `id` int(191) NOT NULL,
  `customer_id` int(191) NOT NULL,
  `prefix` varchar(191) NOT NULL,
  `rate` varchar(191) NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prefix_histories`
--

INSERT INTO `prefix_histories` (`id`, `customer_id`, `prefix`, `rate`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 1, '88017', '2', 0, '2019-04-18 01:24:52', '2019-04-18 01:24:52'),
(2, 1, '88018', '1', 0, '2019-04-18 01:25:13', '2019-04-18 01:25:13'),
(3, 1, '88018', '1.5', 0, '2019-04-21 06:13:31', '2019-04-21 06:13:31'),
(4, 4, '88017', '1', 0, '2019-04-23 00:37:26', '2019-04-23 00:37:26'),
(5, 4, '88017', '1.5', 0, '2019-04-23 01:01:00', '2019-04-23 01:01:00'),
(6, 16, '88017', '2', 0, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(7, 16, '88019', '3', 0, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(8, 16, '88018', '1', 0, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(9, 16, '88016', '2', 0, '2019-04-28 07:11:06', '2019-04-28 07:11:06'),
(10, 16, '017', '1', 0, '2019-04-30 04:03:14', '2019-04-30 04:03:14'),
(11, 16, '016', '1', 0, '2019-04-30 04:03:14', '2019-04-30 04:03:14'),
(12, 16, '019', '2', 0, '2019-04-30 04:03:14', '2019-04-30 04:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `queue_message`
--

CREATE TABLE `queue_message` (
  `id` int(20) NOT NULL,
  `message` mediumtext NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `user_id` varchar(15) NOT NULL,
  `route_id` varchar(191) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `send_id` varchar(100) DEFAULT '0',
  `msg_count` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `queue_message`
--

INSERT INTO `queue_message` (`id`, `message`, `phone_number`, `user_id`, `route_id`, `status`, `send_id`, `msg_count`, `updated_at`, `created_at`) VALUES
(1, '[MESSAGE]', '8801740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(2, '[MESSAGE]', '8801740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(3, '[MESSAGE]', '8801740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(4, 'hello9', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(5, '15-20', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(6, '15-20', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(7, '15-20', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(8, '15-20', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(9, '15-20', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(10, '15-20', '880172222222', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(11, 'Test Message', '01740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:18', '2019-04-30 04:22:18'),
(12, 'Test Message', '01740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:22:19', '2019-04-30 04:22:19'),
(13, 'Hello World', '8801921345016', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:25:23', '2019-04-30 04:25:23'),
(14, 'Data Message', '01740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:27:53', '2019-04-30 04:27:53'),
(15, 'Data Message', '01740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:27:53', '2019-04-30 04:27:53'),
(16, 'New Test Message', '01740963472', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:32:53', '2019-04-30 04:32:53'),
(17, 'New Data Test Message', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:32:53', '2019-04-30 04:32:53'),
(18, 'Data Test Message', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:34:43', '2019-04-30 04:34:43'),
(19, 'Bangla', '8801921345016', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:39:43', '2019-04-30 04:39:43'),
(20, 'english', '01921345016', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:39:43', '2019-04-30 04:39:43'),
(21, 'Math', '01921345016', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:41:11', '2019-04-30 04:41:11'),
(22, 'Data Test Message.....................', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:44:29', '2019-04-30 04:44:29'),
(23, 'Data Test Message.....................', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:45:48', '2019-04-30 04:45:48'),
(24, 'Physics', '01921345016', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:45:48', '2019-04-30 04:45:48'),
(25, 'Data Test Message', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:45:48', '2019-04-30 04:45:48'),
(26, 'Data Test Message', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:45:49', '2019-04-30 04:45:49'),
(27, 'Data Test Message.....................', '017777884968', '16', '13', 3, 'FFFFFF', 1, '2019-04-30 04:46:13', '2019-04-30 04:46:13'),
(28, '!@', '8801921345016', '16', '0', 0, '0', 1, '2019-05-02 04:53:56', '2019-05-02 04:53:56'),
(29, '!!@', '8801921345016', '16', '0', 0, '0', 1, '2019-05-02 04:54:45', '2019-05-02 04:54:45'),
(30, '!!@', '8801921345016', '16', '0', 0, '0', 1, '2019-05-02 04:55:15', '2019-05-02 04:55:15'),
(31, '02!!@#$%^&*()_++20', '8801921345016', '16', '0', 0, '0', 1, '2019-05-02 05:46:08', '2019-05-02 05:46:08'),
(32, '!!@#$%^&*()_++', '8801921345016', '16', '0', 0, '0', 1, '2019-05-02 05:46:31', '2019-05-02 05:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `route_api`
--

CREATE TABLE `route_api` (
  `route` int(11) NOT NULL,
  `route_name` varchar(155) NOT NULL,
  `api` varchar(255) DEFAULT NULL,
  `server` varchar(191) DEFAULT NULL,
  `method` varchar(191) NOT NULL,
  `user` varchar(191) DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  `port` int(20) DEFAULT NULL,
  `smpp_status` tinyint(1) NOT NULL DEFAULT '0',
  `route_capacity` int(55) NOT NULL,
  `alert` int(11) NOT NULL,
  `tpm` int(11) NOT NULL DEFAULT '20',
  `bar` int(20) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route_api`
--

INSERT INTO `route_api` (`route`, `route_name`, `api`, `server`, `method`, `user`, `password`, `port`, `smpp_status`, `route_capacity`, `alert`, `tpm`, `bar`, `status`, `updated_at`, `created_at`) VALUES
(1, 'My Server', NULL, '192.168.0.106', 'SMPP', 'admin', 'admin123', 2775, 0, 5000, 100, 50, 50, 1, '2019-04-26 20:59:08', '2019-04-23 04:08:38'),
(2, 'My Http', 'http://localhost/sms/public/admin/route', NULL, 'GET', NULL, NULL, NULL, 0, 2000, 200, 15, 150, 1, '2019-04-23 04:30:58', '2019-04-23 04:19:29'),
(13, 'New SMPP', NULL, '192.168.0.107', 'SMPP', 'arif', '1234', 2775, 1, 949, 100, 15, 50, 1, '2019-04-30 04:46:13', '2019-04-23 23:17:08'),
(14, 'Own Server', NULL, '192.168.0.103', 'SMPP', 'arif', '1234', 2774, 0, 10000, 10, 30, 10, 1, '2019-04-27 00:14:56', '2019-04-24 04:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `route_auth`
--

CREATE TABLE `route_auth` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_auth`
--

INSERT INTO `route_auth` (`id`, `username`, `password`, `route_id`, `created_at`, `updated_at`) VALUES
(2, 'authuser', 'authpassword', 4, '2019-04-23 06:34:15', '2019-04-23 06:34:15');

-- --------------------------------------------------------

--
-- Table structure for table `route_capacity_history`
--

CREATE TABLE `route_capacity_history` (
  `id` int(11) NOT NULL,
  `route_id` varchar(11) NOT NULL,
  `route_capacity` varchar(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route_capacity_history`
--

INSERT INTO `route_capacity_history` (`id`, `route_id`, `route_capacity`, `updated_at`, `created_at`) VALUES
(1, '1', '5000', '2019-04-23 04:08:38', '2019-04-23 04:08:38'),
(2, '2', '2000', '2019-04-23 04:19:29', '2019-04-23 04:19:29'),
(3, '3', '20000', '2019-04-23 04:42:41', '2019-04-23 04:42:41'),
(4, '4', '5000', '2019-04-23 06:34:15', '2019-04-23 06:34:15'),
(5, '5', '2000', '2019-04-23 22:39:27', '2019-04-23 22:39:27'),
(6, '13', '1000', '2019-04-23 23:17:08', '2019-04-23 23:17:08'),
(7, '14', '10000', '2019-04-24 04:35:26', '2019-04-24 04:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `route_prefixes`
--

CREATE TABLE `route_prefixes` (
  `id` int(191) NOT NULL,
  `route_id` int(191) NOT NULL,
  `prefix` varchar(191) NOT NULL,
  `rate` varchar(191) NOT NULL,
  `status` varchar(10) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route_prefixes`
--

INSERT INTO `route_prefixes` (`id`, `route_id`, `prefix`, `rate`, `status`, `updated_at`, `created_at`) VALUES
(1, 13, '017', '2', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(2, 13, '016', '1', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(3, 13, '019', '1', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(4, 13, '88017', '2', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(5, 13, '88015', '3', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(6, 13, '88016', '1', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(7, 13, '88019', '2', '1', '2019-04-30 04:22:06', '2019-04-30 04:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `route_prefix_histories`
--

CREATE TABLE `route_prefix_histories` (
  `id` int(191) NOT NULL,
  `route_id` int(191) NOT NULL,
  `prefix` varchar(191) NOT NULL,
  `rate` varchar(191) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route_prefix_histories`
--

INSERT INTO `route_prefix_histories` (`id`, `route_id`, `prefix`, `rate`, `is_delete`, `updated_at`, `created_at`) VALUES
(1, 13, '017', '2', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(2, 13, '016', '1', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(3, 13, '019', '1', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(4, 13, '88017', '2', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(5, 13, '88015', '3', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(6, 13, '88016', '1', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06'),
(7, 13, '88019', '2', 0, '2019-04-30 04:22:06', '2019-04-30 04:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `short_link_accounts`
--

CREATE TABLE `short_link_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_vendor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpm` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telerivets`
--

CREATE TABLE `telerivets` (
  `id` int(10) NOT NULL,
  `label` varchar(191) NOT NULL,
  `api_key` varchar(191) NOT NULL,
  `project_id` varchar(191) NOT NULL,
  `route_id` varchar(191) DEFAULT NULL,
  `random_key` varchar(191) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(22) NOT NULL,
  `api_key` varchar(191) NOT NULL,
  `user_capacity` int(11) NOT NULL,
  `alert` int(11) NOT NULL,
  `user_tpm` int(11) NOT NULL DEFAULT '10',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_smpp` tinyint(1) NOT NULL DEFAULT '0',
  `system_id` varchar(191) DEFAULT NULL,
  `smpp_password` varchar(191) DEFAULT NULL,
  `smpp_port` int(10) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `api_key`, `user_capacity`, `alert`, `user_tpm`, `status`, `is_smpp`, `system_id`, `smpp_password`, `smpp_port`, `updated_at`, `created_at`) VALUES
(1, 'Tanvir', 'Tanvir321@gmail.com', 'Tanvir321', 'hWQMqakpshhbc6T8bSYUxYlVfDSOlB5E4q49QGaaRp52KvG7', 1000, 50, 15, 1, 1, NULL, NULL, 2776, '2019-04-23 01:26:42', NULL),
(4, 'Hasan', 'Hasan123@gmail.com', 'Hasan123', 'CEyMD6V2VeZJlECcrsrSvOLcf9g6amnzCgQEFRRq6Az1XfMc', 1000, 100, 15, 1, 1, NULL, NULL, 2777, '2019-04-27 01:32:46', NULL),
(15, 'Arif', 'arif123@gmail.com', '1234arif', 'TW5JUE8qiQGpZ0LQxLWZf9hj10e1qXlmmrS9V6tlPAykZAxm', 5000, 100, 15, 1, 1, 'arif', 'arif', 2779, '2019-04-27 08:44:04', NULL),
(16, 'SMS', 'mysms123@gmail.com', 'sms2', '5X2xF3mdPh1pUNCJak1JX4H5ZhAxECi9TeR3GlV7UIgkZZnB', 956, 10, 5, 1, 1, 'sms', 'sms2', 2789, '2019-05-02 10:52:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_capacity`
--

CREATE TABLE `user_capacity` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `capacity` int(11) NOT NULL,
  `capacity_type` int(3) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_capacity`
--

INSERT INTO `user_capacity` (`id`, `user_id`, `capacity`, `capacity_type`, `updated_at`, `created_at`) VALUES
(5, 1, 1000, 1, '2019-04-21 04:37:59', '2019-04-21 04:37:59'),
(8, 4, 1000, 1, '2019-04-23 00:02:39', '2019-04-23 00:02:39'),
(19, 15, 5000, 1, '2019-04-27 02:44:04', '2019-04-27 02:44:04'),
(20, 16, 1000, 1, '2019-04-27 02:45:22', '2019-04-27 02:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `user_route_api`
--

CREATE TABLE `user_route_api` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `count` int(22) NOT NULL,
  `total_send` int(11) NOT NULL,
  `tpm` int(10) NOT NULL,
  `status` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_route_api`
--

INSERT INTO `user_route_api` (`id`, `user_id`, `route_id`, `capacity`, `count`, `total_send`, `tpm`, `status`, `updated_at`, `created_at`) VALUES
(1, 16, 13, 0, 0, 27, 20, 1, '2019-04-30 04:46:13', '2019-04-30 04:18:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `call_backs`
--
ALTER TABLE `call_backs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deletes`
--
ALTER TABLE `deletes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `error_message`
--
ALTER TABLE `error_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_replace_report`
--
ALTER TABLE `message_replace_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_send`
--
ALTER TABLE `message_send`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp_outgoing_string`
--
ALTER TABLE `otp_outgoing_string`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp_string`
--
ALTER TABLE `otp_string`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp_vendors`
--
ALTER TABLE `otp_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `over_capacity`
--
ALTER TABLE `over_capacity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_books`
--
ALTER TABLE `phone_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prefixes`
--
ALTER TABLE `prefixes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prefix_histories`
--
ALTER TABLE `prefix_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue_message`
--
ALTER TABLE `queue_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_api`
--
ALTER TABLE `route_api`
  ADD PRIMARY KEY (`route`);

--
-- Indexes for table `route_auth`
--
ALTER TABLE `route_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_capacity_history`
--
ALTER TABLE `route_capacity_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_prefixes`
--
ALTER TABLE `route_prefixes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_prefix_histories`
--
ALTER TABLE `route_prefix_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `short_link_accounts`
--
ALTER TABLE `short_link_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telerivets`
--
ALTER TABLE `telerivets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_capacity`
--
ALTER TABLE `user_capacity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_route_api`
--
ALTER TABLE `user_route_api`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `call_backs`
--
ALTER TABLE `call_backs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `deletes`
--
ALTER TABLE `deletes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `error_message`
--
ALTER TABLE `error_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message_replace_report`
--
ALTER TABLE `message_replace_report`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `message_send`
--
ALTER TABLE `message_send`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otp_outgoing_string`
--
ALTER TABLE `otp_outgoing_string`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otp_string`
--
ALTER TABLE `otp_string`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otp_vendors`
--
ALTER TABLE `otp_vendors`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `over_capacity`
--
ALTER TABLE `over_capacity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `phone_books`
--
ALTER TABLE `phone_books`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prefixes`
--
ALTER TABLE `prefixes`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `prefix_histories`
--
ALTER TABLE `prefix_histories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `queue_message`
--
ALTER TABLE `queue_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `route_api`
--
ALTER TABLE `route_api`
  MODIFY `route` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `route_auth`
--
ALTER TABLE `route_auth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `route_capacity_history`
--
ALTER TABLE `route_capacity_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `route_prefixes`
--
ALTER TABLE `route_prefixes`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `route_prefix_histories`
--
ALTER TABLE `route_prefix_histories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `short_link_accounts`
--
ALTER TABLE `short_link_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `telerivets`
--
ALTER TABLE `telerivets`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_capacity`
--
ALTER TABLE `user_capacity`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user_route_api`
--
ALTER TABLE `user_route_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
